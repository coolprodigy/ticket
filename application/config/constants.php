<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('SITE_URL','http://localhost/ticket/');
define('ADMIN_URL','http://localhost/ticket/assets/admin/');
define('DASHBOARD',SITE_URL.'admin/dashboard');
define('CAT_FORM',SITE_URL.'admin/category');
define('LIST_CAT',SITE_URL.'admin/category/list_cat');
define('COMP_FORM',SITE_URL.'admin/complain');
define('LIST_COMP',SITE_URL.'admin/complain/list_comp');
define('USER_FORM',SITE_URL.'admin/user');
define('LIST_USER',SITE_URL.'admin/user/list_user');
define('ADMIN_HEADER','admin/common/header_view');
define('ADMIN_FOOTER','admin/common/footer_view');
define('ADMIN_VIEW','admin/');
define('ADDCAT',SITE_URL.'admin/category/addcat');
define('EDITCAT',SITE_URL.'admin/category/edit_cat/');
define('ADDUSER',SITE_URL.'admin/user/adduser');
define('EDITUSER',SITE_URL.'admin/user/edit_user/');
define('ADDCOMPLAIN',SITE_URL.'admin/complain/addcomplain');
define('EDITCOMPLAIN',SITE_URL.'admin/complain/edit_comp/');
define('VERIFICATION',SITE_URL.'admin/home/verify_login');
define('LOGOUT',SITE_URL.'admin/logout');
define('DETAILS_FORM',SITE_URL.'admin/head/userdetails_form');
define('UPDATEDETAILS',SITE_URL.'admin/head/updatedetails');
define('COMPLAINTASSIGNEDLIST',SITE_URL.'admin/head/complaint_assigned_list');
define('FORGOT_FORM',SITE_URL.'admin/forgot');
define('FORGOT_PASSWORD',SITE_URL.'admin/forgot/forgot_password');
define('SECTION_FORM',SITE_URL.'admin/section');
define('ADDSECTION',SITE_URL.'admin/section/addsection');
define('LIST_SECTION',SITE_URL.'admin/section/list_section');
define('EDITSECTION',SITE_URL.'admin/section/edit_sec/');
define('REPORTS',SITE_URL.'admin/reports');
define('GENERATEREPORTS',SITE_URL.'admin/reports/generate');
define('GETREPORTS',SITE_URL.'admin/reports/getreport');

/* End of file constants.php */
/* Location: ./application/config/constants.php */