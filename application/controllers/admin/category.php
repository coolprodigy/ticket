<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	#Author:Sumith Nalwala
	#Date:19/03/3015
	#If you are making any changes then please mention the date and comment the changes
		
		class Category extends CI_Controller{
		function __Construct(){
		parent::__construct();
		$this->load->model('admin/do_category');
		$this->load->model('admin/do_complain');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->session->set_userdata('is_in_login_page', false);
		$tmp=$this->session->userdata('logged_in');
		if($tmp['status']=='0' || $tmp['status']=='2') exit('You dont have access to this page.');	
		}
	

		#This function will load the category form by which user can add category.
		
		public function index(){
		$this->load->view(ADMIN_HEADER);
		$this->load->view(ADMIN_VIEW.'cat_form');
		$this->load->view(ADMIN_FOOTER);
		}
	
		#This function will retrieve the values posted by admin and will pass that value to the model for insertion.

		public function addcat(){
		$this->form_validation->set_error_delimiters('<div class="error" style="color: red;">', '</div>');
		$this->form_validation->set_rules('comp_cat','Complain Category','required|min_length[3]|max_length[40]');
				if($this->form_validation->run()==false){
							$this->load->view(ADMIN_HEADER);
							$this->load->view(ADMIN_VIEW.'cat_form');
							$this->load->view(ADMIN_FOOTER);
					}
				else{
				
							$data1=array(
							'comp_cat_id'=>$this->input->post('cat_id')?$this->input->post('cat_id'):'',
							'cat_name'=>$this->input->post('comp_cat')
							);
							
							$this->do_category->addcat($data1);
					}	
		}
		
		#This function will display the list of all categories in the front end

		public function list_cat(){
		$cat['cat_list_data']=$this->do_category->get_cat_list();
		$this->load->view(ADMIN_HEADER);
		$this->load->view(ADMIN_VIEW.'cat_list',$cat);
		$this->load->view(ADMIN_FOOTER);
		}
		
		#This function is used for editing the categories.the form will be displayed in the edit mode.

		public function edit_cat($comp_cat_id='null'){
		$cat_edit['edit']=$this->do_category->edit_cat($comp_cat_id);
		$this->load->view(ADMIN_HEADER);
		$this->load->view(ADMIN_VIEW.'cat_form',$cat_edit);
		$this->load->view(ADMIN_FOOTER);
	}
		
		#This function uses the id of category which has to be deleted.
		public function del_cat(){
		$id=$_POST['id'];
		$del=$this->do_category->del_cat($id);
			if($del){
			echo "true";
			}
	}
}