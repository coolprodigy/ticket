<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	#Author:Sumith Nalwala
	#Date:19/03/3015
	#If you are making any changes then please mention the date and comment the changes
	class Complain extends CI_Controller {
	function __Construct(){
	parent::__construct();
	$this->load->model('admin/do_complain');
	$this->load->model('admin/do_user');
	$this->load->model('admin/do_category');
	$this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
	$this->session->set_userdata('is_in_login_page', false);
	#if($this->session->userdata('logged_in')['status']=='0') exit('You dont have access to this page.');
	}

	#This function will load the complain form for adding the complaints of user.
	public function index(){
	$this->load->view(ADMIN_HEADER);
	$this->load->view(ADMIN_VIEW.'complaint_form');
	$this->load->view(ADMIN_FOOTER);
	}
	
	#This function is used for inserting data into the database.Validation is also done using these method.
	public function addcomplain(){
	$this->form_validation->set_error_delimiters('<div class="error" style="color: red;margin-left:150px;">', '</div>');
	#$this->form_validation->set_rules('man_ticket','Ticket No','required|numeric');
	$this->form_validation->set_rules('cust_name','First Name','required|max_length[40]');
	$this->form_validation->set_rules('cust_lastname','Last Name','required|max_length[40]');
	#$this->form_validation->set_rules('cust_mobile','Mobile Number','required|min_length[10]|max_length[13]|numeric');
	#$this->form_validation->set_rules('cust_email','Email Address','required|valid_email');
	$this->form_validation->set_rules('cust_date','Date','required|date_valid');
	$this->form_validation->set_rules('address','address','required|min_length[5]|max_length[140]');
	$this->form_validation->set_rules('cust_message','Complaint Gist','required|min_length[5]');
	$this->form_validation->set_rules('seluser','Assign To','required');
			if ($this->form_validation->run() == FALSE)
			{
			$this->load->view(ADMIN_HEADER);
			$this->load->view(ADMIN_VIEW.'complaint_form');
			$this->load->view(ADMIN_FOOTER);
			}
			else
			{
			#$config['upload_path']= './uploads/';
            #$config['allowed_types'] = 'gif|jpg|png|doc|docx|xls|csv';
            #$config['max_size']        = '20000';
            #$config['max_width']  = '1024';
            #$config['max_height']  = '768'; 
            #$this->load->library('upload', $config);
					/*if(!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						$this->load->view(ADMIN_VIEW.'complaint_form', $error);
					   }
					 else{
					 $data1 = array('upload_data'=>$this->upload->data());
					 }*/
					 #print_R($_POST);die;
						$limit=1;
						$start=0;
						$data=array(
					   'cust_id'=>$this->input->post('cust_id')?$this->input->post('cust_id'):'',
					   'man_ticket'=>$this->input->post('man_ticket'),	
					   'cust_name'=>$this->input->post('cust_name'),
					   'cust_lastname'=>$this->input->post('cust_lastname'),
					   'cust_mobile'=>$this->input->post('cust_mobile'),
					   'cust_date'=>$this->input->post('cust_date'),
					   #'cust_subject'=>$this->input->post('cust_subject'),
					   'cust_email'=>$this->input->post('cust_email'),
					   'cust_message'=>$this->input->post('cust_message'),
					   'comp_cat_id'=>$this->input->post('sel_cat')?implode(',',$this->input->post('sel_cat')):'',
					   'comp_user_id'=>$this->input->post('seluser'),
					   'comment'=>$this->input->post('comment'),	
					   'section'=>$this->input->post('section'),
					   'cust_address'=>$this->input->post('address'),	
					   'comp_assist_id'=>$this->input->post('selassist')?implode(',',$this->input->post('selassist')):''			
						);	
			       /*if(isset($data1['upload_data']['file_name'])){
                        $data['image']= '/uploads/'.$data1['upload_data']['file_name'];
                    }
                    else{
                    $data['image']=''; 
                    }*/
			$images		= '';
			if(isset($_POST['uploader_count']) > 0){
				$cnt		= $_POST['uploader_count'];
				if($cnt > 0){
					for($i=0; $i<$cnt; $i++){
						if($_POST["uploader_".$i."_name"] != '' && $_POST["uploader_".$i."_status"] == 'done'){
							$images		.= $_POST["uploader_".$i."_name"].',';
						}
					}//for ends
				}
			}

			if($images){
				$images = substr($images, 0, -1);
			}
			#print_r($_POST);
			$data['limit']=$limit;
			$data['start']=$start;
			$data['image']=$images;
			#echo "<pre>";
			#print_R($data);die;
			/*if($data['image']>0){
			var_dump($data['image']);
			echo strlen($data['image']);
			$multiple_img=explode(',',$data['image']);
			}
			print_r($multiple_img);die;*/
			#print_R($data);die;
			$this->do_complain->addcomplain($data);
			}
	}
	
	#This function is used to edit the desired complain
	public function edit_comp($cust_id="null"){
		$result=$this->do_complain->updatestatus($cust_id);
				if($result){
					$data['edit_data']=$this->do_complain->edit_comp($cust_id);
					$this->load->view(ADMIN_HEADER);
					$this->load->view(ADMIN_VIEW.'complaint_form',$data);
					$this->load->view(ADMIN_FOOTER);
				}
	}
	 
	#This function will display the admin and superadmin at the frontend.
    public function show_user(){
	$val=$_POST['option'];
	$data=$this->do_complain->show_user($val);
	echo"<option value='0' selected>Unassigned</option>";
		foreach($data as $row){
		echo"<option value='$row[user_id]'>$row[user_name]</option>";
		}
	}
	
	#This function will check for the complainers existence in the database using mobile no and fetch his relevant details.
	public function check_mobile(){
	$mno=$_POST['mno'];
		if($mno==''){
			echo false;
		}	
		else{
			$result=$this->do_complain->check_mobile($mno);
				if($result){
				#print_r($result);
				 echo json_encode($result);
				}
				else{
				echo false;
				}
		}
	}

	
	#This function is used to load all the comments of respective complaints
    public function load_comments(){
    $complain=$_POST['complain'];
    $result=$this->do_complain->load_comments($complain);
    #echo "<pre>";
    #print_r($result);die;
        if($result){
        $cnt=0;
		foreach($result as $key2=>$row){
			$arrange[]=$key2;
		}
		array_multisort($arrange, SORT_ASC, $result);
		#echo "<pre>";
		#print_R($result);die;
        foreach($result as $key=>$value){
		foreach($value as $key1=>$val){#print_r($val);
		$mod_date=strtotime($val["date"]);
		$com_date=date('d-M-y,h:i:s',$mod_date);
        if($cnt%2!=0){
			 echo'<article class="timeline-item">
                                          <div class="timeline-desk">
                                              <div class="panel">
                                                  <div class="panel-body">
                                                      <span class="arrow"></span>
                                                      <span class="timeline-icon red"></span>
                                                      <span class="timeline-date">
														  </span>
														<!--<h1 class="green"></h1>-->
									 <p><a>'.ucwords(isset($val['complain_added_by'])?$val['complain_added_by'].' '.'Created Complaint':(isset($val['user'])?$val['user']:(isset($val['assigned_admin'])?$val['assigned_admin']:(isset($val['assigned_assistant'])?$val['assigned_assistant']:(isset($val['assigned_category'])?$val['assigned_category']:(isset($val['assigned_admin_record'])?$val['assigned_admin_record']:(isset($val['assigned_assistant_record'])?$val['assigned_assistant_record']:(isset($val['assigned_category_record'])?$val['assigned_category_record']:(isset($val['complain_edited_name'])?$val['complain_edited_name']:''))))))))).'</a>'.' '.$com_date.'<br/><a style="color:#797979;" >'.(isset($val['ticket_no'])?'Complaint No'.' '. $val['ticket_no']:(isset($val["comment"])?$val["comment"]:(isset($val['current_assigned'])?'Complaint assigned To'.' '.$val['current_assigned']:(isset($val['user'])?$val['user']:(isset($val['current_attached'])?$val['current_attached'].' '.'Attached to the complaint':(isset($val['prev_ass_name'])?'Complaint Transferred from '.' '.$val['prev_ass_name']:(isset($val['prev_attached_name'])?$val['prev_attached_name'].' '.'dettached from the complaint':(isset($val['ass_dept'])?$val['ass_dept'].' '.'linked to the complaint ':(isset($val['prev_cat_name'])?$val['prev_cat_name'].' ' .'De-linked from the complaint':(isset($val['complain_edited_name'])?'Complain details has been edited':'')))))))))).'</a></p>
                                                  </div>
                                              </div>
                                          </div>
                                      </article>';
        }else{
			echo'<article class="timeline-item alt">
                                          <div class="timeline-desk">
                                              <div class="panel">
                                                  <div class="panel-body">
                                                      <span class="arrow-alt"></span>
                                                      <span class="timeline-icon green"></span>
                                                      <span class="timeline-date">
														  </span>
														<!--<h1 class="green"></h1>-->
									 <p><a style="color:#39B6AE;">'.ucwords(isset($val['complain_added_by'])?$val['complain_added_by'].' '.'Created Complaint':(isset($val['user'])?$val['user']:(isset($val['assigned_admin'])?$val['assigned_admin']:(isset($val['assigned_assistant'])?$val['assigned_assistant']:(isset($val['assigned_category'])?$val['assigned_category']:(isset($val['assigned_admin_record'])?$val['assigned_admin_record']:(isset($val['assigned_assistant_record'])?$val['assigned_assistant_record']:(isset($val['assigned_category_record'])?$val['assigned_category_record']:(isset($val['complain_edited_name'])?$val['complain_edited_name']:''))))))))).'</a>'.' '.$com_date.'<br/><a style="color:#797979;" >'.(isset($val['ticket_no'])?'Complaint No'.' '. $val['ticket_no']:(isset($val["comment"])?$val["comment"]:(isset($val['current_assigned'])?'Complaint assigned To'.' '.$val['current_assigned']:(isset($val['user'])?$val['user']:(isset($val['current_attached'])?$val['current_attached'].' '.'Attached to the complaint':(isset($val['prev_ass_name'])?'Complaint Transferred from '.' '.$val['prev_ass_name']:(isset($val['prev_attached_name'])?$val['prev_attached_name'].' '.'dettached from the complaint':(isset($val['ass_dept'])?$val['ass_dept'].' '.'linked to the complaint ':(isset($val['prev_cat_name'])?$val['prev_cat_name'].' ' .'De-linked from the complaint':(isset($val['complain_edited_name'])?'Complain details has been edited':'')))))))))).'</a></p>
                                                  </div>
                                              </div>
                                          </div>
                                      </article>'; 		
                    }
            $cnt++; 
				}
		     }
        }
        else{
        echo false;
        }
    }


	#This function lists all the complain
    public function list_comp(){ 
		 $data['page_data']=$this->do_complain->get_comp_list();
		 $this->load->view(ADMIN_HEADER);
		 $this->load->view(ADMIN_VIEW.'complain_list',$data);
		 $this->load->view(ADMIN_FOOTER);
	 }
	
	#This function is used to delete the complain
	public function del_comp(){
        $id=$_POST['id'];
		$del=$this->do_complain->del_comp($id);
			if($del){
			echo "true";
			}
	}
	/*public function updatestatus(){
	$compid=$_POST['compid'];
	#echo $compid;die;
	$res=$this->do_complain->updatestatus($compid);
		if($res){
		echo true;	
		}
	}

		public function save(){
		$arr= $_POST;
		print_r($_POST);die;
		$images		= '';
		if(!empty($arr) && count($arr) > 0){
			$cnt		= $arr['uploader_count'];
			if($cnt > 0){
				for($i=0; $i<$cnt; $i++){
					if($arr["uploader_".$i."_name"] != '' && $arr["uploader_".$i."_status"] == 'done'){
						$images		.= $arr["uploader_".$i."_name"].',';
					}
				}//for ends
			}
		}

		if($images){
			$images = substr($images, 0, -1);
		}

		$this->load->database();
		$db_data = array('id'=> NULL,'image'=> $images);
		$this->db->insert('upload',$db_data);
		$this->load->view('upload');
	}
	*/
	public function upload(){
	#echo $_FILES['file']['name'];
	$config['upload_path']= './uploads/';
	$config['allowed_types'] = 'gif|jpg|png|doc|docx|xls|csv';
	$config['max_size']	= '20000';
	$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('file'))
		{
		$error = array('error' => $this->upload->display_errors());
		print_r($error);
		}
		else
		{
		$data = array('upload_data' => $this->upload->data());
		print_r($data);
		}	
    }
	
	public function show_assistant(){
	$id=$_POST['aid'];
	$selass=$this->do_complain->show_assistant($id);
	echo"<option value='' selected>Select Assistant</option>";
		foreach($selass as $row){
		echo"<option value='$row[user_id]'>$row[user_name]</option>";
		}
	}

	public function del_image(){
	$id=$_POST['id'];
	$result=$this->do_complain->del_image($id);
		if($result){
			echo true;
		}
	}
	
	public function comp_status(){
	$stats=$_POST['stats'];
	$cust_id=$_POST['cust_id'];
	$result=$this->do_complain->comp_status($stats,$cust_id);
		if($result){
			echo true;
		}
	}
}