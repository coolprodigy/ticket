<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Dashboard extends CI_Controller {
		function __Construct(){
		parent:: __construct();
		$this->load->model('admin/do_user');
		$this->load->model('admin/do_complain');
		$this->session->set_userdata('is_in_login_page', false);
	}
	#After successful login the user will be redirected to the view loaded in this function
	public function index()
	{	
			
			$session_data = $this->session->userdata('logged_in');
			#print_r($session_data);die;
			$data=$this->list_user();
			$data['page_data1']=array_pop($this->list_comp());
			#print_r($data);die;
			$data['page_data2']=array_pop($this->unassigned());
			$data['page_data3']=array_pop($this->assigned());
			$data['page_data4']=array_pop($this->assignedtome());
			$this->load->view(ADMIN_HEADER);
			$this->load->view(ADMIN_VIEW.'home_view',$data);
			$this->load->view(ADMIN_FOOTER);
	}
	
	#This function will display the total count of users.
	public function list_user(){ 
	$data['page_data']=$this->do_user->count_user();
	return $data;
	}
	
	#This function will display the total count of complaints.
	 public function list_comp(){ 
		 $data['page_data1']=$this->do_complain->get_comp_list();
		#print_r($data);die;
		return $data;
	 }
	
	#This function will display the unassigned no of complaints.
	public function unassigned(){
	$data['page_data2']=$this->do_complain->get_unassigned();
	return $data;
	}
	
	#This function will display the assigned no of complaints.
	public function assigned(){
	$data['page_data3']=$this->do_complain->get_assigned();
	return $data;
	}

	#This function will display the count of assigned no of complaints to the superadmin.    
    public function assignedtome(){
    $data['page_data4']=$this->do_complain->assignedtome();
    return $data;
    }
}