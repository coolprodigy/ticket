<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
		class Forgot extends CI_Controller{
		function __Construct(){
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->model('admin/do_forgot');
		}
 
		public function index(){
		$this->load->view(ADMIN_VIEW.'forgot_form'); 
		}
	
		#This function has been written for forgot password
		public function forgot_password(){
		$this->form_validation->set_error_delimiters('<div class="error" style="color: red;">', '</div>');
		$this->form_validation->set_rules('ckemail','Email','required|valid_email|xss_clean|callback_check_email');
			if($this->form_validation->run()==False){
			#echo 'hello';die;
			$this->load->view(ADMIN_VIEW.'forgot_form');
			}
		}
		#This is callback function which would check for the existence of email.
		public function check_email(){
		#print_R($_POST);
		$data=array(
		'email'=>$_POST['ckemail']
		);
			$result=$this->do_forgot->check_email($data);
			if($result){
			#$mail=file_get_contents(SITE_URL('admin/user/sendmail'),true);
			#echo $mail;
			require(APPPATH.'controllers/admin/user.php');
			$mail=new user();
			$pwd=$mail->get_random_password();
			$subject='Dear User,<br/>Your Password has been set.<br/>Please login using following password<br/><br/> Login Information<br/>UserID:'.' '.$data['email'].'<br/>Password:'.$pwd.'<br/>Thanks<br/>Admin Team';
			#$mail->sendmail($data['email'],$pwd,$subject);
			$this->session->set_userdata('is_in_login_page', true);
			$data['pwd']=$pwd;
			$this->do_forgot->update_new_pwd($data);
			$pass['checked']=true;
			$this->load->view(ADMIN_VIEW.'login',$pass);
			}
			else{
			#echo 'bye';die;
			$this->form_validation->set_message('check_email', 'No email with these record');
			return false;
			}
		}	
 }