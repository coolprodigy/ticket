<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	#Author:Sumith Nalwala
	#Date:26/03/2015
	#If you are making any changes then please mention the date and comment the changes

	class Head extends CI_Controller {
		function __Construct(){
			parent::__construct();
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$this->load->model('admin/do_head');
			$this->load->model('admin/do_complain');
			$tmp=$this->session->userdata('logged_in');
			if($tmp['status']=='1') exit('You dont have access to this page.');
			$this->session->set_userdata('is_in_login_page', false);
		 }
	#This function loads the form of users setting(change password). 
	public function userdetails_form(){
	$this->load->view(ADMIN_HEADER);
	$this->load->view(ADMIN_VIEW.'details_form');
	$this->load->view(ADMIN_FOOTER);
	}

	#This function will check for the current password.if it is not available then it will prompt to enter correct password
	public function checkpwd(){
	$tmp=$this->session->userdata('logged_in');
	$data['id']=$tmp['id'];
	$data['pwd']=$_POST['pwd'];
	$result=$this->do_head->checkpwd($data);
		if($result){
			echo 'true';
		}
		else{
		echo 'false';
		
		}
	}
	
	#This function validates the password fields and then enters into the database.
	public function updatedetails(){
	$this->form_validation->set_error_delimiters('<div class="error" style="color: red;">', '</div>');
	$this->form_validation->set_rules('old_pwd','Current Password','required|min_length[3]|max_length[15]|xss_clean');
	$this->form_validation->set_rules('new_pwd','New Password','required|min_length[3]|max_length[15]|matches[con_pwd]|xss_clean');
	$this->form_validation->set_rules('con_pwd','Confirm Password','required|min_length[3]|max_length[15]|xss_clean');
			if($this->form_validation->run()==false){
			$this->load->view(ADMIN_HEADER);
			$this->load->view(ADMIN_VIEW.'details_form');
			$this->load->view(ADMIN_FOOTER);
			}
			else{
				$data1=array(
				'password'=>md5($this->input->post('new_pwd'))	
				);
			$this->do_head->updatedetails($data1);
			}

	}

	public function complaint_assigned_list(){
	$tmp=$this->session->userdata('logged_in');
	$id=$tmp['id'];
	if($tmp['status']==0){
	#echo 'hi';die;
	$data['page_data']=$this->do_head->complaint_assigned_list($id);
	}
	else{
	#echo 'hi';die;
	$data['page_data']=$this->do_head->complaint_attached_list($id);
	}
	#print_R($data);
	$this->load->view(ADMIN_HEADER);
	$this->load->view(ADMIN_VIEW.'complaint_assigned_list',$data);
	$this->load->view(ADMIN_FOOTER);
	}

}