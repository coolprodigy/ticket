<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __Construct(){
	#echo 'hi';die;
	parent:: __construct();
	$this->load->model('admin/do_home');
	$this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
	}
	#This function loads the login page.
	/*public function index()
	{	
		#$this->load->view(ADMIN_VIEW.'login');
		
	}*/
	
	#This function validates the field then it checks the user credentials.
	public function verify_login(){
		$this->form_validation->set_error_delimiters('<div class="error" style="color: red;">', '</div>');
		$this->form_validation->set_rules('uname','Username','trim|required|min_length[10]|max_length[25]|xss_clean');
		$this->form_validation->set_rules('pwd','Password','trim|required|min_length[4]|max_length[15]|xss_clean|callback_check_database');
			if($this->form_validation->run()==false){
			   $this->load->view(ADMIN_VIEW.'login','refresh');
			}
			else{
				redirect(ADMIN_VIEW.'dashboard','refresh');
			}
	}
	
	#This function checks for the values in the database
	public function check_database(){
				$data=array(
				'uname'=>$this->input->post('uname'),	
				'password'=>md5($this->input->post('pwd'))
				);
				#print_R($data);die;
				$result=$this->do_home->verify_login($data);
				#print_R($result);die;
					if($result){
						
						$sess_array = array();
						$sess_array = array(
						'id' => $result['user_id'],
						'uname' => $result['user_email'],
						'status'=>$result['role']
						);
						
						$this->session->set_userdata('logged_in',$sess_array);
						return TRUE;
					}
					else
				   {
					 $this->form_validation->set_message('check_database', 'Invalid username or password');
					 return false;
				   }
	}
	
}

