<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
    class Login extends CI_Controller {
		function __Construct(){
		parent:: __construct();
		$this->session->set_userdata('is_in_login_page', true);
	}
	#After successful login the user will be redirected to the view loaded in this function.
	public function index()
	{	
	$this->load->view(ADMIN_VIEW.'login');
	}
	

}