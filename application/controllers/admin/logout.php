<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Logout extends CI_Controller {
		function __Construct(){
		parent:: __construct();
		$this->session->set_userdata('is_in_login_page', false);
		 
	}

	#This function is used for log out.
	public function index(){
		$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		redirect(ADMIN_VIEW.'login', 'refresh');
		
	}
	
	
}