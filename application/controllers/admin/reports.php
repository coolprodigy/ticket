<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Reports extends CI_Controller{
	function __construct(){
	parent::__construct();
	$this->load->model('admin/do_complain');
	$this->load->model('admin/do_reports');
	}
	
	public function index(){
		$this->load->view(ADMIN_HEADER);
		$this->load->view(ADMIN_VIEW.'reports_form');
		$this->load->view(ADMIN_FOOTER);
	}

	public function generate(){
	$extended='';
	if(isset($_POST['strtdt']) && !empty($_POST['strtdt']) && isset($_POST['enddt']) && !empty($_POST['enddt'])){
		$data=array(
		'strtdt'=>$this->input->post('strtdt'),	
		'enddt'=>$this->input->post('enddt')
		);
		$extended .=" and a.cust_date between '". $data['strtdt']."' and '".$data['enddt']."'";
	}
	elseif(isset($_POST['monthdt']) && !empty($_POST['monthdt'])){
		$dt=explode('/',$this->input->post('monthdt'));
		$strt=$dt[1].'-'.$dt[0].'-'.'01';
		$endt=date("Y-m-t", strtotime($strt));
		#print_R($dt);die;
		$data=array(	
		'monthst'=>$strt,
		'monthet'=>$endt
		);
		$extended .=" and a.cust_date between '". $data['monthst']."' and '".$data['monthet']."'";
	}
	elseif(isset($_POST['user']) && !empty($_POST['user'])){
		$data=array(
		'user'=>$this->input->post('user')	
		);
		$extended .=" and b.comp_user_id='".$data['user']. "' group by a.section";
	}
	else{
		$data='';
		$extended='';	
	}
	$res=$this->do_reports->generate($extended);
		if($res){
			$this->getreport($res);
		}
	/*$this->load->library('excel');
	$file = './reports/tpl_report.xlsx';
	$objPHPExcel = PHPExcel_IOFactory::load($file);
	#Read cell data into array format for display.
	#$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection(); 
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Hello');
	$objPHPExcel->getActiveSheet()->SetCellValue('D5', 'world!');
	$objPHPExcel->getActiveSheet()->SetCellValue('E5', 'Hello');
	$objPHPExcel->getActiveSheet()->SetCellValue('F5', 'world!');
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	#var_dump($objWriter);
	$objWriter->save('./reports/reports-'.date("Y-m-d").'.xlsx');*/
	}

	#THis function is used to get report from the database
	public function getreport($res1){
	$result=$this->do_reports->getreport($res1);
	#echo "<pre>";
		if($result){
			$i=1;
			$j=5;
			$this->load->library('excel');
			$file = './reports/tpl_report.xlsx';
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getProperties()->setCreator("Darbhanga Team");
			$objPHPExcel->getProperties()->setTitle("Grievance ".date("Y-m-d"));
			$objPHPExcel->getActiveSheet()->mergeCells("A2:Y2"); 
			#$objPHPExcel->getActiveSheet()->mergeCells(cellsToMergeByColsRow(0,2,3))
			$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'STATUS REPORT OF PUBLIC GRIEVANCE PETITION AS ON '.date("d.m.Y"));
			$res=$this->do_reports->getuserid($res1);
			#print_R($res);die;
			$a='5';
			$uid=array();
			foreach($res as $key=>$value){
			$uid[$value['user']] = $a;
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$a, $i);
			$a++;
			$i++;
			}
			#echo "<pre>";
			#print_r($uid);die;
			while($row=mysql_fetch_assoc($result)){#print_R($row);
				$cells=explode(',',$row['section_cell']);
				#print_R($cells);die;
				#Read cell data into array format for display.
				#$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				$j=$uid[$row['user']];
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$j, $row['name']);
				$objPHPExcel->getActiveSheet()->SetCellValue($cells[0].$j, $row['total']);
				$objPHPExcel->getActiveSheet()->SetCellValue($cells[1].$j, $row['resolved']);
				$objPHPExcel->getActiveSheet()->SetCellValue($cells[2].$j, $row['pending']);
				$objPHPExcel->getActiveSheet()->SetCellValue('U'.$j, $row['alltotal']);
				$objPHPExcel->getActiveSheet()->SetCellValue('X'.$j, $row['allresolved']);
				$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$j, $row['allpending']);
				$j++; // user - row id
			}
			$dt=date("Y-m-d-his").rand();
			#$a= "<a href='".SITE_URL."reports/reports_$dt.xlsx'>$dt</a>";
			#echo $a;
			$a= SITE_URL."reports/reports_$dt.xlsx";
			//$view=$a;
			$this->session->set_flashdata('rep_view',$a);
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('./reports/reports_'.$dt.'.xlsx');
			$filepath=$dt.'.xlsx';	
			$result1=$this->do_reports->insertpath($filepath,$res1);
			#echo $filepath;die;
				if($result1){
					redirect(ADMIN_VIEW.'reports/report_result');
				}
			#$this->load->view(ADMIN_HEADER);
			#$this->load->view(ADMIN_FOOTER);
		}
			
			#var_dump($objWriter);
	}
	
	#This function is used to load the view of report generated in view
	public function report_result(){
	#$this->load->view(ADMIN_HEADER);
	$this->load->view(ADMIN_VIEW.'report_result');
	#$this->load->view(ADMIN_FOOTER);
	}
}