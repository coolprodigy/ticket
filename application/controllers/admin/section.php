<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	#Author:Sumith Nalwala
	#Date:29/05/2015
	#If you are making any changes then please mention the date and comment the changes
		
		class Section extends CI_Controller{
		function __Construct(){
		parent::__construct();
		$this->load->model('admin/do_section');
		$this->load->model('admin/do_complain');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->session->set_userdata('is_in_login_page', false);
		$tmp=$this->session->userdata('logged_in');
		if($tmp['status']=='0' || $tmp['status']=='2') exit('You dont have access to this page.');	
		}
	

		#This function will load the category form by which user can add category.
		
		public function index(){
		$this->load->view(ADMIN_HEADER);
		$this->load->view(ADMIN_VIEW.'section_form');
		$this->load->view(ADMIN_FOOTER);
		}
	#This function will insert the values into the database.

		public function addsection(){
		$this->form_validation->set_error_delimiters('<div class="error" style="color: red;">', '</div>');
		$this->form_validation->set_rules('comp_sec','Complain Section','required|min_length[3]|max_length[40]');
				if($this->form_validation->run()==false){
							$this->load->view(ADMIN_HEADER);
							$this->load->view(ADMIN_VIEW.'section_form');
							$this->load->view(ADMIN_FOOTER);
					}
				else{
				
							$data1=array(
							'comp_sec_id'=>$this->input->post('sec_id')?$this->input->post('sec_id'):'',
							'complaint_section'=>$this->input->post('comp_sec')
							);
							
							$this->do_section->addsection($data1);
					}	
		}

		public function list_section(){
		$sec['sec_list_data']=$this->do_section->get_sec_list();
		$this->load->view(ADMIN_HEADER);
		$this->load->view(ADMIN_VIEW.'section_list',$sec);
		$this->load->view(ADMIN_FOOTER);
		}
		
		public function edit_sec($comp_sec_id='null'){
		$sec_edit['edit']=$this->do_section->edit_sec($comp_sec_id);
		$this->load->view(ADMIN_HEADER);
		$this->load->view(ADMIN_VIEW.'section_form',$sec_edit);
		$this->load->view(ADMIN_FOOTER);
	}

		public function del_sec(){
		$id=$_POST['id'];
		$del=$this->do_section->del_sec($id);
			if($del){
			echo "true";
			}
	}
}