<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	#Author:Sumith Nalwala
	#Date:19/03/3015
	#If you are making any changes then please mention the date and comment the changes

	class User extends CI_Controller {
	function __Construct(){
		parent::__construct();
		$this->load->model('admin/do_category');
		$this->load->model('admin/do_user');
		$this->load->model('admin/do_complain');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->session->set_userdata('is_in_login_page', false);
		$tmp=$this->session->userdata('logged_in');
		if($tmp['status']=='0' || $tmp['status']=='2') exit('You dont have access to this page.');	
	 }
	
	#This function will display the add user form.
	public function index(){
	$cat['cat_list_data']=$this->do_category->get_cat_list();
	$this->load->view(ADMIN_HEADER);
	$this->load->view(ADMIN_VIEW.'user_form',$cat);
	$this->load->view(ADMIN_FOOTER);
	}

	#This function will takes the input to add user and validate.
	public function adduser(){ 
	$this->form_validation->set_error_delimiters('<div class="error" style="color: red;">', '</div>');
	$this->form_validation->set_rules('selrole','Select Role','required');
	$this->form_validation->set_rules('desg','Select Designation','required|min_length[3]|max_length[40]|xss_clean');
	$this->form_validation->set_rules('user_name','First Name','required|min_length[3]|max_length[40]|alpha|xss_clean');
	$this->form_validation->set_rules('user_lname','Last Name','required|min_length[3]|max_length[40]|alpha|xss_clean');
	$this->form_validation->set_rules('user_email','Email Address','required|valid_email|xss_clean');
	$this->form_validation->set_rules('user_mobile','Mobile Number','required|min_length[10]|max_length[13]|numeric|xss_clean');
	/*$this->form_validation->set_rules('pwd','Password','required|min_length[5]|matches[con_pwd]|xss_clean');
	$this->form_validation->set_rules('con_pwd','Confirm Passwrod','required|min_length[5]|xss_clean');
	$this->form_validation->set_rules('user_message','Your Message','required|min_length[5]');*/
				if ($this->form_validation->run() == FALSE)
				{		
						$cat['cat_list_data']=$this->do_category->get_cat_list();
						$this->load->view(ADMIN_HEADER);
						$this->load->view(ADMIN_VIEW.'user_form',$cat);
						$this->load->view(ADMIN_FOOTER);
				}
				else
				{		
						$config['upload_path']= './uploads/';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size']        = '20000';
						$config['max_width']  = '1024';
						$config['max_height']  = '768'; 
						$this->load->library('upload', $config);
					if(!$this->upload->do_upload('image')){
						#echo "hi";die;
						$error = array('error' => $this->upload->display_errors());
						$this->load->view(ADMIN_VIEW.'user_form', $error);
					   }
					 else{
					 $data1 = array('upload_data'=>$this->upload->data());
					 }
						$pwd=$this->get_random_password();
						#$id=$this->input->post('user_id');
						$selcat=$this->input->post('selcat')?implode(',', $this->input->post('selcat')):'';
						#$selcatval = implode(',', $selcat);
						$data=array(
						'user_id'=>$this->input->post('user_id')?$this->input->post('user_id'):'',
						'designation'=>$this->input->post('desg'),
						'role'=>$this->input->post('selrole'),
						'user_name'=>$this->input->post('user_name'),
						'user_lname'=>$this->input->post('user_lname'),
						'user_email'=>$this->input->post('user_email'),
						'user_mobile'=>$this->input->post('user_mobile'),
						/*'password'=>md5($this->input->post('pwd')),*/
						'password'=>md5($pwd),
						'cat_sel'=>$selcat
						/*'user_email'=>$this->input->post('user_email'),
						'user_mobile'=>$this->input->post('user_mobile'),
						'user_message'=>$this->input->post('user_message'),*/
						);
					if(isset($data1['upload_data']['file_name'])){
                        $data['image']= 'uploads/'.$data1['upload_data']['file_name'];
                    }
                    else{
                    $data['image']=''; 
                    }
				#print_R($data);die;
				$result=$this->do_user->adduser($data);
                        if($result && $data['user_id']==''){
						$subject='Dear User,<br/>Your account has been successfully created.<br/>Please click on below URL or paste into your browser<br/><br/> http://localhost/ticket/<br/><br/>Login Information<br/>User ID:'.$data['user_email'].'<br/>Password:'.$pwd.'<br/>Thanks<br/>Admin Team';
						$this->sendmail($data['user_email'],$pwd,$subject);
						}
				redirect('admin/user/list_user');	
				}
	}
	
	#This function will display the entire list of users in the frontend.
	public function list_user(){ 
	$data['page_data']=$this->do_user->get_user_list();
	$this->load->view(ADMIN_HEADER);
	$this->load->view(ADMIN_VIEW.'user_list',$data);
	$this->load->view(ADMIN_FOOTER);			
	}
	
	#This function will delete the user form the database.		
	public function del_User() {
	$id=$_POST['id'];
		$del=$this->do_user->del_user($id);
		if($del){
		echo "true";
		}
	}

	#This function will display the user that need to be edited.
	public function edit_user($id="null",$type="null"){
	$data['type']=$type;
	$data['cat_list_data']=$this->do_category->get_cat_list();
	$data['edit_data']=$this->do_user->edit_user($id);
	$this->load->view(ADMIN_HEADER);
	$this->load->view(ADMIN_VIEW.'user_form',$data);
	$this->load->view(ADMIN_FOOTER);
	}
	
	#This function will send the mail to the user.
	public function sendmail($to,$password,$subject){
 
		$this->load->library('email');
		$config['protocol']='smtp';
		$config['smtp_host']='ssl://smtp.googlemail.com';
		$config['smtp_port']='465';
		$config['smtp_timeout']='30';
		$config['smtp_user']='infotestng@gmail.com';
		$config['smtp_pass']='9757252991';
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from('infotestng@gmail', 'Grievance');
		$this->email->to($to);
		$this->email->subject('Getting started with email');
		$this->email->message($subject);
		#$this->email->send();
		if($this->email->send())
         {
          return true;
         }
         else
        {
         show_error($this->email->print_debugger());
        }
	}
	
	#This function will generate random password of 8 characters.
	public function get_random_password($chars_min=8, $chars_max=8, $use_upper_case=true, $include_numbers=true, $include_special_chars=true)
    {
        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if($include_numbers) {
            $selection .= "1234567890";
        }
        if($include_special_chars) {
            $selection .= "!@\"#$%&[]{}?|";
        }

        $password = "";
        for($i=0; $i<$length; $i++) {
            $current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
            $password .=  $current_letter;
        }                

      return $password;
    }

	public function get_searched_user(){
	$search=isset($_POST['srch'])?$_POST['srch']:'';
	$result=$this->do_user->get_user_list($search);
	foreach ($result as $key=>$row){
	$response='<div class="col-md-6 col-sm-6" id="listuser" style="font-size:inherit">';
    $response.='<div class="panel">';
    $response.='<div class="panel-body">';
    $response.='<div class="media" >';
	$response.='<div style="float:right;"><a href="'.EDITUSER.$row['user_id'].'"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></div>';
    $response.='<a class="pull-left profile-pic" href="#">';
                             if($row['image']!=''){
							  $response.='<img class="thumb media-object" src="'.SITE_URL.$row["image"].'" >';
							  }else {
							   $response.='<img class="thumb media-object" src="'.SITE_URL.'assets/admin/img/default.png">';
							  }	
							   $response.='</a>';
							   $response.='<div class="media-body">';
                               $response.='<h4>'.ucfirst($row["user_name"]).' '.ucfirst($row["user_lname"]).'</h4>';
								$response.='<address>';
                                     $response.='<p><span style="float:left;margin-left:0px;"><strong>'.$row['designation'].'</strong></span></p><br>';
                                      $response.='<p>'.$row['user_email'].'</p>';
                                      $response.='<abbr title="Phone"></abbr><p>'.$row['user_mobile'].'</p>';
									  $response.='<p style="color:green;">New Task Issued :'.$row['assigned'].'</p>';
									  $response.='<p style="color:red;">Task Pending    : '.$row['assigned'].'</p>';
									  $response.='<p style="color:blue;">Comments        :'. $row['comments'].'</p>';
									  $response.='</address>';
									  $response.='<ul>';
                                      $response.='<li><a><i></i></a></li>';
                                      $response.='<li><a><i></i></a></li>';
                                      $response.='<li><a><i></i></a></li>';
                                      $response.='<li><a><i></i></a></li>';
                                  $response.='</ul>';
                              $response.='</div>';
                          $response.='</div>';
                      $response.='</div>';
                  $response.='</div>';
              $response.='</div>'; 
			  echo $response;
			}

	} 

}
?>
