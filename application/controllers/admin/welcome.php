<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	function index()
	{
		$this->load->view('admin/upload', array('error' => ' ' ));
	}
	
	
	function upload(){
		echo $_FILES['file']['name'];
		$config['upload_path']= './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|doc|docx|xls|csv';
		$config['max_size']	= '20000';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			print_r($data);
		}	
	
	}
	
	function save(){
		$arr	= $_POST;
		print_r($_POST);die;
		$images		= '';
		if(!empty($arr) && count($arr) > 0){
			$cnt		= $arr['uploader_count'];
			if($cnt > 0){
				for($i=0; $i<$cnt; $i++){
					if($arr["uploader_".$i."_name"] != '' && $arr["uploader_".$i."_status"] == 'done'){
						$images		.= $arr["uploader_".$i."_name"].',';
					}
				}//for ends
			}
		}

		if($images){
			$images = substr($images, 0, -1);
		}

		$this->load->database();
		$db_data = array('id'=> NULL,'image'=> $images);
		$this->db->insert('upload',$db_data);
		$this->load->view('upload');
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */