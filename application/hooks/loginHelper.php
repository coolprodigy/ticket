<?php
class SessionData 
{
    var $CI;

    function __construct(){
         $this->CI =& get_instance();
		
	}

    function initializeData() {
			if(!$this->CI->session->userdata('logged_in') && !$this->CI->session->userdata('is_in_login_page'))
			{	
				redirect(ADMIN_VIEW.'login','refresh');
			}
			if($this->CI->session->userdata('logged_in')){
			$class=$this->CI->router->fetch_class();
			#echo $class;
			#$method=$this->CI->router->fetch_method();
				if($class=='login'|| $class=='forgot'){
					if((strpos(isset($_SERVER['HTTP_REFERER']), 'http://localhost/ticket/admin/login') === 0)){
					echo "<script type='text/javascript'>window.history.go(-1);</script>";
					}
					else{
					#header('Location: ' . $_SERVER['HTTP_REFERER']);
					echo "<script type='text/javascript'>window.history.go(+1);</script>";
					}
				}
			}
	}

}
?>