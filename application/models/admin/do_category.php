<?php 
	#Author:Sumith Nalwala
	#Date:19/03/3015
	#If you are making any changes then please mention the date and comment the changes
		
	class Do_Category extends CI_Model{
		function __Construct(){
		parent::__construct();
		}
	
	#This function will add the category into teh database.

	public function addcat($data1){
		$sql="insert into comp_cat(comp_cat_id,cat_name) values('".$data1['comp_cat_id']."','".$data1['cat_name']."') on duplicate key update cat_name='".$data1['cat_name']."'";
		$this->db->query($sql);
		redirect('admin/category/list_cat');
	}  
	
	#This function retrieves all the categories from the database.
	public function get_cat_list(){
		$this->db->order_by("cat_name", "asc"); 
		$sql=$this->db->get('comp_cat');
		return $sql->result_array();
	}
	
	#This function retrieves the desired row for editing.
	public function edit_cat($comp_cat_id){
	$sql=$this->db->get_where('comp_cat', array('comp_cat_id' => $comp_cat_id));
	return $sql->row_array();
	}
	
	#This function is used for deleting the category from the database.
	public function del_cat($id){
	$sql=$this->db->delete('comp_cat',array('comp_cat_id'=>$id));
	return $sql;
	}
}