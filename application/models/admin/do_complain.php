<?php
	#Author:Sumith Nalwala
	#Date:19/03/3015
	#If you are making any changes then please mention the date and comment the changes
	
	class Do_Complain extends CI_Model{
		function __construct(){
		parent::__construct();
	}
   
	#This function will insert into the database and retrieve the ticket no generated for complaint.
	public function addcomplain($data){
			$this->db->select('ticket_no,date_added');
			$this->db->order_by("cust_id","desc");
			$this->db->from('user_complain');
			$this->db->limit($data['limit'],$data['start']);
			$query=$this->db->get();
			$rowcount = $query->num_rows();
			$result=$query->result_array();
				if($result){
				$com_date=$result['0']['date_added'];
				$com_year = date("Y", strtotime($com_date));
				}
			$curyear=date('Y');
			#echo $curyear;
			#echo $com_year;
			$dt=date('Y');
				if($rowcount=='0' || $com_year<$curyear ){
				$ftkt=1;
				$ticket=$ftkt.'/'.$dt;
				}
				elseif($data['cust_id']){
				$sql2=$this->db->query("select ticket_no from user_complain where cust_id=".$data['cust_id']);
				$result3=$sql2->row_array();
				#echo $result3['ticket_no'];die;
				$ticket=$result3['ticket_no'];
				}
				else{
				$tkt=explode('/',$result['0']['ticket_no']);
				$ntkt=$tkt[0];
				$tktn=$ntkt+1;
				$ticket=$tktn.'/'.$dt;
				}
			date_default_timezone_set("Asia/Kolkata");
			$tmp=$this->session->userdata('logged_in');
			$userid=$tmp['id'];
			$check_edit=$this->edited($data,$userid);
			#echo strlen($check_edit);
			if($check_edit){
			#echo $check_edit;die;
			$this->db->query($check_edit);
			}
			$sql="insert into user_complain(cust_id,cust_name,cust_lastname,cust_mobile,cust_date,cust_message,cust_address,cust_email,ticket_no,date_added,section,complain_added_by) values('".$data['cust_id']."','".addcslashes($data['cust_name'],"';")."','".addcslashes($data['cust_lastname'],"';")."','".$data['cust_mobile']."','".$data['cust_date']."','".addcslashes($data['cust_message'],"';")."','".addcslashes($data['cust_address'],"';")."','".$data['cust_email']."','".$ticket."',now(),'".$data['section']."','".$userid."') on duplicate key update cust_name='".addcslashes($data['cust_name'],"';")."',cust_lastname='".addcslashes($data['cust_lastname'],"';")."',cust_mobile='".$data['cust_mobile']."',cust_email='".$data['cust_email']."',cust_message='".addcslashes($data['cust_message'],"';")."',cust_address='".addcslashes($data['cust_address'],"';")."'";
			$this->db->query($sql);
			$lastid=$this->db->insert_id();
			#echo $lastid;
			if(strlen($data['image'])>0){
			$img_cust_id=$lastid?$lastid:$data['cust_id'];
			$multiple_img=explode(',',$data['image']);
				foreach($multiple_img as $img){
				$this->db->query("Insert into user_complain_images(img_id,cust_id,img_path) values('','".$img_cust_id."','uploads/".$img."')");	
				}		
			}
			#echo 'hi';die;
			if($data['cust_id']){
			$sql3=$this->db->get_where('complaint_assigned',array('comp_cust_id'=>$data['cust_id']))->row_array();
			$query1="update complaint_assigned set comp_user_id='".$data['comp_user_id']."' where comp_cust_id='".$data['cust_id']."'";
			
				if($data['comp_user_id']!=$sql3['comp_user_id']){
				#echo 'bye';die;
				$success=$this->db->query($query1);
					if($sql3['comp_user_id']!=0 && $success){
					$sql9="INSERT IGNORE INTO complaint_assigned_record(comp_record_id,comp_id,comp_cust_id,comp_user_id,comp_status,comp_assigned_by,comp_added,comp_modified) values('','".$sql3['comp_id']."','".$sql3['comp_cust_id']."','".$sql3['comp_user_id']."','".$sql3['comp_status']."','".$sql3['comp_assigned_by']."','".$sql3['comp_added']."','".$sql3['comp_modified']."')";
					mysql_query($sql9);
					}
				}
			#echo 'cool';die;
			$sql5=$this->db->get_where('complaint_category',array('comp_cust_id'=>$data['cust_id']))->row_array();
				
				if($data['comp_cat_id']!=$sql5['comp_cat_id']){
				$query7=$this->db->query("update complaint_category set comp_cat_id='".$data['comp_cat_id']."' where comp_cust_id='".$data['cust_id']."'");
					if(!empty($sql5['comp_cat_id']) && $query7){
					$this->db->query("insert ignore into complaint_category_record(cat_record_id,category_id,comp_cat_id,comp_cust_id,comp_cat_added_by,category_added,category_modified) values('','".$sql5['category_id']."','".$sql5['comp_cat_id']."','".$sql5['comp_cust_id']."','".$sql5['comp_cat_added_by']."','".$sql5['category_added']."','".$sql5['category_modified']."')");
					}
				}
			$query2=$this->db->query("select user_email from user_register where user_id=".$data['comp_user_id']);
			$sql4=$this->db->get_where('complaint_attached',array('comp_cust_id'=>$data['cust_id']))->row_array();
				
				if($data['comp_assist_id']!=$sql4['comp_assist_id']){
				$query5=$this->db->query("update complaint_attached set comp_assist_id='".$data['comp_assist_id']."' where comp_cust_id='".$data['cust_id']."'");
					if($sql4['comp_assist_id']!='' && $query5){
					$this->db->query("Insert ignore into complaint_attached_record(comp_record_id,comp_att_id,comp_cust_id,comp_assist_id,comp_assist_added_by,comp_assist_added,comp_assist_modified) values('','".$sql4['comp_att_id']."','".$sql4['comp_cust_id']."','".$sql4['comp_assist_id']."','".$sql4['comp_assist_added_by']."','".$sql4['comp_assist_added']."','".$sql4['comp_assist_modified']."')");
					}
				}
			$result1=$query2->row_array();
				if($result1){
				$subject="Complaint Updated";
				$message="The complaint has been updated with ticket no ".$ticket;
				#$this->sendmail($result1['user_email'],$subject,$message);
				}
			}
			else{
            $this->db->set('comp_cust_id',$lastid);
            $this->db->set('comp_user_id',$data['comp_user_id']);
            $this->db->set('comp_status',0);
			$this->db->set('comp_assigned_by',$userid);
			$this->db->set('comp_added',date('Y-m-d H:i:s'));
			$this->db->insert('complaint_assigned');
			$query3=$this->db->query("select user_email from user_register where user_id=".$data['comp_user_id']);
			$query4=$this->db->query("Insert into complaint_attached(comp_att_id,comp_cust_id,comp_assist_id,comp_assist_added_by,comp_assist_added) values('','".$lastid."','".$data['comp_assist_id']."','".$userid."',now())");
			$query6=$this->db->query("Insert into complaint_category(category_id,comp_cat_id,comp_cust_id,comp_cat_added_by,category_added) values('','".$data['comp_cat_id']."','".$lastid."','".$userid."',now())");
			$result2=$query3->row_array();
				if($result2){
				$subject="Complaint Assigned";
				$message="The complain has been registered and assigned to you with ticket no ".$ticket;
				#$this->sendmail($result2['user_email'],$subject,$message);
				}
			}
            $complain_no=$lastid?$lastid:$data['cust_id'];
            if(strlen($data['comment'])>1){
			$comment_status='0';
			#echo $data['comment'];
			$comment=addcslashes($data['comment'],"'");
			#echo $comment;
			#die;
            $sql1=$this->db->query("insert into complaint_comment(comment_id,comment_complaint_id,comment_desc,comment_user_id,comment_status) values('','".$complain_no."','".$data['comment']."','".$userid."','".$comment_status."') ");
            }
			#echo 'hi';die;
			#$ticket_no['ticketno']=$ticket;
			if($sql && $ticket){
					if($tmp['status']=='0' || $tmp['status']=='2'){
					echo "<script>
					alert('Your complain has been registered with complain no ".$ticket."');
					window.location.href='../head/complaint_assigned_list';
					</script>";
					}
					else{
					#echo 'hi';die;
					echo "<script>
					alert('Your complain has been registered with complain no ".$ticket."');
					window.location.href='list_comp';
					</script>";
					}
			}
	}
    
	#This function will display all the complains added by the user.
	public function get_comp_list(){
	  #$sql=$this->db->query("SELECT a.*,b.comp_user_id from user_complain a left join complaint_assigned b on a.cust_id=b.comp_cust_id");
	  $sql=$this->db->query("SELECT a.*,b.comp_user_id,b.comp_status,(select count(c.comment_complaint_id) from complaint_comment c where a.cust_id=c.comment_complaint_id) as comments,d.complaint_section as section_name,e.comp_cat_id,(select GROUP_CONCAT(cat_name) from comp_cat f where FIND_IN_SET(f.comp_cat_id,e.comp_cat_id)) as cat_name,concat(g.user_name,' ',g.user_lname) as assigned_to,g.designation,g.image as assigned_image,(select h.comment_desc from complaint_comment h where a.cust_id=h.comment_complaint_id ORDER BY h.comment_added desc limit 0, 1) as last_comment,(select concat(user_name,' ',user_lname) from user_register where user_id=(select h.comment_user_id from complaint_comment h where a.cust_id=h.comment_complaint_id ORDER BY h.comment_added desc limit 0, 1)) as commented_by,(select count(comment_status)  from complaint_comment where a.cust_id=comment_complaint_id and comment_status=0) as unread_comments,(select h.comment_added from complaint_comment h where a.cust_id=h.comment_complaint_id ORDER BY h.comment_added desc limit 0, 1) as dt_added from user_complain a left join complaint_assigned b on a.cust_id=b.comp_cust_id left join complaint_section d on a.section=d.comp_sec_id left join complaint_category e on a.cust_id=e.comp_cust_id left join user_register g on b.comp_user_id=g.user_id order by a.cust_id desc");
	  return $sql->result_array();
    }
	
	#This function is used to delete the complain
	public function del_comp($id){
	$sql=$this->db->delete('user_complain',array('cust_id'=>$id));
	$query=$this->db->delete('complaint_assigned',array('comp_cust_id'=>$id));
	return $sql;
	//echo $sql; exit;
	}
	
	#This function is used to edit the desired complain
	public function edit_comp($cust_id=null){
	    #$sql="SELECT a.*,b.comp_cat_id,b.comp_user_id,c.cat_name,d.user_name from user_complain a left join complaint_assigned b on a.cust_id=b.comp_cust_id left join comp_cat c on b.comp_cat_id=c.comp_cat_id left join user_register d on b.comp_user_id=d.user_id where a.cust_id=".$cust_id;
		#echo $sql;die;
		$sql="SELECT a.*,g.comp_cat_id,b.comp_user_id,b.comp_resolved,if(b.comp_added>b.comp_modified,b.comp_added,b.comp_modified) as assigned_date,GROUP_CONCAT(distinct(c.cat_name)) as cat_name,if(g.category_added>g.category_modified,g.category_added,g.category_modified) as category_date,d.user_name,e.comp_assist_id,if(e.comp_assist_added>e.comp_assist_modified,e.comp_assist_added,e.comp_assist_modified) as attached_date,GROUP_CONCAT(distinct(f.user_name)) as attached,(select comment_added from complaint_comment  order by comment_added desc limit 1) as comment_date,(select group_concat(img_id) from user_complain_images where cust_id=a.cust_id) as img_id,(select group_concat(img_path) from user_complain_images where cust_id=a.cust_id) as images from user_complain a left join complaint_assigned b on a.cust_id=b.comp_cust_id left join complaint_category g on g.comp_cust_id=a.cust_id left join comp_cat c on FIND_IN_SET(c.comp_cat_id,g.comp_cat_id) left join user_register d on b.comp_user_id=d.user_id left join complaint_attached e on a.cust_id=e.comp_cust_id left join user_register f on FIND_IN_SET(f.user_id,e.comp_assist_id) where a.cust_id=".$cust_id;
		#echo $sql;die;
		$result=$this->db->query($sql);
		return $result->row_array();
		#print_r($result->row_array());die; 
	   }

	#This function displays the list of department heads to which the complaints can be assigned.
	public function show_user($val){
	/*$this->db->select('user_id,user_name');
	$this->db->get_where(array('role'=>'0','role'=>'1'));
	$this->db->like(array('cat_sel'=>$val));*/#Commented by sumith because of modifications
	$sql=$this->db->query("SELECT user_id,user_name from user_register where cat_sel like'%".$val."%' and role!=2");
	#echo $sql;die;
	return $sql->result_array();
	}

	#This function will check for the number in database
	public function check_mobile($mno){
	$this->db->select('cust_name,cust_lastname,cust_email');
	$sql=$this->db->get_where('user_complain',array('cust_mobile'=>$mno));
		if($sql->num_rows()>0){	
			return $sql->row_array();
		}
		else{
		return false;
		}	
	}

	#This function will retrieve the number of unassigned complaints
	public function get_unassigned(){
	$sql="SELECT count(comp_user_id) as comp_user_id from complaint_assigned where comp_user_id=0";
	$result=$this->db->query($sql);
	return $result->row_array();
	}
	
	#This function will retrieve the number of assigned complaints
	public function get_assigned(){
	#$sql="SELECT count(comp_user_id) as comp_user_id from complaint_assigned where comp_user_id!=0";
	$sql="select count(b.comp_user_id) as comp_user_id from user_register a join complaint_assigned b on a.user_id=b.comp_user_id where a.role!=1 and b.comp_user_id!=0";
	$result=$this->db->query($sql);
	return $result->row_array();
	}

	#This function will retrieve the number of assigned complaints
    public function assignedtome(){
    $sql="select count(b.comp_user_id) as comp_user_id from user_register a join complaint_assigned b on a.user_id=b.comp_user_id where a.role=1";
    $result=$this->db->query($sql);
    return $result->row_array();
    }

    #This function is used to load all the details of respective complaints
    public function load_comments($complain){
    #$sql=$this->db->query("SELECT a.*,b.user_id,b.user_name from  complaint_comment a left join user_register b on a.comment_user_id=b.user_id where a.comment_complaint_id=".$complain);
	#$sql=mysql_query("select a.date_added,a.ticket_no,a.cust_id,a.cust_message,o.comp_cat_id,if(o.category_added>o.category_modified,o.category_added,o.category_modified) as category_added_date,b.comp_user_id,if(b.comp_added>b.comp_modified,b.comp_added,b.comp_modified) as assigned_to_date,c.cat_name,c.comp_cat_id,concat(d.user_name,' ',d.user_lname) as complain_added_by,e.comment_desc,e.comment_id,e.comment_user_id,concat(f.user_name,' ',f.user_lname) as commented_by,e.comment_added,g.comp_assist_id,if(g.comp_assist_added>g.comp_assist_modified,g.comp_assist_added,g.comp_assist_modified) as attached_to_date,h.user_id as assistant_id,concat(h.user_name,' ',h.user_lname) as assistant_name,i.comp_user_id as prev_ass_id,i.comp_record_id as assigned_recorded,i.comp_record_added,if(i.comp_added>i.comp_modified,i.comp_added,i.comp_modified) as prev_assigned_to_date,concat(j.user_name,' ',j.user_lname) as prev_ass_name,k.comp_record_id,k.comp_record_added,k.comp_assist_id as prev_attached_id,if(k.comp_assist_added>k.comp_assist_modified,k.comp_assist_added,k.comp_assist_modified) as prev_attached_to_date,concat(l.user_name,' ',l.user_lname) as prev_attach_name,l.user_id as prev_attach_id,m.comp_user_id,concat(n.user_name,' ',n.user_lname) as current_assigned,n.user_id as current_assigned_id,p.cat_record_id,p.comp_cat_id as prev_cat_id,if(p.category_added>p.category_modified,p.category_added,p.category_modified) as prev_cat_date,q.cat_name as prev_cat_name from user_complain a left join complaint_assigned b on a.cust_id=b.comp_cust_id left join complaint_category o on o.comp_cust_id=a.cust_id left join comp_cat c on FIND_IN_SET(c.comp_cat_id,o.comp_cat_id) left join user_register d on a.complain_added_by=d.user_id left join complaint_comment e on e.comment_complaint_id=a.cust_id left join user_register f on e.comment_user_id=f.user_id left join complaint_attached g on g.comp_cust_id=a.cust_id left join user_register h on FIND_IN_SET(h.user_id,g.comp_assist_id) left join complaint_assigned_record i on i.comp_cust_id=a.cust_id left join user_register j on i.comp_user_id=j.user_id left join complaint_attached_record k on k.comp_cust_id=a.cust_id left join user_register l on FIND_IN_SET(l.user_id,k.comp_assist_id) left join complaint_assigned m on m.comp_cust_id=a.cust_id left join user_register n on m.comp_user_id=n.user_id left join complaint_category_record p on p.comp_cust_id=a.cust_id left join comp_cat q on FIND_IN_SET(q.comp_cat_id,p.comp_cat_id)  where a.cust_id=43");
	$sql=mysql_query("select a.date_added,a.ticket_no,a.cust_id,a.cust_message,o.comp_cat_id,if(o.category_added>o.category_modified,o.category_added,o.category_modified) as category_added_date,b.comp_user_id,if(b.comp_added>b.comp_modified,b.comp_added,b.comp_modified) as assigned_to_date,(select GROUP_CONCAT(c.cat_name) from comp_cat c where FIND_IN_SET(c.comp_cat_id,o.comp_cat_id) ) as cat_name,t.comp_cat_added_by,concat(u.user_name,' ',u.user_lname)  as assigned_category,concat(d.user_name,' ',d.user_lname) as complain_added_by,e.comment_desc,e.comment_id,e.comment_user_id,concat(f.user_name,' ',f.user_lname) as commented_by,e.comment_added,g.comp_assist_id as assistant_id,g.comp_assist_added_by,if(g.comp_assist_added>g.comp_assist_modified,g.comp_assist_added,g.comp_assist_modified) as attached_to_date,(select GROUP_CONCAT(concat(h.user_name,' ',h.user_lname)) from user_register h where FIND_IN_SET(h.user_id,g.comp_assist_id) ) as assistant_name,concat(s.user_name,' ',s.user_lname)  as assigned_assistant,i.comp_user_id as prev_ass_id,i.comp_record_id as assigned_recorded,i.comp_record_added,if(i.comp_added>i.comp_modified,i.comp_added,i.comp_modified) as prev_assigned_to_date,i.comp_assigned_by,concat(v.user_name,' ',v.user_lname) as assigned_admin_record,concat(j.user_name,' ',j.user_lname) as prev_ass_name,k.comp_record_id,k.comp_record_added,k.comp_assist_id as prev_attached_id,if(k.comp_assist_added>k.comp_assist_modified,k.comp_assist_added,k.comp_assist_modified) as prev_attached_to_date,(select GROUP_CONCAT(concat(l.user_name,' ',l.user_lname)) from user_register l where FIND_IN_SET(l.user_id,k.comp_assist_id)) as prev_attach_name,w.comp_assist_added_by,concat(x.user_name,' ',x.user_lname)  as assigned_assistant_record,m.comp_user_id,m.comp_assigned_by,concat(n.user_name,' ',n.user_lname) as current_assigned,concat(r.user_name,' ',r.user_lname)  as assigned_admin,n.user_id as current_assigned_id,p.cat_record_id,p.comp_cat_id as prev_cat_id,if(p.category_added>p.category_modified,p.category_added,p.category_modified) as prev_cat_date,(select GROUP_CONCAT(q.cat_name) from comp_cat q where FIND_IN_SET(q.comp_cat_id,p.comp_cat_id)) as prev_cat_name,y.comp_cat_added_by,concat(z.user_name,' ',z.user_lname)  as assigned_category_record,a1.cust_record_added,a1.complain_edited_by,concat(b1.user_name,' ',b1.user_lname) as complain_edited_name from user_complain a left join complaint_assigned b on a.cust_id=b.comp_cust_id left join complaint_category o on o.comp_cust_id=a.cust_id left join user_register d on a.complain_added_by=d.user_id left join complaint_comment e on e.comment_complaint_id=a.cust_id left join user_register f on e.comment_user_id=f.user_id left join complaint_attached g on g.comp_cust_id=a.cust_id left join complaint_assigned_record i on i.comp_cust_id=a.cust_id left join user_register j on i.comp_user_id=j.user_id left join complaint_attached_record k on k.comp_cust_id=a.cust_id left join complaint_assigned m on m.comp_cust_id=a.cust_id left join user_register n on m.comp_user_id=n.user_id left join complaint_category_record p on p.comp_cust_id=a.cust_id left join user_register r on r.user_id=m.comp_assigned_by left join user_register s on s.user_id=g.comp_assist_added_by left join complaint_category t on t.comp_cust_id=a.cust_id left join user_register u on u.user_id=t.comp_cat_added_by left join user_register v on v.user_id=i.comp_assigned_by left join complaint_attached_record w on w.comp_cust_id=a.cust_id left join user_register x on w.comp_assist_added_by=x.user_id left join complaint_category_record y on y.comp_cust_id=a.cust_id left join user_register z on y.comp_cat_added_by=z.user_id left join user_complain_record a1 on a.cust_id=a1.cust_id left join user_register b1 on a1.complain_edited_by=b1.user_id where a.cust_id='".$complain."'");
	while($row=mysql_fetch_assoc($sql)){
	$data[$row['date_added']][$row['cust_id']]['date']=$row['date_added'];
	$data[$row['date_added']][$row['cust_id']]['complain_added_by']=$row['complain_added_by'];
	$data[$row['date_added']][$row['cust_id']]['ticket_no']=$row['ticket_no'];
	$data[$row['comment_added']][$row['comment_user_id']]['date']=$row['comment_added'];
	$data[$row['comment_added']][$row['comment_user_id']]['user']=$row['commented_by'];
	$data[$row['comment_added']][$row['comment_user_id']]['comment']=$row['comment_desc'];
	$data[$row['assigned_to_date']][$row['current_assigned_id']]['current_assigned']=$row['current_assigned'];
	$data[$row['assigned_to_date']][$row['current_assigned_id']]['date']=$row['assigned_to_date'];
	$data[$row['assigned_to_date']][$row['current_assigned_id']]['assigned_admin']=$row['assigned_admin'];
	$data[$row['attached_to_date']][$row['assistant_id']]['current_attached']=$row['assistant_name'];
	$data[$row['attached_to_date']][$row['assistant_id']]['date']=$row['attached_to_date'];
	$data[$row['attached_to_date']][$row['assistant_id']]['assigned_assistant']=$row['assigned_assistant'];
	$data[$row['category_added_date']][$row['comp_cat_id']]['ass_dept']=$row['cat_name'];
	$data[$row['category_added_date']][$row['comp_cat_id']]['date']=$row['category_added_date'];
	$data[$row['category_added_date']][$row['comp_cat_id']]['assigned_category']=$row['assigned_category'];
	$data[$row['prev_assigned_to_date']][$row['assigned_recorded']]['prev_ass_name']=$row['prev_ass_name'];
	$data[$row['prev_assigned_to_date']][$row['assigned_recorded']]['date']=$row['prev_assigned_to_date'];
	$data[$row['prev_assigned_to_date']][$row['assigned_recorded']]['assigned_admin_record']=$row['assigned_admin_record'];
	$data[$row['prev_attached_to_date']][$row['prev_attached_id']]['prev_attached_name']=$row['prev_attach_name'];
	$data[$row['prev_attached_to_date']][$row['prev_attached_id']]['date']=$row['prev_attached_to_date'];
	$data[$row['prev_attached_to_date']][$row['prev_attached_id']]['assigned_assistant_record']=$row['assigned_assistant_record'];
	$data[$row['prev_cat_date']][$row['cat_record_id']]['prev_cat_name']=$row['prev_cat_name'];
	$data[$row['prev_cat_date']][$row['cat_record_id']]['date']=$row['prev_cat_date'];
	$data[$row['prev_cat_date']][$row['cat_record_id']]['assigned_category_record']=$row['assigned_category_record'];
	$data[$row['cust_record_added']][$row['complain_edited_by']]['complain_edited_name']=$row['complain_edited_name'];
	$data[$row['cust_record_added']][$row['complain_edited_by']]['date']=$row['cust_record_added'];
	}
	#echo '<pre>';
	#print_r($data);
	return $data;
	#print_R(array_unique(explode(',',($a['category']))));die;
	#print_R($a);die;
    }

	#This function will send the mail to the user.
	public function sendmail($to,$subject,$message){
		$this->load->library('email');
		$config['protocol']='smtp';
		$config['smtp_host']='ssl://smtp.googlemail.com';
		$config['smtp_port']='465';
		$config['smtp_timeout']='30';
		$config['smtp_user']='infotestng@gmail.com';
		$config['smtp_pass']='9757252991';
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from('infotestng@gmail', 'Grievance');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		#$this->email->send();
		if($this->email->send())
         {
          return true;
         }
         else
        {
         show_error($this->email->print_debugger());
        }
	}
	
	#This function gives the no of unread comments
	public function unread_comments(){
	$tmp=$this->session->userdata('logged_in');
	if($tmp['status']==1){
	$sql=$this->db->query('SELECT count(comment_status) as unread from complaint_comment where comment_status=0');
	}
	elseif($tmp['status']==0){
	$sql=$this->db->query("SELECT count(a.comment_status) as unread from complaint_comment a left join complaint_assigned b on a.comment_complaint_id=b.comp_cust_id where a.comment_status=0 and b.comp_user_id='".$tmp['id']."'");
	}
	else{
	$sql=$this->db->query("SELECT count(a.comment_status) as unread from complaint_comment a left join complaint_attached b on a.comment_complaint_id=b.comp_cust_id where a.comment_status=0 and FIND_IN_SET('".$tmp['id']."',b.comp_assist_id)");
	}
	return $sql->row_array();
	}
	
	#This function gives the no of unread comments
	public function unread_complaints(){
	$tmp=$this->session->userdata('logged_in');
	if($tmp['status']==1){
	$sql=$this->db->query('SELECT count(comp_status) as unread_comp from complaint_assigned where comp_status=0');
	}
	elseif($tmp['status']==0){
	$sql=$this->db->query("select count(comp_status) as unread_comp from complaint_assigned where comp_status=0 and comp_user_id='".$tmp['id']."'");
	}
	else{
	$sql=$this->db->query("select count(a.comp_status) as unread_comp,b.comp_assist_id from complaint_assigned a left join complaint_attached b on a.comp_cust_id=b.comp_cust_id where a.comp_status=0 and FIND_IN_SET('".$tmp['id']."',b.comp_assist_id)");
	}
	return $sql->row_array();
	}
	
	#This function will update the status of complaint and comment to read status 
	public function updatestatus($compid){
	$sql=$this->db->query('update complaint_comment set comment_status=1 where comment_complaint_id='.$compid);
	$sql1=$this->db->query('update complaint_assigned set comp_status=1 where comp_cust_id='.$compid);
		if($sql or $sql1){
		return true;
		}
		
	}
	
	#This function will show the assistants
	public function show_assistant($aid){
	$sql=$this->db->query("SELECT user_id,user_name from user_register where cat_sel like'%".$aid."%' and role=2");
	return $sql->result_array();
	}
	
	#This function will show all the sections
	public function show_section(){
	$sql=$this->db->get('complaint_section');
	return $sql->result_array(); 
	}
	
	#This function will fetch the data if the complain has been edited.
	public function edited($data,$userid){
	$query=$this->db->get_where('user_complain',array('cust_id'=>$data['cust_id']))->row_array();
	$a='';		
		if($query){
		$sql="insert into user_complain_record set cust_id='".$data['cust_id']."',date_added='".$query['date_added']."',date_modified='".$query['date_modified']."',complain_added_by='".$query['complain_added_by']."',complain_edited_by='".$userid."'";
			if($query['cust_name']!=$data['cust_name']){
			$sql.=",cust_name='".$query['cust_name']."'";
			$a=true;
			}
			if($query['cust_lastname']!=$data['cust_lastname']){
			$sql.=",cust_lastname='".$query['cust_lastname']."'";
			$a=true;
			}
			if($query['cust_mobile']!=$data['cust_mobile']){
			$sql.=",cust_mobile='".$query['cust_mobile']."'";
			$a=true;
			}
			if($query['cust_date']!=$data['cust_date']){
			$sql.=",cust_date='".$query['cust_date']."'";
			$a=true;
			}
			if($query['cust_message']!=$data['cust_message']){
			$sql.=",cust_message='".$query['cust_message']."'";
			$a=true;
			}
			if($query['cust_email']!=$data['cust_email']){
			$sql.=",cust_email='".$query['cust_email']."'";
			$a=true;
			}
			if($query['section']!=$data['section']){
			$sql.=",section='".$query['section']."'";
			$a=true;
			}
			if($a){
			return $sql;
			}
			else{
			return false;
			}
			#$this->db->query($sql);
		}
	}
	
	#This function will delete the image from the database which could be excel,doc or image file.
	public function del_image($id){
	$sql=$this->db->delete('user_complain_images', array('img_id' => $id)); 
	return $sql;
	}
	
	public function comp_status($stats,$cust_id){
	#echo "update complaint_assigned set comp_resolved='".$stats."' where comp_cust_id=".$cust_id;die;
	return $result=$this->db->query("update complaint_assigned set comp_resolved='".$stats."' where comp_cust_id='".$cust_id."'");
	}
}