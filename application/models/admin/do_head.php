<?php 
	#Author:Sumith Nalwala
	#Date:19/03/3015
	#If you are making any changes then please mention the date and comment the changes
		
	class Do_Head extends CI_Model{
		function __Construct(){
		parent::__construct();
		}
	
	#Date:26/03/3015
	#This function will check for the password.
	public function checkpwd($data){
	$sql=$this->db->get_where('user_register',array('user_id'=>$data['id'],'password'=>md5($data['pwd'])));
		if($sql->num_rows() > 0){
			return true;
			}
	} 
	
	#Date:26/03/3015
	#This function will update the password of the user.
	public function updatedetails($data1){
	$tmp=$this->session->userdata('logged_in');
	$id=$tmp['id'];
	$this->db->update('user_register', $data1, array('user_id' => $id));
	redirect('admin/head/userdetails_form');
	}
	
	public function complaint_assigned_list($cid){
	#$tmp=$this->session->userdata('logged_in');
	$id=$cid;
	#$sql='SELECT a.*,b.* from complaint_assigned a join user_complain b on a.comp_cust_id=b.cust_id where a.comp_user_id='.$id;
	#$sql="SELECT a.*,b.*,(select count(c.comment_complaint_id) from complaint_comment c where c.comment_complaint_id=b.cust_id) as comments from complaint_assigned a join user_complain b on a.comp_cust_id=b.cust_id where a.comp_user_id=".$id;
	$sql="SELECT a.*,b.comp_user_id,b.comp_status,(select count(c.comment_complaint_id) from complaint_comment c where a.cust_id=c.comment_complaint_id) as comments,d.complaint_section as section_name,e.comp_cat_id,(select GROUP_CONCAT(cat_name) from comp_cat f where FIND_IN_SET(f.comp_cat_id,e.comp_cat_id)) as cat_name,concat(g.user_name,' ',g.user_lname) as assigned_to,g.designation,g.image as assigned_image,(select h.comment_desc from complaint_comment h where a.cust_id=h.comment_complaint_id ORDER BY h.comment_added desc limit 0, 1) as last_comment,(select concat(user_name,' ',user_lname) from user_register where user_id=(select h.comment_user_id from complaint_comment h where a.cust_id=h.comment_complaint_id ORDER BY h.comment_added desc limit 0, 1)) as commented_by,(select count(comment_status)  from complaint_comment where a.cust_id=comment_complaint_id and comment_status=0) as unread_comments,(select h.comment_added from complaint_comment h where a.cust_id=h.comment_complaint_id ORDER BY h.comment_added desc limit 0, 1) as date_added from user_complain a left join complaint_assigned b on a.cust_id=b.comp_cust_id left join complaint_section d on a.section=d.comp_sec_id left join complaint_category e on a.cust_id=e.comp_cust_id left join user_register g on b.comp_user_id=g.user_id where b.comp_user_id='".$id."' order by a.cust_id desc"; 
	#echo $sql;die;
	$result=$this->db->query($sql);
	return $result->result_array();
	}

	public function complaint_attached_list($aid){
	#$tmp=$this->session->userdata('logged_in');
	$id=$aid;
	#$sql="SELECT a.*,b.*,(select count(c.comment_complaint_id) from complaint_comment c where c.comment_complaint_id=b.cust_id) as comments,d.* from complaint_assigned a join user_complain b on a.comp_cust_id=b.cust_id left join complaint_attached d on a.comp_cust_id=d.comp_cust_id where FIND_IN_SET(".$id.",d.comp_assist_id)";
	$sql="SELECT a.*,b.comp_user_id,b.comp_status,(select count(c.comment_complaint_id) from complaint_comment c where a.cust_id=c.comment_complaint_id) as comments,d.complaint_section as section_name,e.comp_cat_id,(select GROUP_CONCAT(cat_name) from comp_cat f where FIND_IN_SET(f.comp_cat_id,e.comp_cat_id)) as cat_name,concat(g.user_name,' ',g.user_lname) as assigned_to,g.designation,g.image as assigned_image,(select h.comment_desc from complaint_comment h where a.cust_id=h.comment_complaint_id ORDER BY h.comment_added desc limit 0, 1) as last_comment,(select concat(user_name,' ',user_lname) from user_register where user_id=(select h.comment_user_id from complaint_comment h where a.cust_id=h.comment_complaint_id ORDER BY h.comment_added desc limit 0, 1)) as commented_by,(select count(comment_status) from complaint_comment where a.cust_id=comment_complaint_id and comment_status=0) as unread_comments,(select h.comment_added from complaint_comment h where a.cust_id=h.comment_complaint_id ORDER BY h.comment_added desc limit 0, 1) as date_added,i.comp_assist_id from user_complain a left join complaint_assigned b on a.cust_id=b.comp_cust_id left join complaint_section d on a.section=d.comp_sec_id left join complaint_category e on a.cust_id=e.comp_cust_id left join user_register g on b.comp_user_id=g.user_id left join complaint_attached i on i.comp_cust_id=a.cust_id where FIND_IN_SET(".$id.",i.comp_assist_id) order by a.cust_id desc";
	$result=$this->db->query($sql);
	return $result->result_array();
	}

}