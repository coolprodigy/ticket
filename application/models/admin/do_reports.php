<?php 
	class Do_reports extends CI_Model{
		function __Construct(){
		parent::__construct();
		}
	
	#This function will add the category into teh database.

	public function generate($extended){
		$sql="SELECT a.section,(select group_concat(cust_id) from user_complain where section=a.section) as ids,b.comp_user_id as user,(select count(comp_resolved) from complaint_assigned where comp_resolved=0 and comp_user_id=b.comp_user_id and find_in_set(comp_cust_id,ids)) as pending,(select count(comp_resolved) from complaint_assigned where comp_resolved=1 and comp_user_id=b.comp_user_id and find_in_set(comp_cust_id,ids)) as resolved,(select count(comp_resolved) from complaint_assigned where comp_user_id=b.comp_user_id and find_in_set(comp_cust_id,ids)) as total from user_complain a left join complaint_assigned b on a.cust_id=b.comp_cust_id where a.section=1 and b.comp_user_id=b.comp_user_id";
		$sql .=$extended;
		#echo $sql;
		$result=mysql_query($sql);
		$num_rows = mysql_num_rows($result);
			if($num_rows==0){
				$msg="No Records Found";
				$this->session->set_flashdata('error',$msg);
				redirect(ADMIN_VIEW.'reports');
			}
		#echo $num_rows;die;
		#var_dump($result);die;
		$data='';
		$query="select report_id from report_metadata order by report_id desc limit 1";
		$reportid=$this->db->query($query);
		$id = $reportid->num_rows();
		$idval=$reportid->row_array();
		if($id>0){
		$id=$idval['report_id']+1;
		}
		else{
		$id=$id+1;
		}
		while($row=mysql_fetch_assoc($result)){
			#var_dump($row);die;
			$data .="(null,$id,$row[section],$row[user],$row[pending],$row[resolved],$row[total]),";
		} 
		$data=substr($data,0,-1);
		$sql1="insert ignore into reports(id,report_id,section,user,pending,resolved,total) values ".$data;
		$this->db->query($sql1);
		$tmp=$this->session->userdata('logged_in');
		$sql3="insert into report_metadata(report_id,generated_by) values($id,$tmp[id])";
		$this->db->query($sql3);
		return $report_id=$this->db->query("select report_id from report_metadata order by report_id desc")->row_array();
	}

	public function getreport($res){
	$sql="select a.*,b.*,concat(c.user_name,' ',c.user_lname)as name,d.complaint_section,d.section_cell,(select sum(e.total) from reports e where e.report_id='".$res['report_id']."' and e.user=a.user) as alltotal,(select sum(f.resolved) from reports f where f.report_id='".$res['report_id']."' and f.user=a.user) as allresolved,(select sum(g.pending) from reports g where g.report_id='".$res['report_id']."' and g.user=a.user) as allpending from reports a left join report_metadata b on a.report_id=b.report_id left join user_register c on c.user_id=a.user left join complaint_section d on d.comp_sec_id=a.section where a.report_id='".$res['report_id']."'";
	#echo $sql;die;
	$result=mysql_query($sql);
	#echo $result;die;
	return $result;	
	}
	 
	 #This function is used to retrieve the distinct ids(if the id is repeated) and attach the appropriate excel cell to the ids. 
	 public function getuserid($res){
	 $sql="select distinct(a.user) from reports a where a.report_id='".$res['report_id']."'";
	 return $this->db->query($sql)->result_array();
	 }

	#THis function is used to insert the report filename into the database for future reference.
	public function insertpath($filepath,$res){
	 return $sql=$this->db->query("update report_metadata set file_path='".$filepath."' where report_id=".$res['report_id']); 
	}
	
}	