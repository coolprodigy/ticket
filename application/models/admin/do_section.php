<?php 
	#Author:Sumith Nalwala
	#Date:19/03/3015
	#If you are making any changes then please mention the date and comment the changes
		
	class Do_Section extends CI_Model{
		function __Construct(){
		parent::__construct();
		}
	
	#This function will add the category into teh database.

	public function addsection($data1){
		$sql="insert into complaint_section(comp_sec_id,complaint_section) values('".$data1['comp_sec_id']."','".$data1['complaint_section']."') on duplicate key update complaint_section='".$data1['complaint_section']."'";
		$this->db->query($sql);
		redirect('admin/section/list_section');
	}  
	
	#This function retrieves all the categories from the database.
	public function get_sec_list(){
		$sql=$this->db->get('complaint_section');
		return $sql->result_array();
	}
	
	#This function retrieves the desired row for editing.
	public function edit_sec($comp_sec_id){
	$sql=$this->db->get_where('complaint_section', array('comp_sec_id' => $comp_sec_id));
	return $sql->row_array();
	}
	
	#This function is used for deleting the category from the database.
	public function del_sec($id){
	$sql=$this->db->delete('complaint_section',array('comp_sec_id'=>$id));
	return $sql;
	}
}