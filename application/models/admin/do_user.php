<?php 
	#Author:Sumith Nalwala
	#Date:19/03/3015
	#If you are making any changes then please mention the date and comment the changes
	class Do_User extends CI_Model{
		function __Construct(){
		parent::__construct();
	}

	#This function will insert the user record into the database and on duplicate it will update the record.
	public function adduser($data){
		$sql="insert into user_register(user_id,user_name,user_lname,user_email,user_mobile,password,role,designation,cat_sel,image) values('".$data['user_id']."','".$data['user_name']."','".$data['user_lname']."','".$data['user_email']."','".$data['user_mobile']."','".$data['password']."','".$data['role']."','".$data['designation']."','".$data['cat_sel']."','".$data['image']."') on duplicate key update role='".$data['role']."',user_name='".$data['user_name']."',user_lname='".$data['user_lname']."',cat_sel='".$data['cat_sel']."',user_mobile='".$data['user_mobile']."',image='".$data['image']."'";
		#echo $sql;die;
		$result=$this->db->query($sql);
		return $result;
		
	}
	
	#This function will get all the result
	public function get_user_list($srch=null){
		#$sql=$this->db->get("user_register");
		#$sql=$this->db->query("SELECT a.*,(select count(b.comp_user_id) from complaint_assigned b where a.user_id=b.comp_user_id) as assigned from user_register a");
		$sql=$this->db->query("SELECT a.*,(select count(b.comp_user_id) from complaint_assigned b where a.user_id=b.comp_user_id) as assigned,(select count(comment_user_id) from complaint_comment c where a.user_id=c.comment_user_id) as comments from user_register a where a.user_name like '".$srch."%'");
		return $sql->result_array();
	}
	
	#This function will delete the user record.
	public function del_user($id){
	$sql=$this->db->delete('user_register',array('user_id'=>$id));
	$data=array('comp_user_id'=>'0');
	$this->db->where('comp_user_id', $id);
	$query=$this->db->update('complaint_assigned',$data);
	return $sql;
	}
	
	#This function is used for editing the user record.
	public function edit_user($id){
	$sql=$this->db->get_where('user_register',array('user_id'=>$id));
	return $sql->row_array();

	}

	#This function will display all the comment in the last inserted .
	public function allcomments(){
	$sql=$this->db->query("SELECT a.*,concat(b.user_name,' ',b.user_lname) as name FROM complaint_comment a left join user_register b on a.comment_user_id=b.user_id order by  comment_id desc");
	return $sql->result_array();
	} 
 
	public function count_user(){
	$sql=$this->db->query("select count(user_id) as total from user_register");
	return $sql->row_array();
	}
 }
?>