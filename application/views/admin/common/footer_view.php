<!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Softpillar.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    
    <script src="<?php echo ADMIN_URL;?>js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo ADMIN_URL;?>js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo ADMIN_URL;?>js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo ADMIN_URL;?>js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="<?php echo ADMIN_URL;?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="<?php echo ADMIN_URL;?>js/owl.carousel.js" ></script>
    <script src="<?php echo ADMIN_URL;?>js/jquery.customSelect.min.js" ></script>
    <script src="<?php echo ADMIN_URL;?>js/respond.min.js" ></script>

    <!--right slidebar-->
    <script src="<?php echo ADMIN_URL;?>js/slidebars.min.js"></script>

    <!--common script for all pages-->
    <script src="<?php echo ADMIN_URL;?>js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="<?php echo ADMIN_URL;?>js/sparkline-chart.js"></script>
    <script src="<?php echo ADMIN_URL;?>js/easy-pie-chart.js"></script>
    <script src="<?php echo ADMIN_URL;?>js/count.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
  <script>

      //owl carousel

      $(document).ready(function() {
		  $.fn.dataTable.ext.errMode = 'throw';
         $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
		  
		$('#dttable').DataTable( {
		 "aaSorting": [],
		"oLanguage": {
		  "sUrl": "<?php echo ADMIN_URL;?>plugins/language/hindi.txt"
		}
	});
		  
      });
  
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>
 <script>
 $(function() {
  $( "#date,#date1" ).datepicker({dateFormat:'yy-mm-dd',
  /* minDate:new Date(),
   maxDate:0,*/	
 });
  });

</script>
  </body>
</html>