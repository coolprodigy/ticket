<!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-9">
                      <section class="panel">
                          <header class="panel-heading">
                            Add Complain
							<?php 
							$tmp=$this->session->userdata('logged_in');
                            if(isset($edit_data)){
                            extract($edit_data);
							$assistantid=explode(',',$comp_assist_id);
							#$assistantname=explode(',',$attached);
							#print_r($assistant);
							#print_r($edit_data);die;
                            if($tmp['status']==1){   ?>
                            <div style="clear:both;float:right;"><a onclick='edit();'><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></div>
                            <?php } } ?>

                          </header>
                          <div class="panel-body">
                          <form  action="<?php echo ADDCOMPLAIN;?>" method="POST" enctype="multipart/form-data" id='addcomplain' <?php if($tmp['status']==2){?> onsubmit='return makeread()' <?php } ?>>
						  <input type='hidden' value='<?=isset($cust_id)?$cust_id:''?>' name='cust_id' id='cust_id'>
							 <div class="form-group">
                                      <label>Mobile No.</label>
                                      <input type="text" class="form-control" name="cust_mobile" id="cust_mobile" value="<?=isset($cust_mobile)?$cust_mobile:''?>" placeholder="Enter Mobile Number">
									  <?php echo form_error('cust_mobile');?>
                                  </div>
							 <div class="form-group">
                                      <label>First Name</label>
                                      <input type="text" class="form-control" id="cust_name" name="cust_name" value="<?=isset($cust_name)?$cust_name:''?>" size="50" placeholder="Enter Your First Name">
									  <?php echo form_error('cust_name');?>
                                  </div>
								  <div class="form-group">
                                      <label>Last Name</label>
                                      <input type="text" class="form-control" id="cust_lastname" value="<?=isset($cust_lastname)?$cust_lastname:''?>" name="cust_lastname" placeholder="Enter Your Last Name">
									  <?php echo form_error('cust_lastname');?>
                                  </div>
								  <div class="form-group">
                                      <label>Email </label>
                                      <input type="text" class="form-control" id="cust_email" name="cust_email" value="<?=isset($cust_email)?$cust_email:''?>" placeholder="Enter Email" >
									  <?php echo form_error('cust_email');?>
                                  </div>
								  <div class="form-group">
                                      <label>Date</label>
                                      <input type="text" class="form-control" name="cust_date" id="date" value="<?=isset($cust_date)?$cust_date:''?>" placeholder="Enter date" readonly required>
									<?php echo form_error('cust_date');?>
                                  </div>
								  <div class="form-group">
                                      <label >Subject</label>
                                      <input type="text" class="form-control" name="cust_subject" id="cust_subject" value="<?=isset($cust_subject)?$cust_subject:''?>" placeholder="Enter Subject">
									  <?php echo form_error('cust_subject');?>
                                  </div>
								  <div class="form-group">
                                  <label>Gist Of Compalaint</label>
                                  </div>
								  <div class="form-group">
									   <textarea  name="cust_message"  id="cust_message" class="span12 ckeditor  m-wrap" cols="70" rows="5"><?=isset($cust_message)?$cust_message:''?> 
									  </textarea>
									  <?php echo form_error('cust_message');?>
                                  </div>
                                  <div class="form-group">
                                      <label>Attach File</label>
									  <input type="file" name="image" id="image">
                                     <?php if(isset($image)){?><p class="help-block"><label>Existing File:</label><?=($image=='')?'No file Attached':'File attached'?></p> <?php }?>
                                  </div>
								  <div class="form-group">
								   <label>Assign To:</label>
								  <label style='margin-left:150px;'>Select Department:</label>
								  <select name='sel_cat' id='sel_cat' onChange='sel_user();'>
								  <option value='' >All</option>
								  <?php $result=$this->do_category->get_cat_list();?>
								  <?php foreach($result as $row){?>
								  <option value='<?php echo $row['comp_cat_id'];?>'><?php echo $row['cat_name'];?></option>
								  <?php } ?>
								  </select>
								  </div>
                                  <div class="form-group">
								  <select name='seluser' id='seluser'>
								  <option value='0'>Unassigned</option>
								  </select>
								  <?php echo form_error('seluser');?>
								  </div>
								  <div class="form-group">
                                  <label>Attach Assistant</label>
                                  </div>
								   <div class="form-group"> 
								   <label >Attach To:</label>
								   <label style='margin-left:150px;'>Select Department:</label>
								   <select name='sel_cat' id='sel_cat1' onChange='sel_assist();'>
								  <option value='' >All</option>
								  <?php $result=$this->do_category->get_cat_list();?>
								  <?php foreach($result as $row){?>
								  <option value='<?php echo $row['comp_cat_id'];?>'><?php echo $row['cat_name'];?></option>
								  <?php } ?>
								  </select>
								   </div>
									<div class="form-group"> 
								   <select name='selassist[]' id='selassist' multiple></select>
								   </div>
								   <div class="form-group">
                                  <label>Select Comment:</label>
                                  <select name='selcomment' id='selcomment'>
								  <option value='0'>Select Comment</option>
								  <option value='1'>Please action in 3 days</option>
								  <option value='2'>Please action in 5 days</option>
								  <option value='3'>Please action in 7 days</option>
								  </select>
								  </div>
								   <div class="form-group" >
                                  <label>Add Comment</label>
                                  </div>
                                  <div class="form-group"><textarea  name="comment"  id="comment" class="span12 ckeditor  m-wrap" cols="70" rows="5"></textarea></div>
                                  <input style='height:30px;width:80px;' type="submit" class="btn green" tabindex="16" id='submit' value='Submit'>
                              </form>
                          </div>
                      </section>
                  </div>
                
              </div>
              <!--afer
This section was previously used to load the comment section.
-->
                                    <!--<div class="time col-lg-9" style="display:none;">
                      <section class="panel">
                          <header class="panel-heading">
                              Comments
                              <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                          </header>
                          <div class="panel-body profile-activity" id='addcomments'>
                              

                          </div>
                      </section>
                  </div>-->
                                  <!--before-->

              <!-- page end-->
				 <!--timeline start-->
			   <div class="row" style="height:10px;"> </div>
                      <section class="panel" >
                          <div class="panel-body">
                                  <div class="text-center mbot30">
                                      <h3 class="timeline-title">History</h3>
                                      <p class="t-info">Records</p>
                                  </div>
					
                                  <div class="timeline" id='timeline'>
									</div>

                                  <div class="clearfix">&nbsp;</div>
                              </div>
                      </section>
			<!--Timeline ends-->
          </section>
      </section>

<!--Name
Author:Sumith Nalwala
-->  
  <script type='text/javascript'>
   $(document).ready(function(){
   sel_user();
   sel_assist();
   if($('#cust_id').val()!=''){
   $('#submit').val('Update');
   $('#sel_cat').val(<?=isset($comp_cat_id)?$comp_cat_id:''?>);
    $('.time').show();
	load_comments();
	
   }
});
/* It is used as a filter to select the user on basis of dropdown*/

	function sel_user(){
		var id=$('#sel_cat').val();
			$.ajax({
				    type:'POST',
					url:'<?=SITE_URL?>admin/complain/show_user',
					data:{option:id},   
			}).done(function(response){
				$('#seluser').html(response);
				 $('#seluser').val(<?=isset($comp_user_id)?$comp_user_id:'0'?>);
				 if($('#seluser').val()==null){
					$('#seluser').val('0');
				 }
					 
			});
	}

	function sel_assist(){
		var id=$('#sel_cat1').val();
			$.ajax({
			type:'POST',
			url:'<?=SITE_URL?>admin/complain/show_assistant',		
			data:{aid:id}	
			}).done(function(resp){
				$('#selassist').html(resp);
				<?php if(isset($assistantid)){?>
				var assist= <?php echo json_encode($assistantid)?>;
				$('#selassist').val(assist);
				<?php } ?>
		});	
	
	}

	

   </script>   
 <script type='text/javascript'>
/*To disable all the inputs of form using javascript*/
$(document).ready(function(){
 if($('#addcomplain :input').val()){
 $("#addcomplain :input").prop("readonly", true);
 $("#comment").prop("readonly", false);
	<?php 
	if($tmp['status']==2){?>
	$('select').attr("disabled", "disabled");
	$('#submit').prop("readonly",false);
	<?php } ?>
	
 }
});

/*After clicking on th option of edit this function is called*/
function edit(){
$("#addcomplain :input").prop("readonly", false);
}

/*#Author:Sumith Nalwala
Date:30/03/3015.
This function fetches the details of the complainer if is in the database*/
$(document).ready(function(){
$('#cust_mobile').blur(function(){
var mno=$('#cust_mobile').val();
    $.ajax({
        url:'<?=SITE_URL?>admin/complain/check_mobile',
        type:'post',
        data:{mno:mno}
    }).done(function(response){
        if(response!=false){
        var data=JSON.parse(response);
            if(confirm('The name of the complainer is '+data['cust_name'] +' '+'Do you want to fetch his details?')){
            $('#cust_name').val(data['cust_name']);
            $('#cust_lastname').val(data['cust_lastname']);
            $('#cust_email').val(data['cust_email']);
            }
        }    
            else{
            $('#cust_name').val("");
            $('#cust_lastname').val("");
            $('#cust_email').val("");
            }
            });
   });
	/*This is used to get the value from the dropdown and add it to the comment box it has been done using ternary operator
	Author:Sumith Nalwala:Date:8/04/2015
	*/
	$("#selcomment").change(function () {
			($('#selcomment').val()!=0?$('#comment').text($('#selcomment option:selected').text()):$('#comment').text(''));
	});

});

/*To laod the comments
Author:Sumith Nalwala
*/
function load_comments(){
var complain=$('#cust_id').val();
        $.ajax({
        type:'post',
        url:'<?=SITE_URL?>admin/complain/load_comments',
        data:{complain:complain}
        }).done(function(response){
        $('#timeline').html(response);
    });  
}

function makeread(){
 $("#addcomplain :input").prop("readonly", true);
 $('select').attr("disabled", false);
 return true;
 }

</script>
<!--Name
Author:Sumith Nalwala
-->      