<!--main content start-->
      <section id="main-content">
          <section class="wrapper">
             <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              List Of Copmlaints
                          </header>
						  <table class="table table-striped table-advance table-hover display " id='dttable' cellspacing="0" width="100%">
                              <thead>
                              <tr>
								  <th width="3%">Complaints</th>	
                                  <th width="3%"> SRN</th>
								  <th width="10%"> Name</th>
								  <th width="8%"> Mobile NO.</th>
								  <th width="10%"> Email</th>
								  <!--<th width="10%"> Subject</th>-->
								  <th width="8%"> Date</th>
								  <th width="10%"> Message</th>
								  <th width="10%"> Ticket No</th>
								  <th width="5%">No of Comments</th>
								  <th width="10%"> Status</th>
								  <th width="15%"> Operation</th>
                              </tr>
                              </thead>
                              <tbody>
							 <?php $cnt=0; if(count($page_data)>0){ foreach($page_data as $row) { $cnt++;?>
                             <?php $id= $row['cust_id']; ?>
								
                                 <tr id='<?php echo $row['cust_id'];?>'>
								 <td><?php echo ($row['comp_status']==1?"<img src='".SITE_URL."assets/admin/img/read.png'>":"<img src='".SITE_URL."assets/admin/img/unread.png'>")?></td>
                                  <td data-id='<?php echo $cnt;?>'><?php echo $cnt;?></td>
                                  <td class='hindi'><?php echo $row['cust_name'];?></td>
                                  <td><?php echo $row['cust_mobile'];?></td>
                                  <td><?php echo $row['cust_email'];?></td>
                                  <!--<td><?php echo $row['cust_subject'];?></td>-->
                                  <td><?php echo $row['cust_date'];?></td>
                                  <td class='hindi'><?php echo $row['cust_message'];?></td>
								  <td><?php echo $row['ticket_no'];?></td>
								  <td class='hindi'><?php echo $row['comments'];?></td>
                                  <td><?php echo ($row['comp_user_id']==0)?'Unassigned':'Assigned'?></td>
                                  <td>
                                      <!--<a href="<?php echo base_url()."form/edit/".$row['cust_id']; ?>/view"><button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button></a>

                                      <a href="<?php echo EDITCOMPLAIN.$row['cust_id']; ?>"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>-->
									   <a href="<?php echo EDITCOMPLAIN.$row['cust_id'];?>"><button class="btn btn-success btn-xs" ><i class="fa fa-check"></i></button></a>
                                       <a > <button class="btn btn-danger btn-xs"  onclick="del_comp(<?php echo $row['cust_id'];?>);"> <i class="fa fa-trash-o "></i></button></a>
                                  </td>
                              </tr>
							 <?php } }else{?>
	                         <tr class="odd gradeX"> 
	                         <td colspan="6">No records found</td> 
	                           </tr> 
                              </tr>
                            <?php }?>
                             </tbody>
                          </table>
						  
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
<script type='text/javascript'>
/*function updatestatus(id){
	var compid=id;
	$.ajax({
		url:'<?=SITE_URL?>admin/complain/updatestatus',
		type:'POST',
		data:{compid:compid}
	}).done(function(response){
		if(response){
		alert(response);
		return true;
		}
		else{
		return false;
		}
	});
	
}
onclick="return updatestatus(<?php echo $row['cust_id'];?>);"
*/
</script>