<section id="container" class="">
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <section class="panel">
                   <header class="panel-heading">
                             Complaint List
                          </header>
                  
                   <div class="adv-table">
                   
                  <table id="dttable" class="table table-hover p-table">
                      <thead>
                      <tr>
                          
                       <th style="width:155px;text-align: center;"> Compliant </th>
                          

<th style="width: 122px; text-align:center;">Source</th>

<th style="width: 190px; text-align:center">Department (S) </th>

<th style="  width: 125px;text-align: center;">Assign To</th>

<th style="width:100px;text-align: center;">Status</th>


<th style="width:285px; text-align:center"><span style="
    margin-left: -100px;
">Latest Activity / Comments</span></th>


                    

                      </tr>
                      </thead>
                      <tbody>
					  <?php foreach($page_data as $row){?>
                      <tr >
                          
                          
                          <td class="p-name " style="text-align: center;" data-href="<?php echo EDITCOMPLAIN.$row['cust_id'];?>">
                         <a style="
    color: #FCB322;
"><?php echo $row['ticket_no'];?>.</a><br>
                              <a class='hindi'><?php echo ucwords($row['cust_name'].' '.$row['cust_lastname']);?></a>
                              <br>
                              <small style="rgb(183, 183, 183)">Created On:<?php echo date('d M Y',strtotime($row['date_added']));?></small> <br>
                                 <small style="rgb(183, 183, 183)"><?php echo $row['cust_mobile'];?></small> <br>
                                <small style="rgb(183, 183, 183)"><?php echo $row['cust_email'];?></small>
                          </td>
                          <td class="p-team  sorting_1 hindi" style="
    text-align: center;
    font-size: 18px;
" data-href="<?php echo EDITCOMPLAIN.$row['cust_id'];?>">

<?php echo $row['section_name'];?>
                      
                          </td>
                          
  <td style="text-align:center" class="p-team" data-href="<?php echo EDITCOMPLAIN.$row['cust_id'];?>">
          <?php $cat_name=explode(',',$row['cat_name']);
				foreach($cat_name as $cat){
				echo ($cat==null?'-':$cat)."<br>";
				}
		  ?>
		                            </td>
<td class="p-team  sorting_1" style="
    text-align: center;
" data-href="<?php echo EDITCOMPLAIN.$row['cust_id'];?>">
                      
                              <a href="#"> <?php echo ($row['assigned_image']!='')?"<img class='thumb media-object' src='".SITE_URL.$row['assigned_image']."' >":"<img class='thumb media-object' src='".SITE_URL."assets/admin/img/default.jpg' >" ?></a>
                              </br>
                           
      <small style="margin-left: -6px;rgb(183, 183, 183)"><?php echo ucwords($row['assigned_to']);?></small><br>
	   <small style="margin-left: -11px;rgb(183, 183, 183)"><?php echo ucwords($row['designation']);?></small>
           
                    
                   
                      
                          </td>
                          
                         <td class=" " style="
    text-align: center;
" data-href="<?php echo EDITCOMPLAIN.$row['cust_id'];?>"> <?php echo ($row['comp_user_id']==0)?'Unassigned':'Assigned'?> </td>
                          
<td style="text-align:center" class="p-name" data-href="<?php echo EDITCOMPLAIN.$row['cust_id'];?>">
        
<div style="
    width: 262px;
    line-height: 1.5em;    
    height: 6em;    overflow: hidden;    text-overflow: ellipsis;
">     
<a><?php echo ucwords($row['commented_by']);?></a><br>
  
<span style="line-height: 1.5em;height: 3em;" class='hindi'>
        <?php echo ucwords($row['last_comment']==null?'-':$row['last_comment']);?><span>
    </div>
    
<?php
$date=date('d-m-Y');
$cur_date=strtotime($date);
$date_added=date('d-m-Y',strtotime($row['date_added']));
$date_time=date('d-m-Y h:i A',strtotime($row['date_added']));
$dt_add=date('h:i A',strtotime($row['date_added']));
#date('g:i a','h:i A');
$new_date=strtotime($date_added);
if($row['date_added']!=null){
?>
<span style="color: rgb(204, 204, 204);margin-left: -95px;"><?php echo ($cur_date==$new_date?'Today'.' '.$dt_add:$date_time);?></span>
<?php } ?>
  <?php if($row['unread_comments']!=0){?>  
    <div class="circle">
<?php echo $row['unread_comments'];?>
 </div>
<?php } ?>
 <div class="rightmark">
<i class="fa fa-chevron-right"></i>

 </div>

                              
                          </td>
                          
                      </tr>
 <?php }?>                     
                      
                     <!-- <tr>
                          
                          
                       <td class="p-name " style="
    text-align: center;
">
                         <a href="project_details.html" style="
    color: #FCB322;
">01 / 20-12-2015.</a><br>
                              <a href="project_details.html">Pratish Rupawate:</a>
                              <br>
                              <small style="rgb(183, 183, 183)">Created On:30 Apr 2015</small> <br>
                                 <small style="rgb(183, 183, 183)">8976274649</small> <br>
                                <small style="rgb(183, 183, 183)">sumit@gmail.com</small>
                          </td>
                          
                          
                          
                          <td class="p-team  sorting_1 hindi" style="
    text-align: center;
    font-size: 18px;
">

eq[;ea=h lfpoky;
                          </td>
                          
  <td style="text-align:center" class="p-team">
           Water Department
                          </td>
  <td class="p-team " style="
    text-align: center;
">
                      
                              <a href="#"><img alt="image" class="" src="<?php echo ADMIN_URL?>img/default.jpg"></a>
                              </br>
                           
      <small style="margin-left: -6px;rgb(183, 183, 183)">Suresh Mehra</small><br>
	   <small style="margin-left: -11px;rgb(183, 183, 183)">B.D.O Dharbanga</small>
           
                    
                   
                      
                          </td>
                          
                         <td class=" " style="
    text-align: center;
">   
                       

             
Unassigned
                    
                      </td>
                          
<td style="text-align:center" class="p-name">
        
<div style="
    width: 262px;
    line-height: 1.5em;    
    height: 6em;    overflow: hidden;    text-overflow: ellipsis;
">    
<a>Pratish Rupawate</a><br>
  
<span style="line-height: 1.5em;height: 3em;">
        I am Duncan Macleod, born 400 years ago in the Highlands of Scotland. I am Duncan Macleod, born 400 years ago in the Highlands of Scotland.<span>

        
    </div>

<span style="color: rgb(204, 204, 204);margin-left: -95px;">2.40pm</span>
    
    <div class="circle">
2

<div class="rightmark">
<i class="fa fa-chevron-right"></i>

 </div>
 </div>                </td>
                      </tr>-->
                      </tbody>
                  </table>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
     
  </section>
 <style> 
.commnets{
	width:119px;
	height:50px;
	float:left;
	margin-left: 13px;
	
}

.unread{
   
    padding: 10px;
    background: #e6e6e6;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
    width:59px;
    height:31px;
	float:right;
}

.read{
   
    padding: 10px;
    background: #FBDDAF;
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
    width:59px;
    height:31px;
	float:left;
}

.triangle-down{
	width: 0;
  	height: 0;
	border-left: 25px solid transparent;
  	margin-top: 10px;
  	border-top: 22px solid rgb(251, 221, 175);
}

.triangle-down-unread{

  width: 0;
  height: 0;
  border-right: 16px solid transparent;
  border-top: 18px solid rgb(230, 230, 230);
  float: right;
  margin-right: 5px;
  margin-top: 3px;
  
  }
  
.status{
	    width: 105px;
		height: 34px;
		float: left;
		margin-left: -200px;
		align-content: center;
 }
 
.rcorners1 {
         border-radius: 7px;
  	 	 background: #8AC007;
  	 	 padding: 4px;
  	 	 width: 106px;
}

.rcorners2 {
         border-radius: 7px;
  	 	 background: #e84c3d;
  	 	 padding: 4px;
  	 	 width: 106px;
}

.circle {
	 width: 20px;
	  height: 20px;
	  float: right;
	  margin-top: -50px;
  	    margin-right: 40px;
	  background: #4f81bd;
	  color: white;
  	  text-align: center;
      border-radius: 50px;
     -webkit-border-radius: 50px;
}

.small1{
	margin-top: 0;
	rgb(183, 183, 183);float: left;
    margin-left: -97px;
    margin-top: -16px;
    font-size: 13px;
    color: #797979;
	text-transform: uppercase;
	text-align: center;
	width: 126px;
	
}
	

.comments{
	  width: 330px;
	  float: left;
	  margin-left: -78px;
	  margin-top: -11px;
	  text-align: center;

	
}

.comments1{
	line-height: 1.5em;
    height: 3em;
    overflow: hidden;
    text-overflow: ellipsis;
    width: 100%;  text-align: center;

	
}

.time{
	 margin-top: 0;
    color: #bcbcbc;
	
	
}

.point{
	color: white;
    margin-left: 7px;
	
	
}

.rightmark{
	color: #a9a9a9;
  float: right;
    font-size: large;
  margin-top: -53px;
  margin-right: 2px;
}

.rightmark1{
	  font-size: large;
  margin-top: -37px;
  margin-right: -3px;
}
tr{
   cursor: pointer;
   /* whatever other hover styles you want */
}
</style>
<script type='text/javascript'>

   /* $('table tr').click(function(){
        window.location = $(this).attr('data-href');
    });*/
	$('#dttable tr td').click(function ()    {
	location.href = $(this).attr('data-href');
	});
</script>