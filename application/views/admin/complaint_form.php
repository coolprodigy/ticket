<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/admin/plupload/css/jquery-ui.css" />
<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/admin/plupload/css/jquery.ui.plupload.css" type="text/css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL; ?>assets/admin/plupload/js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL; ?>assets/admin/plupload/js/jquery.ui.plupload.js"></script>
<section id="container" class="">
			<section id="main-content">
					<section class="wrapper site-min-height">
              <!-- page start-->
							<section class="panel">
								<header class="panel-heading">
									Complaint Form
								</header>
							</section>
										<?php
											$tmp=$this->session->userdata('logged_in');
											if(isset($edit_data)){
											#echo "<pre>";
											#print_R($edit_data);die;
											extract($edit_data);
											if(isset($comp_resolved))
											{?>										
											<div class="row" id="complainStatus">
											<!--// For Resolved //-->
											
												<div class="col-md-12 <?=($comp_resolved==0?'pending':($comp_resolved==1?'success':'putup'))?>" id="wrap">
													<div class="col-md-10">
													<p><?=($comp_resolved==0?'<span class="success">Pending</span>':($comp_resolved==1?'<span class="pending">Resolved</span>':'<span class="putup">Put up for resolution</span>'))?></p>
												</div>
											<div class="col-md-2">
										<?php 
											if($tmp['status']==1){ ?>		
												<input type="button" data-href='1' value='Resolve'  class='resstat' />
												<input type="button" data-href='0' value='UnResolve'  class='resstat' />
										<?php } ?>
										<?php 
											if($tmp['status']==0){ ?>
												<input type="button" data-href='2' value='Put up for Resolution'  class='resstat' />
										<?php } ?>
											</div>
										</div>
											</div>
										<?php } } ?> <!--this is to show the status of the complaint and access for changing it-->
										

							
							<div class="row">
								<div class="col-md-8" style='width:100%;'>
									<section class="panel">
									
										<div class="bio-graph-heading project-heading">
										<?php 
											if(isset($edit_data)) {
												#echo "<pre>";
												#print_R($edit_data);die;
												extract($edit_data);
												$last_modified=array($date_added,$date_modified,$assigned_date,$category_date,$attached_date,$comment_date);
												array_multisort($last_modified,SORT_DESC,$last_modified);
												$categoryid=explode(',',$comp_cat_id);
												$assistantid=explode(',',$comp_assist_id);
												$image=explode(',',$images);
										?>
											<strong>COMPLAINT NO: ( <?php echo $ticket_no;?>)</strong>
										<?php } else{ ?>
											<strong>Add Complaint</strong>
										<?php } ?>
										</div>
										
										<?php if(isset($edit_data) && $tmp['status']==1){ ?>
											<span class='useredit'><a onclick='edit();'><img src="<?=SITE_URL?>assets/admin/img/edit.png" width="45" height="41"></a></span>
										<?php } ?>
										
										<div class="panel-body bio-graph-info">
											<!--<h1>New Dashboard BS3 </h1>-->                                 
											<form  action="<?php echo ADDCOMPLAIN;?>" method="POST" enctype="multipart/form-data" id='addcomplain' onsubmit='return makeread();'>
												<input type='hidden' value='<?=isset($cust_id)?$cust_id:''?>' name='cust_id' id='cust_id'>


												<!-- side a -->
												<div class="col-md-8 col-sm-12">

													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Mobile:</span></p>
														</div>
														<div class="col-sm-12 col-lg-10">
															<input type="text" class="form-control hindi" name="cust_mobile" id="cust_mobile" value="<?=isset($cust_mobile)?$cust_mobile:''?>" placeholder="Enter Mobile Number">
															<?php echo form_error('cust_mobile');?>
														</div>
													</div>


													<div class="row">
														<div class="col-sm-12 col-lg-2">
	                                       					<p><span class="bold">First Name:</span></p>
	                                       				</div>
														<div class="col-sm-12 col-lg-10">
															<input type="text" class="form-control hindi" id="cust_name" name="cust_name" value="<?=isset($cust_name)?$cust_name:''?>" size="50"  >
											 				<?php echo form_error('cust_name');?>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Last Name:</span></p>														
														</div>
														<div class="col-sm-12 col-lg-10">
															<input type="text" class="form-control hindi" id="cust_lastname" value="<?=isset($cust_lastname)?$cust_lastname:''?>" name="cust_lastname">
															<?php echo form_error('cust_lastname');?>
														</div>
													</div>



													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p> <span class="bold">Email:</span></p>														
														</div>
														<div class="col-sm-12 col-lg-10">
															<input type="text" class="form-control" id="cust_email" name="cust_email" value="<?=isset($cust_email)?$cust_email:''?>" placeholder="Enter Email">
															<?php echo form_error('cust_email');?>
														</div>
													</div>



													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Date:</span></p>
														</div>
														<div class="col-sm-12 col-lg-10">
															<input type="text" class="form-control" name="cust_date" id="date" value="<?=isset($cust_date)?$cust_date:''?>" placeholder="Enter date" readonly required>
															<?php echo form_error('cust_date');?>
														</div>
													</div>


													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Complaint<br> Gist: </span></p>
														</div>
														<div class="col-sm-12 col-lg-10">
															<textarea  name="cust_message"  id="cust_message" class="span12 ckeditor  m-wrap hindi" cols="70" rows="5" ><?=isset($cust_message)?$cust_message:''?></textarea>
															<?php echo form_error('cust_message');?>
														</div>
													</div>



													<?php
														if(isset($image)) {
															$image=array_filter($image);
														}
														
														if (isset($image) && !empty($image)) {
													?>
													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class='bold'>Attached Files:</span></p>													
														</div>
														<div class="col-sm-12 col-lg-10">
															
															<?php
																$i=0;
																foreach($image as $img_up) {
																	$img_up = pathinfo($img_up);
																	#print_R($img_up);die;

																	$img_ids=explode(',',$img_id);
																	#print_r($img_id);die;
															?>  
																	<a><i class="fa fa-times"  data-href='<?php echo $img_ids[$i];?>'></i></a>
									                                
									                                <?php
																		switch($img_up['extension']){
																			case "docx":
																			case "doc":
																	?>
																	
																	<i class="fa fa-file-text-o"></i></br>
																	
																	<?php
																		break;
																		case "jpg":
																		case "jpeg":
																		case "gif":
																		case "png":
																	?>
																	
																	<i class="fa fa-picture-o"></i> </br>
																	
																	<?php
																		break; 
																		case "xml":
																		case "xls":
																		case "csv":
																	?>
																	
																	<i class="fa fa-file-excel-o"></i> </br>
																	
																	<?php
																		break;
																		case "pdf":
																	?>
																	
																	<i class="fa fa-file-pdf-o" ></i> </br>
																	
																	<?php
																		break;
																		case "zip":
																	?>
																	
																	<i class="fa fa-file-archive-o"></i> </br>
																	
																	<?php
																		break; 
																		}
																	?>
																	
																	<span><?php echo $img_up['basename'];?></span>
																<!-- </div> -->
														<?php
															$i++;
															} //end  foreach
														?>


														</div> <!-- end col-md -->

														
													</div> <!-- row --><?php } // end if (isset($image) && !empty($image)) { ?>



													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Attachment :</span></p>
														</div>
														<div class="col-sm-12 col-lg-10">
															<div id="uploader"></div>
														</div>
													</div>


													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Address :</span></p>
														</div>
														<div class="col-sm-12 col-lg-10">
															<textarea  name="address" id="address" class="span12 ckeditor  m-wrap hindi" cols="70" rows="5" ><?=isset($cust_address)?$cust_address:''?></textarea>
															<?php echo form_error('address');?>
														</div>
													</div>



													<?php if(isset($comp_cat_id) && !empty($com_cat_id)){?>
													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Linked Departments:</span></p>													
														</div>
														<div class="col-sm-12 col-lg-10">
															<p><?php echo $cat_name;?></p>															
														</div>
													</div>
													<?php } ?>



													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Section:</span></p>
														</div>
														<div class="col-sm-12 col-lg-10">
															<select class='krutiDev' name='section' id='section'>
																<option value=''>Select Section</option>
																<?php $res=$this->do_complain->show_section();?>
																<?php foreach ($res as $row){ ?>
																<option value="<?php echo $row['comp_sec_id'];?>"><?php echo $row['complaint_section'];?></option>
																<?php } ?>
															</select>
														</div>
													</div>



													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Link To Department:</span></p>
														</div>
														<div class="col-sm-12 col-lg-10">
															<select name='sel_cat[]' id='org_cat' multiple=""  >
										  						<?php $result=$this->do_category->get_cat_list();?>
																<?php foreach($result as $row){?>
																<option value='<?php echo $row['comp_cat_id'];?>'><?php echo $row['cat_name'];?>.</option>
																<?php } ?>
															</select> 				
														</div>
													</div>
												</div> <!-- end side a -->



												<!-- side B -->
												<div class="col-md-4 col-sm-12">
													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Created On:</span></p>
														</div>
														<div class="col-sm-12 col-lg-8">
															<input type="text" class="form-control" value='<?=isset($date_added)?date('d-M-Y',strtotime($date_added)):date('d-M-Y');?>' readonly>
														</div>
													</div>
												</div>
												<?php if(isset($last_modified)){?>
												<div class="col-md-4 col-sm-12">
													<div class="row">
														<div class="col-sm-12 col-lg-2">
															<p><span class="bold">Last Modified:</span></p>
														</div>
														<div class="col-sm-12 col-lg-8">
															<input type="text" class="form-control" value='<?php echo date('d-M-Y,h:i:s',strtotime($last_modified[0]));?>' readonly>
														</div>
													</div>
												</div>
												  <?php } ?>

										</div>	

									
                      </section>





                      <!-- ########################################################################## -->



                      <article class="panel">
                      	<section class="panel-body">

	                      	<div class="col-lg-12">
	                      		<div class="row">
		                      		<?php if(isset($edit_data) && ($tmp['status']==1 || $tmp['status']==0)){?>
	    	                  			<span class='useredit'><a onclick='mkedit();'><img src="<?=SITE_URL?>assets/admin/img/edit.png" width="45" height="41"></a></span>
	                      			<?php } ?>
	                      		</div>
							
						

	                      		<div class="row">
	                      			<?php if(isset($user_name)){?>
										<div class="col-lg-12">
											<p><span class="bold">Complaint Presently Assigned To : </span><?php echo $user_name;?></p>
										</div>
									<?php } ?>
	                      		</div>

	                      		
	                      		<div class="row">
	                      			<div class="col-lg-12">
	                      				<p><span class="bold">Assign To Admin</span></p> 
	                      			</div>
	                      		</div>


	                      		<div class="row">
	                      			<div class="col-lg-2"><p><span class="bold">Select Department:</span></p></div>
	                      			<div class="col-lg-3">
	                      				<select id='sel_cat' onChange='sel_user();'>
	                      					<option value=''>Select Department</option>
	                      					<?php $result=$this->do_category->get_cat_list();?>
											<?php foreach($result as $row){?>
											<option value='<?php echo $row['comp_cat_id'];?>'><?php echo $row['cat_name'];?></option>
											<?php } ?>
										</select>
									</div>
								
									<div class="col-lg-1"><p><span class="bold">Assign to:</span></p></div>
	                      			<div class="col-lg-2">
	                      				<select name='seluser' id='seluser'>
	                      					<option value='0'>Unassigned</option>
	                      				</select>
	                      				<?php echo form_error('seluser');?>
									</div>

									<div class="col-lg-4"></div>
	                      		</div>

	                      	</div> <!-- end col-lg-12 -->

	                      </section>
	                    </article>  



                      <!-- ############################################################# -->


                        <!--<section class="panel">
										 <?php if(isset($edit_data) && ($tmp['status']==1 || $tmp['status']==0)){?>
										<span style="float: right;"><a onclick='mkassedit();'><img src="<?=SITE_URL?>assets/admin/img/edit.png" width="45" height="41"></a></span> 
										<?php } ?>
										<div class="panel-body">
										<?php if(isset($attached)){ 
										if($attached==''||$attached!=''){?>
										<p><span class="bold">Complaint Presently Attached To : </span><?php echo $attached;?></p>
										<?php } } ?>
										<p><span class="bold">Attach To Assistant</span></p> 
												<p><span class="bold">Select Departhment </span>: &nbsp;&nbsp;
														<select id='sel_cat1' onChange='sel_assist();'>
																	<option value='' >Select Department</option>
																	<?php $result=$this->do_category->get_cat_list();?>
																	<?php foreach($result as $row){?>
																	<option value='<?php echo $row['comp_cat_id'];?>'><?php echo $row['cat_name'];?></option>
																	<?php } ?>
														</select> 

									&nbsp;&nbsp;&nbsp;&nbsp;<strong>Attach To</strong> : </br>&nbsp;
														<select style="margin-left: 355px;font-size: 13px;margin-top: -19px;" name='selassist[]' id='selassist' multiple>
														  <option value="" style="position: absolute;background: white;">Select Asistant</option>
														</select>
												</p>  
										</div>												
                      </section>-->
                       	
                       	<article class="panel">
                       		<section class="panel-body">
                       		<div class="col-lg-12">
	                       		<div class="row">
	                       			<?php if(isset($edit_data) && ($tmp['status']==1 || $tmp['status']==0 || $tmp['status']==2)){?></div>
		                       			 <span class='useredit'><a onclick='mkcomedit();'><img src="<?=SITE_URL?>assets/admin/img/edit.png" width="45" height="41"></a></span>
									<?php } ?>
	                       		</div>	

                       		
	                       		<div class="row">
	                       			<div class="col-sm-12 col-lg-2">
	                       				<p><span class="bold">Select Comments:</span></p>
	                       			</div>
	                       			<div class="col-sm-12 col-lg-10">
	                       				<select name='selcomment' id='selcomment'>
	                       					<option value='0'>Select Comment</option>
											<option value='1'>Please action in 3 days</option>
											<option value='2'>Please action in 5 days</option>
											<option value='3'>Please action in 7 days</option>
										</select> 
	                       			</div>
	                       		</div>

                       		
	                       		<div class="row">
	                       			<div class="col-sm-12 col-lg-2">
	                       				<p><span class="bold">Add Comments:</span></p>
	                       			</div>
	                       			<div class="col-sm-12 col-lg-10">
	                       				<textarea  name="comment"  id="comment" class="span12 ckeditor  m-wrap hindi" cols="70" rows="5"></textarea>
	                       			</div>
	                       		</div>
                       		</section>
                       	</article>	












										<div class="col-lg-offset-2 col-lg-10" >
											<button class="btn btn-success" type="button" style="width: 340px;"  onclick=' location.reload(true)'>Cancel</button>

											<input class="btn btn-danger" type="submit" style="width: 340px;" value='Save' id='submit'>
										</div>
                                     
                                      
									</form>  
                                   <div class="row"> </div>
                      <section class="panel showtimeline" >
                          <div class="panel-body">
                                  <div class="text-center mbot30">
                                      <h3 class="timeline-title">History</h3>
                                      <p class="t-info">Records</p>
                                  </div>
					
                                  <div class="timeline" id='timeline'>
									</div>

                                  <div class="clearfix">&nbsp;</div>
                              </div>  
							  </div>
                          
                                    
                                    
                      
							  
							
                      </section>
					  
			<!--Timeline ends-->          
      </section>
	   </section>
  </section>		
<style>
	::-webkit-scrollbar {
    width: 2px;
    height: 3px
}
::-webkit-scrollbar-button {
    background: #f1f1f1
}
::-webkit-scrollbar-track-piece {
    background: #f1f1f1
}
::-webkit-scrollbar-thumb {
    background: #bcbcbc
}
</style>
<!--Name
Author:Sumith Nalwala
-->  
  <script type='text/javascript'>
   $(document).ready(function(){
	$('.showtimeline').hide();
   sel_user();
   sel_assist();
   if($('#cust_id').val()!=''){
   $('#org_cat').val(<?=isset($categoryid)?json_encode($categoryid):''?>); /*
    $('#submit').val('Update');
	Commented by Sumith date:28/04/2015 beacause it is shown in p tag as currently assigned */
	$('#section').val(<?=isset($section)?$section:''?>);
    $('.time').show();
	$('.showtimeline').show();
	load_comments();
   }
});
/* It is used as a filter to select the user on basis of dropdown*/

	function sel_user(){
		var id=$('#sel_cat').val();
			$.ajax({
				    type:'POST',
					url:'<?=SITE_URL?>admin/complain/show_user',
					data:{option:id},   
			}).done(function(response){
				$('#seluser').html(response);
				 $('#seluser').val(<?=isset($comp_user_id)?$comp_user_id:'0'?>);
				 if($('#seluser').val()==null){
					$('#seluser').val('0');
				 }
				 /*Commented by Sumith date:28/04/2015 beacause it is shown in p tag as currently assigned
				 */
					 
			});
	}

	function sel_assist(){
		var id=$('#sel_cat1').val();
			$.ajax({
			type:'POST',
			url:'<?=SITE_URL?>admin/complain/show_assistant',		
			data:{aid:id}	
			}).done(function(resp){
				$('#selassist').html(resp);
				<?php if(isset($assistantid)){?>
				var assist= <?php echo json_encode($assistantid)?>;
				$('#selassist').val(assist);
				<?php } ?>
			/*Commented by Sumith date:28/04/2015 beacause it is shown in p tag as currently assigned	
				*/
		});	
	
	}

	

   </script>   
 <script type='text/javascript'>
/*To disable all the inputs of form using javascript*/
$(document).ready(function(){
 if($('#addcomplain :input').val()){
 $("textarea,select,input").attr('disabled','disabled');
 $('#complainStatus input[type=button]').prop("disabled", false);
 $('#submit').prop("disabled",false);
 /*$("#comment").prop("readonly", false);*/
	<?php 
	if($tmp['status']==2){?>
	$('select').attr("disabled", "disabled");
	/*$('#submit').prop("readonly",false);*/
	<?php } ?>
 }
});

/*After clicking on th option of edit this function is called*/
function edit(){
$("#addcomplain :input").prop("disabled", false);
$("#date").attr('disabled',true);
}

function mkedit(){
$("#sel_cat,#seluser").prop("disabled", false);
}

function mkassedit(){
$("#sel_cat1,#selassist").prop("disabled", false);
}

function mkcomedit(){
$("#selcomment,#comment").prop("disabled", false);
}

/*#Author:Sumith Nalwala
Date:30/03/3015.
This function fetches the details of the complainer if is in the database*/
$(document).ready(function(){
$('#cust_mobile').blur(function(){
var mno=$('#cust_mobile').val();
    $.ajax({
        url:'<?=SITE_URL?>admin/complain/check_mobile',
        type:'post',
        data:{mno:mno}
    }).done(function(response){
        if(response!=false){
        var data=JSON.parse(response);
            if(confirm('The name of the complainer is '+data['cust_name'] +' '+'Do you want to fetch his details?')){
            $('#cust_name').val(data['cust_name']);
            $('#cust_lastname').val(data['cust_lastname']);
            $('#cust_email').val(data['cust_email']);
            }
        }   
            });
   });
	/*This is used to get the value from the dropdown and add it to the comment box it has been done using ternary operator
	Author:Sumith Nalwala:Date:8/04/2015
	*/
	$("#selcomment").change(function () {
			($('#selcomment').val()!=0?$('#comment').text($('#selcomment option:selected').text()):$('#comment').text(''));
		
	});

});

/*To laod the comments
Author:Sumith Nalwala
*/
function load_comments(){
var complain=$('#cust_id').val();
        $.ajax({
        type:'post',
        url:'<?=SITE_URL?>admin/complain/load_comments',
        data:{complain:complain}
        }).done(function(response){
        $('#timeline').html(response);
    });  
}

function makeread(){
 /*$("#addcomplain :input").prop("readonly", true);
 $('select').attr("disabled", false);*/
  $("textarea,select,input").attr("disabled", false);
 return true;
 }

</script>
<!--Name
Author:Sumith Nalwala
-->  
<script type='text/javascript'>
$(document).ready(function() {
	$("#uploader").plupload({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : '/ticket/admin/complain/upload',

		// User can upload no more then 20 files in one go (sets multiple_queues to false)
		max_file_count: 20,
		
		chunk_size: '1mb',

		// Resize images on clientside if we can
		resize : {
			width : 200, 
			height : 200, 
			quality : 90,
			crop: true // crop to exact dimensions
		},
		
		filters : {
			// Maximum file size
			max_file_size : '1000mb',
			// Specify what files to browse for
			mime_types: [
				{title : "Image files", extensions : "jpg,gif,png"},
				{title : "Zip files", extensions : "zip"},
				{title : "Pdf files", extensions : "pdf"},
				{title : "Word files", extensions : "doc,docx,xml,xls,csv"}
			]
		},

		// Rename files by clicking on their titles
		rename: true,
		
		// Sort files
		sortable: true,

		// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
		dragdrop: true,

		// Views to activate
		views: {
			list: true,
			thumbs: true, // Show thumbs
			active: 'thumbs'
		},

		// Flash settings
		flash_swf_url : '../../js/Moxie.swf',

		// Silverlight settings
		silverlight_xap_url : '../../js/Moxie.xap'
	});
	/*$('#submit').click(function(){
		if ($('#uploader').plupload('getFiles').length > 0) {
		$.ajax({
        type: "POST",
        url: '<?php echo SITE_URL;?>admin/complain/addcomplain',
        data: $( "#addcomplain" ).serialize(),
        success: function (response) {
			//alert(response);
			//alert('response'+response);
            //$("#" + container).html(response);
        }
    });
	}
	/*else {
			alert("You must have at least one file in the queue.");
		}
	});*/



$('a i').click(function(){
var id=$(this).attr('data-href');
$.post("<?=SITE_URL?>admin/complain/del_image", {id:id}, function(data) { 
		if(data){
		location.reload();
		}
	});
});

$('#complainStatus input[type=button]').on('click',function(e){
	//e.preventDefault();
	var stats=$(this).attr('data-href');
	var cust_id=$('#cust_id').val();
	//alert(stats);
	$.ajax({
			type: "POST",
			url: '<?php echo SITE_URL;?>admin/complain/comp_status',
			data: {stats:stats,cust_id:cust_id},
			cache: false,
			success: function (response) {
				if(response){
					console.log(response);
					if(stats==1){
						$('#complainStatus #wrap').addClass('success');
						$('#complainStatus #wrap').removeClass('pending');
						$('#complainStatus #wrap').removeClass('putup');
						$('#complainStatus #wrap p').html('Resolved');
					}
					else if(stats==0){
						$('#complainStatus #wrap').addClass('pending');
						$('#complainStatus #wrap').removeClass('success');
						$('#complainStatus #wrap').removeClass('putup');
						$('#complainStatus #wrap p').html('Pending');
					}
					else{
						$('#complainStatus #wrap').addClass('putup');
						$('#complainStatus #wrap').removeClass('success');
						$('#complainStatus #wrap').removeClass('pending');
						$('#complainStatus #wrap p').html('Put up for Resolution');
					}
				}
			}
		})

	}); 
});	
</script>
<!-- production -->