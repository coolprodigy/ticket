<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/admin/plupload/css/jquery-ui.css" />
<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/admin/plupload/css/jquery.ui.plupload.css" type="text/css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL; ?>assets/admin/plupload/js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL; ?>assets/admin/plupload/js/jquery.ui.plupload.js"></script>
<section id="container" class="">
			<section id="main-content">
					<section class="wrapper site-min-height">
              <!-- page start-->
							<section class="panel">
								<header class="panel-heading">
									Complaint Form
								</header>
							</section>
<?php
$tmp=$this->session->userdata('logged_in');
if(isset($edit_data)){
//echo "<pre>";
//print_R($edit_data);die;
extract($edit_data);
if(isset($comp_resolved)){?>
<div class="row" id="complainStatus">
	
	<!--// For Resolved //-->
	<div class="col-md-12 <?=($comp_resolved==0?'pending':($comp_resolved==1?'success':'putup'))?>" id="wrap">
		<div class="col-md-10">
			<p><?=($comp_resolved==0?'<span class="success">Pending</span>':($comp_resolved==1?'<span class="pending">Resolved</span>':'<span class="putup">Put up for resolution</span>'))?></p>
		</div>
		<div class="col-md-2">
		<?php if($tmp['status']==1){ ?>		
			<input type="button" data-href='1' value='Resolve'  class='resstat' />
			<input type="button" data-href='0' value='UnResolve'  class='resstat' />
		<?php } ?>
		<?php if($tmp['status']==0){ ?>
			<input type="button" data-href='2' value='Put up for Resolution'  class='resstat' />
		<?php } ?>
		</div>
	</div>
</div>
<?php } } ?>

							
							<div class="row">
								<div class="col-md-8" style='width:100%;'>
            <section class="panel">
					<div class="bio-graph-heading project-heading">
									<?php 
									//$tmp=$this->session->userdata('logged_in');
									if(isset($edit_data)){
									#echo "<pre>";
									#print_R($edit_data);die;
									$last_modified=array($date_added,$date_modified,$assigned_date,$category_date,$attached_date,$comment_date);
									array_multisort($last_modified,SORT_DESC,$last_modified);
									$categoryid=explode(',',$comp_cat_id);
									$assistantid=explode(',',$comp_assist_id);
									$image=explode(',',$images);
									?>
									 <strong>COMPLAINT NO: ( <?php echo $ticket_no;?>)</strong>
									<?php } else{ ?>
									  <strong>Add Complaint</strong>
									  <?php } ?>
                        </div>
									 <?php if(isset($edit_data) && $tmp['status']==1){?>
									 <span style="float: right;"><a onclick='edit();'><img src="<?=SITE_URL?>assets/admin/img/edit.png" width="45" height="41"></a></span>
									 <?php } ?>
                    <div class="panel-body bio-graph-info">
									  <!--<h1>New Dashboard BS3 </h1>-->
									  <form  action="<?php echo ADDCOMPLAIN;?>" method="POST" enctype="multipart/form-data" id='addcomplain' onsubmit='return makeread();'>
									   <input type='hidden' value='<?=isset($cust_id)?$cust_id:''?>' name='cust_id' id='cust_id'>
                    <div class="row p-details">
									<!--<div class="bio-row" style="margin-top: 27px; float: left;">
										  <p><span class="bold">Ticket No</span>:</p><input type="text" class="form-control" name="man_ticket" id="man_ticket" value="<?=isset($man_ticket)?$man_ticket:''?>" placeholder="Enter Ticket Number" style="width: 300px;margin-left: 135px;margin-top:-35px;">
									  <?php echo form_error('man_ticket');?>
									  </div>-->
									 <div class="bio-row" style="margin-top: 27px; float: left;">
										  <p><span class="bold">Mobile </span>:</p><input type="text" class="form-control hindi" name="cust_mobile" id="cust_mobile" value="<?=isset($cust_mobile)?$cust_mobile:''?>" placeholder="Enter Mobile Number" style="width: 300px;margin-left: 135px;margin-top:-35px;">
									  <?php echo form_error('cust_mobile');?>
									  </div>
									  <div class="bio-row" style='float: left;'>
										  <p><span class="bold">Created On</span>:</p><input type="text" class="form-control"  value='<?=isset($date_added)?date('d-M-Y',strtotime($date_added)):date('d-M-Y');?>' style="width: 300px;margin-left: 135px;margin-top: -35px;" readonly>
									  </div>
									  <div class="bio-row" >
										 <p><span class="bold">First Name</span>:</p><input type="text" class="form-control hindi" id="cust_name" name="cust_name" value="<?=isset($cust_name)?$cust_name:''?>" size="50" style="width:300px; margin-left: 135px;margin-top: -35px;" >
										 <?php echo form_error('cust_name');?>
									  </div>
									<?php if(isset($last_modified)){?>
									 <div class="bio-row" style='float:right;margin-top: -42px;'>
										  <p><span class="bold">Last Modified</span>:</p><input type="text" class="form-control" value='<?php echo date('d-M-Y,h:i:s',strtotime($last_modified[0]));?>' style="width: 300px;margin-left: 135px;margin-top: -35px;" readonly> 
									</div>
										  <?php } ?>
									<div class="bio-row" >
										  <p><span class="bold">Last Name</span>: 
										  </p><input type="text" class="form-control hindi" id="cust_lastname" value="<?=isset($cust_lastname)?$cust_lastname:''?>" name="cust_lastname" style="width: 300px;margin-left: 135px;margin-top: -30px;"> 
										  <?php echo form_error('cust_lastname');?>
									</div>
                                  
                                   <div class="bio-row">
										  <p>
                                      
                                      <span class="bold">Email</span>: 
									      </p>
									  <input type="text" class="form-control" id="cust_email" name="cust_email" value="<?=isset($cust_email)?$cust_email:''?>" placeholder="Enter Email"  style="width: 300px;float: left;margin-left: 135px;margin-top: -35px;">
									    <?php echo form_error('cust_email');?>
										   <p style="
											margin-top: 20px;
										   ">
										 <span class="bold">Date</span>: 
										   </p>
 
									  <input type="text" class="form-control" name="cust_date" id="date" value="<?=isset($cust_date)?$cust_date:''?>" placeholder="Enter date" readonly required style="width: 300px;float: left;margin-left: 135px;margin-top: -35px;">
										<?php echo form_error('cust_date');?>
									   </div>
                                    </div>
									</br>
									<p><span class="bold">Complaint<span style="margin-left: 36px;font-weight: 500;">:</span> <br> Gist</span></p>
									
									<textarea  name="cust_message"  id="cust_message" class="span12 ckeditor  m-wrap hindi" cols="70" rows="5" style="
										width: 300px;
										margin-left:136px;
										margin-top:-39px;
									"><?=isset($cust_message)?$cust_message:''?></textarea>
									<?php echo form_error('cust_message');?>
									<br><br>
									<?php
									if(isset($image)){
									$image=array_filter($image);
									}
									if (isset($image) && !empty($image)){?>
						            <p><span class='bold'>Attached Files</span><span style="margin-left: 15px;">:</span>
									<div style="margin-left: 132px;margin-top: -3px;height:19px;">
									<?php $i=0; foreach($image as $img_up){
									$img_up = pathinfo($img_up);
									#print_R($img_up);die;
									$img_ids=explode(',',$img_id);
									#print_r($img_id);die;
									?>
									<div style="margin-top: -21px;width: 63px;float: left;margin-left: 15px;  text-align: center;">
									<a><i class="fa fa-times" style="float: right;margin-top: -7px;color: red;" data-href='<?php echo $img_ids[$i];?>'></i></a>
									<?php
									switch($img_up['extension']){
									case "docx":
									case "doc":
									?>
									<i class="fa fa-file-text-o" style='font-size:35px;margin-left:5px;color:#0d69c8;'></i></br>
									<?php break;
									case "jpg":
									case "jpeg":
									case "gif":
									case "png":
									?>
									<i class="fa fa-picture-o" style='font-size:35px;margin-left:5px;color:red;'></i> </br>
									<?php break; 
									case "xml":
									case "xls":
									case "csv":
									?>
									<i class="fa fa-file-excel-o" style='font-size:35px;margin-left:5px;color: green;'></i> </br>
									<?php break;
									case "pdf":
									?>
									<i class="fa fa-file-pdf-o" style='font-size:35px;margin-left:5px;color:red;'></i> </br>
									<?php break;
									case "zip":
									?>
									<i class="fa fa-file-archive-o" style='font-size:35px;margin-left:5px'></i> </br>
									<?php break; 
									} ?>
									<span><?php echo $img_up['basename'];?></span>
									</div>
									<?php $i++;} ?>
									</div>
									</p>
									<?php }?>
									</br>
									<p>
									<span class="bold" style="float:left;">Attachment
									</span>
									<span style="margin-left: 28px;">:</span></p>
									<div id="uploader" style='  margin-left: 134px;margin-top: -26px;'>
									
									</div>
									<br><br>
								
									<p><span class="bold">Address</span><span style="margin-left: 50px;">:</span></p>
									<p>
									<textarea  name="address"  id="address" class="span12 ckeditor  m-wrap hindi" cols="70" rows="5" style="
										width: 300px;
										margin-left:136px;
										margin-top:-39px;
									"><?=isset($cust_address)?$cust_address:''?></textarea>
									<?php echo form_error('address');?>
									</p>
									<?php if(isset($comp_cat_id) && !empty($com_cat_id)){?>
									<p> <strong> Linked Departments : </strong> <?php echo $cat_name;?></p>
								    </br>
									<?php } ?>
									<strong>Section</strong><span style="margin-left: 56px;">:</span>
									<select style='margin-left:24px;' class='krutiDev' name='section' id='section'><option value='' >Select Section</option>
									<?php $res=$this->do_complain->show_section();?>
									<?php foreach ($res as $row){ ?>
									<option value="<?php echo $row['comp_sec_id'];?>"><?php echo $row['complaint_section'];?></option>
									<?php } ?>
									</select></br><br/>
									<strong>  Link To <span style="margin-left: 54px;font-weight: 500;">:</span><br>Department </strong></br>
									<select name='sel_cat[]' id='org_cat' multiple="" style="margin-left: 134px;font-size: 13px;margin-top: -32px;" >
									  <?php $result=$this->do_category->get_cat_list();?>
									  <?php foreach($result as $row){?>
									  <option value='<?php echo $row['comp_cat_id'];?>'><?php echo $row['cat_name'];?>.</option>
									  <?php } ?>
									</select> <br/><br/>
									
									<!--<p>
									<span class="bold" style="float:left;">Attachment
									</span>
									<span style="margin-left: 28px;">:</span><!--<div id="uploader" style='margin-left:150px;' ></div><div id="uploader" style='  margin-left: 134px;margin-top: -26px;'></div>
									</p></br></br>-->
                      </section>
            <section class="panel">
										 <?php if(isset($edit_data) && ($tmp['status']==1 || $tmp['status']==0)){?>
									   <span style="float: right;"><a onclick='mkedit();'><img src="<?=SITE_URL?>assets/admin/img/edit.png" width="45" height="41"></a></span> 
										<?php } ?>
										<div class="panel-body">
										<?php if(isset($user_name)){?>
										<p><span class="bold">Complaint Presently Assigned To : </span><?php echo $user_name;?></p>
										<?php } ?>
										<p><span class="bold">Assign To Admin</span></p> 
										  <p><span class="bold">Select Department</span>: &nbsp;&nbsp;<select id='sel_cat' onChange='sel_user();'>
										  <option value='' >Select Department</option>
										  <?php $result=$this->do_category->get_cat_list();?>
										  <?php foreach($result as $row){?>
										  <option value='<?php echo $row['comp_cat_id'];?>'><?php echo $row['cat_name'];?></option>
										  <?php } ?>
										</select>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Assign to</strong> :
										&nbsp;<select name='seluser' id='seluser'>
										   <option value='0'>Unassigned</option>
										</select>
										<?php echo form_error('seluser');?>
										</p>  
										</div>
</br>
</br>
                      </section>
                        <!--<section class="panel">
										 <?php if(isset($edit_data) && ($tmp['status']==1 || $tmp['status']==0)){?>
										<span style="float: right;"><a onclick='mkassedit();'><img src="<?=SITE_URL?>assets/admin/img/edit.png" width="45" height="41"></a></span> 
										<?php } ?>
										<div class="panel-body">
										<?php if(isset($attached)){ 
										if($attached==''||$attached!=''){?>
										<p><span class="bold">Complaint Presently Attached To : </span><?php echo $attached;?></p>
										<?php } } ?>
										<p><span class="bold">Attach To Assistant</span></p> 
												<p><span class="bold">Select Departhment </span>: &nbsp;&nbsp;
														<select id='sel_cat1' onChange='sel_assist();'>
																	<option value='' >Select Department</option>
																	<?php $result=$this->do_category->get_cat_list();?>
																	<?php foreach($result as $row){?>
																	<option value='<?php echo $row['comp_cat_id'];?>'><?php echo $row['cat_name'];?></option>
																	<?php } ?>
														</select> 

									&nbsp;&nbsp;&nbsp;&nbsp;<strong>Attach To</strong> : </br>&nbsp;
														<select style="margin-left: 355px;font-size: 13px;margin-top: -19px;" name='selassist[]' id='selassist' multiple>
														  <option value="" style="position: absolute;background: white;">Select Asistant</option>
														</select>
												</p>  
										</div>												
                      </section>-->
                       <section class="panel">
									   <?php if(isset($edit_data) && ($tmp['status']==1 || $tmp['status']==0 || $tmp['status']==2)){?>
									   <span style="float: right;"><a onclick='mkcomedit();'><img src="<?=SITE_URL?>assets/admin/img/edit.png" width="45" height="41"></a></span>
									   <?php } ?>
										<div class="panel-body">
													<strong>  Select Comments </strong> 
										:&nbsp;&nbsp;
														<select name='selcomment' id='selcomment'>
														  <option value='0'>Select Comment</option>
														  <option value='1'>Please action in 3 days</option>
														  <option value='2'>Please action in 5 days</option>
														  <option value='3'>Please action in 7 days</option>
														</select> 
														</br>
														<strong>  Add Comments </strong>			
														</br>
														</br>
										<textarea  name="comment"  id="comment" class="span12 ckeditor  m-wrap hindi" cols="70" rows="5"></textarea>
										</br>
										</br>
										</div>
                       </section>
                  
										<div class="col-lg-offset-2 col-lg-10" style="margin-top: -10px;">
											<button class="btn btn-danger" type="button" style="width: 340px;" onclick=' location.reload(true)'>Cancel</button>

											<input class="btn btn-danger" type="submit" style="width: 340px;" value='Save' id='submit'>
										</div>
                                     
                                      
									</form>  
							</div>									
							</div>
							</div>
              <!-- page end-->
         
<!--timeline start-->
			   <div class="row" style="height:10px;"> </div>
                      <section class="panel showtimeline" >
                          <div class="panel-body">
                                  <div class="text-center mbot30">
                                      <h3 class="timeline-title">History</h3>
                                      <p class="t-info">Records</p>
                                  </div>
					
                                  <div class="timeline" id='timeline'>
									</div>

                                  <div class="clearfix">&nbsp;</div>
                              </div>
							  
							
                      </section>
					  
			<!--Timeline ends-->          
      </section>
	   </section>
  </section>		
<style>
	::-webkit-scrollbar {
    width: 2px;
    height: 3px
}
::-webkit-scrollbar-button {
    background: #f1f1f1
}
::-webkit-scrollbar-track-piece {
    background: #f1f1f1
}
::-webkit-scrollbar-thumb {
    background: #bcbcbc
}
</style>
<!--Name
Author:Sumith Nalwala
-->  
  <script type='text/javascript'>
   $(document).ready(function(){
	$('.showtimeline').hide();
   sel_user();
   sel_assist();
   if($('#cust_id').val()!=''){
   $('#org_cat').val(<?=isset($categoryid)?json_encode($categoryid):''?>); /*
    $('#submit').val('Update');
	Commented by Sumith date:28/04/2015 beacause it is shown in p tag as currently assigned */
	$('#section').val(<?=isset($section)?$section:''?>);
    $('.time').show();
	$('.showtimeline').show();
	load_comments();
   }
});
/* It is used as a filter to select the user on basis of dropdown*/

	function sel_user(){
		var id=$('#sel_cat').val();
			$.ajax({
				    type:'POST',
					url:'<?=SITE_URL?>admin/complain/show_user',
					data:{option:id},   
			}).done(function(response){
				$('#seluser').html(response);
				 $('#seluser').val(<?=isset($comp_user_id)?$comp_user_id:'0'?>);
				 if($('#seluser').val()==null){
					$('#seluser').val('0');
				 }
				 /*Commented by Sumith date:28/04/2015 beacause it is shown in p tag as currently assigned
				 */
					 
			});
	}

	function sel_assist(){
		var id=$('#sel_cat1').val();
			$.ajax({
			type:'POST',
			url:'<?=SITE_URL?>admin/complain/show_assistant',		
			data:{aid:id}	
			}).done(function(resp){
				$('#selassist').html(resp);
				<?php if(isset($assistantid)){?>
				var assist= <?php echo json_encode($assistantid)?>;
				$('#selassist').val(assist);
				<?php } ?>
			/*Commented by Sumith date:28/04/2015 beacause it is shown in p tag as currently assigned	
				*/
		});	
	
	}

	

   </script>   
 <script type='text/javascript'>
/*To disable all the inputs of form using javascript*/
$(document).ready(function(){
 if($('#addcomplain :input').val()){
 $("textarea,select,input").attr('disabled','disabled');
 $('#complainStatus input[type=button]').prop("disabled", false);
 $('#submit').prop("disabled",false);
 /*$("#comment").prop("readonly", false);*/
	<?php 
	if($tmp['status']==2){?>
	$('select').attr("disabled", "disabled");
	/*$('#submit').prop("readonly",false);*/
	<?php } ?>
 }
});

/*After clicking on th option of edit this function is called*/
function edit(){
$("#addcomplain :input").prop("disabled", false);
$("#date").attr('disabled',true);
}

function mkedit(){
$("#sel_cat,#seluser").prop("disabled", false);
}

function mkassedit(){
$("#sel_cat1,#selassist").prop("disabled", false);
}

function mkcomedit(){
$("#selcomment,#comment").prop("disabled", false);
}

/*#Author:Sumith Nalwala
Date:30/03/3015.
This function fetches the details of the complainer if is in the database*/
$(document).ready(function(){
$('#cust_mobile').blur(function(){
var mno=$('#cust_mobile').val();
    $.ajax({
        url:'<?=SITE_URL?>admin/complain/check_mobile',
        type:'post',
        data:{mno:mno}
    }).done(function(response){
		//alert(response);
        if(response!=false){
        var data=JSON.parse(response);
            if(confirm('The name of the complainer is '+data['cust_name'] +' '+'Do you want to fetch his details?')){
            $('#cust_name').val(data['cust_name']);
            $('#cust_lastname').val(data['cust_lastname']);
            $('#cust_email').val(data['cust_email']);
            }
        }   
            });
   });
	/*This is used to get the value from the dropdown and add it to the comment box it has been done using ternary operator
	Author:Sumith Nalwala:Date:8/04/2015
	*/
	$("#selcomment").change(function () {
			($('#selcomment').val()!=0?$('#comment').text($('#selcomment option:selected').text()):$('#comment').text(''));
		
	});

});

/*To laod the comments
Author:Sumith Nalwala
*/
function load_comments(){
var complain=$('#cust_id').val();
        $.ajax({
        type:'post',
        url:'<?=SITE_URL?>admin/complain/load_comments',
        data:{complain:complain}
        }).done(function(response){
        $('#timeline').html(response);
    });  
}

function makeread(){
 /*$("#addcomplain :input").prop("readonly", true);
 $('select').attr("disabled", false);*/
  $("textarea,select,input").attr("disabled", false);
 return true;
 }

</script>
<!--Name
Author:Sumith Nalwala
-->  
<script type='text/javascript'>
$(document).ready(function() {
	$("#uploader").plupload({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : '/ticket/admin/complain/upload',

		// User can upload no more then 20 files in one go (sets multiple_queues to false)
		max_file_count: 20,
		
		chunk_size: '1mb',

		// Resize images on clientside if we can
		resize : {
			width : 200, 
			height : 200, 
			quality : 90,
			crop: true // crop to exact dimensions
		},
		
		filters : {
			// Maximum file size
			max_file_size : '1000mb',
			// Specify what files to browse for
			mime_types: [
				{title : "Image files", extensions : "jpg,gif,png"},
				{title : "Zip files", extensions : "zip"},
				{title : "Pdf files", extensions : "pdf"},
				{title : "Word files", extensions : "doc,docx,xml,xls,csv"}
			]
		},

		// Rename files by clicking on their titles
		rename: true,
		
		// Sort files
		sortable: true,

		// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
		dragdrop: true,

		// Views to activate
		views: {
			list: true,
			thumbs: true, // Show thumbs
			active: 'thumbs'
		},

		// Flash settings
		flash_swf_url : '../../js/Moxie.swf',

		// Silverlight settings
		silverlight_xap_url : '../../js/Moxie.xap'
	});
	/*$('#submit').click(function(){
		if ($('#uploader').plupload('getFiles').length > 0) {
		$.ajax({
        type: "POST",
        url: '<?php echo SITE_URL;?>admin/complain/addcomplain',
        data: $( "#addcomplain" ).serialize(),
        success: function (response) {
			//alert(response);
			//alert('response'+response);
            //$("#" + container).html(response);
        }
    });
	}
	/*else {
			alert("You must have at least one file in the queue.");
		}
	});*/



$('a i').click(function(){
var id=$(this).attr('data-href');
$.post("<?=SITE_URL?>admin/complain/del_image", {id:id}, function(data) { 
		if(data){
		location.reload();
		}
	});
});

	$('.resstat').on('click',function(e){
	//e.preventDefault();
	var stats=$(this).attr('data-href');
	var cust_id=$('#cust_id').val();
	//alert(stats);
	$.ajax({
			type: "POST",
			url: '<?php echo SITE_URL;?>admin/complain/comp_status',
			data: {stats:stats,cust_id:cust_id},
			cache: false,
			success: function (response) {
				if(response){
					console.log(response);
					if(stats==1){
						$('#complainStatus #wrap').addClass('success');
						$('#complainStatus #wrap').removeClass('pending');
						$('#complainStatus #wrap').removeClass('putup');
						$('#complainStatus #wrap p').html('Resolved');
					}
					else if(stats==0){
						$('#complainStatus #wrap').addClass('pending');
						$('#complainStatus #wrap').removeClass('success');
						$('#complainStatus #wrap').removeClass('putup');
						$('#complainStatus #wrap p').html('Pending');
					}
					else{
						$('#complainStatus #wrap').addClass('putup');
						$('#complainStatus #wrap').removeClass('success');
						$('#complainStatus #wrap').removeClass('pending');
						$('#complainStatus #wrap p').html('Put up for Resolution');
					}
				}
			}
		})

	}); 
});
</script>
<!-- production -->