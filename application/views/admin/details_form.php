	<!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
             
              <div class="row">
                  <div class="col-lg-9">
                      <section class="panel">
                          <header class="panel-heading">
                            Change Password
                          </header>
                          <div class="panel-body">
						  <form  action="<?php echo UPDATEDETAILS;?>" method="POST" >
							  <div class="form-group">
                                      <label>Existing Password</label>
                                      <input type="password" class="form-control" id="old_pwd" name="old_pwd" value="" size="50" placeholder="Enter Current Password">
									  <p style='display:none;color:red;' id='chk'></p>
									  <?php echo form_error('old_pwd');?>
									  <label >New Password</label>
                                      <input type="password" class="form-control chksuccess" id="new_pwd" name="new_pwd" value="" size="50" placeholder="Enter New Password" readonly>
									  <?php echo form_error('new_pwd');?>
									  <label  >Re-Enter Password</label>
                                      <input type="password" class="form-control chksuccess" id="con_pwd" name="con_pwd" value="" size="50" placeholder="Re-Enter Password" readonly>
									  <p style='display:none;color:red;' id='pchk'></p>
									  <?php echo form_error('con_pwd');?>
                                  </div>
								  
                                  <input style='height:30px;width:80px;' type="submit" class="btn green " tabindex="16" id='submit' value='Update'>
                              </form>

                          </div>
                      </section>
                  </div>
                
              </div>
              
              <!-- page end-->
          </section>
      </section>
<script type='text/javascript'>
$('#old_pwd').blur(function(){
var pwd=$('#old_pwd').val();
	$.ajax({
	type:'post',
	url:'<?=SITE_URL?>admin/head/checkpwd',
	data:{pwd:pwd}
	}).done(function(response){
		if(response=='true'){
		$('#chk').hide();
		$('.chksuccess').prop("readonly", '');
		}
		else{
		$('#chk').show().html('Please Enter Current Password');
		$('.chksuccess').prop("readonly", true);
		}
	});

});

$('#con_pwd').blur(function(){
var npwd=$('#new_pwd').val();
var cpwd=$('#con_pwd').val();
	if(npwd!=cpwd){
		$('#pchk').show().html('Please Enter Same As New Password');
		event.preventDefault();
	}
});
</script>
    
     