<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="<?php echo ADMIN_URL?>img/favicon.png">

    <title>FlatLab - Flat & Responsive Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo ADMIN_URL?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ADMIN_URL?>css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo ADMIN_URL?>css/style.css" rel="stylesheet">
    <link href="<?php echo ADMIN_URL?>css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

<!--This form is for forggot password
#Author Sumith Nalwala
-->
<!-- Modal -->
<form method='POST' action='<?php echo FORGOT_PASSWORD;?>' id='forgot_pwd'>
         <div >
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" name="ckemail" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
						  <?php echo form_error('ckemail');?>
                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <input class="btn btn-success" type="submit" id='ckemailchk' value='Submit'>
                      </div>
                  </div>
              </div>
          </div>
          <!-- modal -->
</form>
    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo ADMIN_URL?>js/jquery.js"></script>
    <script src="<?php echo ADMIN_URL?>js/bootstrap.min.js"></script>


  </body>
</html>
<script type='text/javascript'>
/*$('#ckemailchk').click(function(){
var post = $('#forgot_pwd').serialize();
alert(post);
$('#forgot_pwd').submit();
	$.post("<?=SITE_URL?>admin/home/forgot_password", post, function(data) { 
		alert(data);
	});
});*/
</script>