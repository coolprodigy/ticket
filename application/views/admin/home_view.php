<!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!--state overview start-->
              <div class="row state-overview">
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="symbol terques">
                              <i class="fa fa-user"></i>
                          </div>
                          <div class="value">
                             
							  <h1 id='count'>
                                <?php 
								echo ($page_data['total']);
								 ?>

                              </h1>
                              <p>Users</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="symbol red">
                              <i class="fa fa-tags"></i>
                          </div>
                          <div class="value">
                              <h1 id="count2">
                                  <?php 
								echo count($page_data1); ?>
                              </h1>
                              <p>Total Complaints</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="symbol yellow">
                              <i class="fa fa-shopping-cart"></i>
                          </div>
                          <div class="value">
                              <h1 id=" count3">
                                <?php 
								echo $page_data2['comp_user_id']; ?>
                              </h1>
                              <p>Unassigned</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="symbol blue">
                              <i class="fa fa-bar-chart-o"></i>
                          </div>
                          <div class="value">
                              <h1 id=" count4">
                                  <?php 
								echo $page_data3['comp_user_id']; ?>
                              </h1>
                              <p>Assigned to others</p>
                          </div>
                      </section>
                  </div>
				                    <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="symbol blue">
                              <i class="fa fa-bar-chart-o"></i>
                          </div>
                          <div class="value">
                              <h1 id=" count4">
                                  <?php 
                                echo $page_data4['comp_user_id']; ?>
                              </h1>
                              <p>Assigned to Me</p>
                          </div>

              </div>
              <!--state overview end-->

              <!--<div class="row" style="height:462px;"> </div>
			   <!--timeline start-->
			   <div class="row" style="height:180px;"> </div>
                      <section class="panel" >
                          <div class="panel-body">
                                  <div class="text-center mbot30">
                                      <h3 class="timeline-title">Dashboard</h3>
                                      <p class="t-info">Comments</p>
                                  </div>
					
                                  <div class="timeline">
								  <?php $result=$this->do_user->allcomments();
								  #echo "<pre>";
								  #print_r($result);die;
								  $cnt=0;
								  foreach($result as $key=>$value){$cnt++;
								  $mod_date=strtotime($value['comment_added']);
								  $newdate=date('d-M-Y,h:i:s',$mod_date);
								  if($cnt%2==0){
								  ?>		 	
                                      <article class="timeline-item">
                                          <div class="timeline-desk">
                                              <div class="panel">
                                                  <div class="panel-body">
                                                      <span class="arrow"></span>
                                                      <span class="timeline-icon red"></span>
                                                      <span class="timeline-date"><?=$newdate;?></span>
                                                      <!--<h1 class="red">12 July | Sunday</h1>-->
                                                      <p><a href="#"><?php echo ucwords($value['name']);?></a><br/><a  href="<?php echo EDITCOMPLAIN.$value['comment_complaint_id'];?>" style='color:#797979;'><?php echo $value['comment_desc'];?></a></p>
                                                  </div>
                                              </div>
                                          </div>
                                      </article>
									  <?php } else {?>
                                      <article class="timeline-item alt">
                                          <div class="timeline-desk">
                                              <div class="panel">
                                                  <div class="panel-body">
                                                      <span class="arrow-alt"></span>
                                                      <span class="timeline-icon green"></span>
                                                      <span class="timeline-date"><?=$newdate;?></span>
                                                      <!--<h1 class="green">10 July | Wednesday</h1>-->
                                                      <!--<p><a href="#">Jonathan Smith</a> <!--added new milestone --><!--<span><a href="#" class="green">ERP</a></span></p>-->
													  <p><a style='color:#39B6AE;'><?php echo ucwords($value['name']);?></a><br/><a style='color:#797979;' href="<?php echo EDITCOMPLAIN.$value['comment_complaint_id'];?>"><?php echo $value['comment_desc'];?></a></p>
                                                  </div>
                                              </div>
                                          </div>
                                      </article>
									  <?php } } ?>
                                      <!--<article class="timeline-item">
                                          <div class="timeline-desk">
                                              <div class="panel">
                                                  <div class="panel-body">
                                                      <span class="arrow"></span>
                                                      <span class="timeline-icon blue"></span>
                                                      <span class="timeline-date">11:35 am</span>
                                                      <h1 class="blue">05 July | Monday</h1>
                                                      <p><a href="#">Anjelina Joli</a> added new album <span><a href="#" class="blue">PARTY TIME</a></span></p>
                                                      <div class="album">
                                                          <a href="#">
                                                              <img alt="" src="img/sm-img-1.jpg">
                                                          </a>
                                                          <a href="#">
                                                              <img alt="" src="img/sm-img-2.jpg">
                                                          </a>
                                                          <a href="#">
                                                              <img alt="" src="img/sm-img-3.jpg">
                                                          </a>
                                                          <a href="#">
                                                              <img alt="" src="img/sm-img-1.jpg">
                                                          </a>
                                                          <a href="#">
                                                              <img alt="" src="img/sm-img-2.jpg">
                                                          </a>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </article>
                                      <article class="timeline-item alt">
                                          <div class="timeline-desk">
                                              <div class="panel">
                                                  <div class="panel-body">
                                                      <span class="arrow-alt"></span>
                                                      <span class="timeline-icon purple"></span>
                                                      <span class="timeline-date">3:20 pm</span>
                                                      <h1 class="purple">29 June | Saturday</h1>
                                                      <p>Lorem ipsum dolor sit amet consiquest dio</p>
                                                      <div class="notification">
                                                          <i class=" fa fa-exclamation-sign"></i> New task added for <a href="#">Denial Collins</a>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </article>
                                      <article class="timeline-item">
                                          <div class="timeline-desk">
                                              <div class="panel">
                                                  <div class="panel-body">
                                                      <span class="arrow"></span>
                                                      <span class="timeline-icon light-green"></span>
                                                      <span class="timeline-date">07:49 pm</span>
                                                      <h1 class="light-green">10 June | Friday</h1>
                                                      <p><a href="#">Jonatha Smith</a> added new milestone <span><a href="#" class="light-green">prank</a></span> Lorem ipsum dolor sit amet consiquest dio</p>
                                                  </div>
                                              </div>
                                          </div>
                                      </article>-->
                                  </div>

                                  <div class="clearfix">&nbsp;</div>
                              </div>
                      </section>
                      <!--timeline end-->
           </section>
      </section>
    