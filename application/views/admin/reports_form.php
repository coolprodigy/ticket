<?php
if($this->session->flashdata('error')){
	echo $this->session->flashdata('error');
}
?>
<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/admin/plupload/css/jquery-ui.css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<!--main content start-->
      <section id="main-content">
          <section class="wrapper">
             <div class="row">
                  <div class="col-lg-12 min-height">
                      <section class="panel">
                          <header class="panel-heading">
                              Reports
                          </header>
						  <form  action="<?=GENERATEREPORTS;?>" method="POST" enctype="multipart/form-data" target='_blank'>
						  <br/>
						  &nbsp;By Date : <br/><br/>
						    &nbsp;<label for="startdate">Start Date : </label>
							<input type="text" id="date" name='strtdt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<label for="month">End Date : </label>
							<input type="text" id="date1" name='enddt'><br/><br/>
							&nbsp;By Month : <br/><br/>
						  &nbsp;<label for="month">Select Month: </label>
						  <input type="text" id="monthdt" name="monthdt" class="date-picker"/><br/>
						  <input type='submit' id='submit'><br/>
						  </form>
						  <!--<button>Total</button>
						  <button>Pending</button>
						  <button>Resolved</button><br/><br/>
						  <a href='<?=GETREPORTS;?>'><button>Get Reports</button></a>-->
						  <!--<table class="table table-striped table-advance table-hover display"  cellspacing="0" width="100%">
                              <thead>
                              <!--<tr>
                                  <th width="50%"> Name</th>
								  <th width="50%">Complaints</th>
                              </tr>-->
                              <!--</thead>
                              <tbody>
                                 <tr id=''>
                                  <td ></td>
                                  <td></td>-->
                                  <!--<td>
                                      <a href="<?php echo base_url()."form/edit/".$row['comp_cat_id']; ?>/view"><button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button></a>

                                      <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>

                                       <a> <button class="btn btn-danger btn-xs" > <i class="fa fa-trash-o "></i></button></a>
                                  </td>
                              </tr>
							
	                         <!--<tr class="odd gradeX"> 
	                         <td colspan="6">No records found</td> 
	                           </tr> 
                              </tr>-->
                            
                             <!--</tbody>
                          </table>-->
						  
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
	  <script>
	  $(document).ready(function()
{		
    $(function() {
     $('.date-picker').datepicker(
                    {
                        dateFormat: "mm/yy",
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        onClose: function(dateText, inst) {


                            function isDonePressed(){
                                return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
                            }

                            if (isDonePressed()){
								
                                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
                                
                                 $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
                            }
                        },
                        beforeShow : function(input, inst) {
                            inst.dpDiv.addClass('month_year_datepicker')
							
                            if ((datestr = $(this).val()).length > 0) {
                                year = datestr.substring(datestr.length-4, datestr.length);
                                month = datestr.substring(0, 2);
                                $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                                $(this).datepicker('setDate', new Date(year, month-1, 1));
                                $(".ui-datepicker-calendar").hide();
                            }
                        }
                    })
});

		$(".date-picker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
});

$(document).ready(function(){
	$("#date").datepicker({
	dateFormat:'yy-mm-dd',
    onSelect: function(){
		var lth = $('#date').val().length;
		if(lth>=10)
		{
			$('#monthdt').attr('disabled', true);
		}
    },
	onClose: function(dateText){
        if( !dateText ){
           $('#monthdt').attr('disabled', false);
        }
    }
});

});
</script>