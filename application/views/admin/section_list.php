<!--main content start-->
      <section id="main-content">
          <section class="wrapper">
             <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              List Of Copmlaint Categories
                          </header>
						  <table class="table table-striped table-advance table-hover display" id='dttable' cellspacing="0" width="100%">
                              <thead>
                              <tr>
                                  <th width="25%"> SRN</th>
								  <th width="60%">Section</th>
								  <th width="25%">Operation</th>
                              </tr>
                              </thead>
                              <tbody>
							 <?php $cnt=0; if(count($sec_list_data)>0){ foreach($sec_list_data as $row) { $cnt++;?>
                             <?php $id= $row['comp_sec_id']; ?>
                                 <tr id='<?=$row['comp_sec_id']?>'>
                                  <td data-id='<?php echo $cnt;?>'><?php echo $cnt;?></td>
                                  <td><?php echo $row['complaint_section'];?></td>
                                  <td>
                                      <!--<a href="<?php echo base_url()."form/edit/".$row['comp_cat_id']; ?>/view"><button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button></a>-->

                                      <a href="<?php echo EDITSECTION.$row['comp_sec_id']; ?>"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>

                                       <a> <button class="btn btn-danger btn-xs"  onclick="del_sec(<?php echo $row['comp_sec_id'];?>);"> <i class="fa fa-trash-o "></i></button></a>
                                  </td>
                              </tr>
							 <?php } }else{?>
	                         <tr class="odd gradeX"> 
	                         <td colspan="6">No records found</td> 
	                           </tr> 
                              </tr>
                            <?php }?>
                             </tbody>
                          </table>
						  
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
	 