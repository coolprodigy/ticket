 <?php 
if(isset($edit_data)){
extract($edit_data);
$catsel=explode(',',$cat_sel);
#print_R($edit_data);die;
}
?>
 <section id="main-content">
          <section class="wrapper">
            <div class="row">
                  <div class="col-lg-9">
                      <section class="panel">
                          <header class="panel-heading">
                            User Registration
                          </header>
                          <div class="panel-body">
                              <form action="<?php echo ADDUSER;?>" method="POST" enctype="multipart/form-data">
                                   <div class="form-group">
								  <label >Select Role:</label>
								  <select name='selrole' id='selrole' style='margin-left:30px;'>
								  <option value=''>Select Role</option>
								  <option value='1'>SuperAdmin</option>
								  <option value='0'>Admin</option>
								  <option value='2'>Assistant</option>
								  </select>
								  <?php echo form_error('selrole');?>
								  </div>
								  <div class="form-group">
								  <label>Designation</label>
								  <input class="form-control" type='text' name='desg' id='desg' value='<?=isset($designation)?$designation:'';?>'>
								  <?php echo form_error('desg');?>
								  </div>
								  <div class="form-group">
								  <input type='hidden' name='user_id' value='<?=isset($user_id)?$user_id:'';?>' id='user_id'>
                                      <label>First Name</label>
                                      <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Enter Your First Name" value="<?=isset($user_name)?$user_name:'';?>">
									  <?php echo form_error('user_name');?>
                                  </div>
								  <div class="form-group">
                                      <label>Last Name</label>
                                      <input type="text" class="form-control" name="user_lname" id="user_lname" placeholder="Enter Your Last Name" value="<?=isset($user_lname)?$user_lname:'';?>">
									  <?php echo form_error('user_lname');?>
                                  </div>
                                  <div class="form-group">
                                      <label>Mobile No.</label>
                                      <input type="text" class="form-control" name="user_mobile"id="user_mobile" placeholder="Enter Mobile Number" value='<?=isset($user_mobile)?$user_mobile:'';?>'>
									  <?php echo form_error('user_mobile');?>
                                  </div>
								  <div class="form-group">
                                      <label>Email </label>
                                      <input type="text" class="form-control" name="user_email"id="user_email" placeholder="Enter Email" value='<?=isset($user_email)?$user_email:'';?>'>
									  <?php echo form_error('user_email');?>
                                  </div>
								  <!--<div class="form-group">
								  <label >Password</label>
                                      <input type="password" class="form-control " id="pwd" name="pwd"  size="50" placeholder="Enter New Password"  value='<?=isset($password)?$password:'';?>'>
									  <?php echo form_error('pwd');?>
									  <label  >Confirm Password</label>
                                      <input type="password" class="form-control " id="con_pwd" name="con_pwd"  size="50" placeholder="Confirm Password"  value='<?=isset($password)?$password:'';?>'>
									   <p style='display:none;color:red;' id='pchk'></p>
									   <?php echo form_error('con_pwd');?>
								  </div>-->
								  <!--<div class="form-group">
                                      <label>Message</label>
                                     <textarea  name="user_message"  id="user_message" class="span12 ckeditor  m-wrap" cols="70" rows="5"> 
									  </textarea>
									  <?php echo form_error('user_message');?>
                                  </div>-->
								  <?php if(isset($image) && !empty($image)){?>
								  <div class="form-group">
                                      <label>Attached File:</label>
									  <img src="<?= SITE_URL.$image;?>" style='width:25px;height:25px;'>
                                  </div>
								  <?php } ?>
								 <div class="form-group">
                                      <label>Attach File</label>
									  <input type="file" name="image" id="image">
                                  </div>
								  <div class="form-group">
								  <label >Select Category</label>
								  <select name='selcat[]' multiple>
								  <?php /*if(isset($catsel)&&!empty($catsel)){if(array_intersect($catsel,$row)) { echo 'selected';}}*/?>
								  <?php $i=0; ?>
								  <?php foreach($cat_list_data as $row){?>
								  <option value='<?php echo $row["comp_cat_id"];?>' <?=(isset($catsel)&&!empty($catsel))?(array_intersect($catsel,$row)?'selected':''):'';?>><?php echo $row['cat_name'];?></option>
								  <?php $i++; ?>
								  <?php } ?>
								  </select>
								  </div>
                                  <input style='height:30px;width:80px;' type="submit" class="btn btn-info" id='submit' value='Submit'>
                              </form>

                          </div>
                      </section>
                  </div>
                
              </div>
              
              <!-- page end-->
          </section>
      </section>
    <script type='text/javascript'>
   $(document).ready(function(){
   if($('#user_id').val()!=''){
   $('#submit').val('Update');
   $('#selrole').val(<?=isset($role)?$role:'';?>);
   $('#user_email').prop('readonly',true);
   /*$('#pwd').prop('readonly',true);
   $('#con_pwd').prop('readonly',true);*/
   }
  
});
$('#con_pwd').blur(function(){
var npwd=$('#pwd').val();
var cpwd=$('#con_pwd').val();
	if(npwd!=cpwd){
		$('#pchk').show().html('Please Enter Same As Password');
	}

});
   </script> 