<!--
#AUTHOR:Sumith Nalwala
Date:16-04-2015
-->
<section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <ul class="directory-list" id='listuser_sec'>
					<li id=''><a>all</a></li>	
					<?php 
					$letters=range('a','z');
					foreach($letters as $letter){?>
					<li id='<?php echo $letter ;?>'><a><?php echo $letter ;?></a></li>
				  <?php } ?>
              </ul>
			  
              <div class="directory-info-row">
			  
              <div class="row" id='adduser'>
			  <?php foreach($page_data as $row){?>
              <div class="col-md-6 col-sm-6" id='listuser'>
                  <div class="panel">
                      <div class="panel-body">
                          <div class="media">
						  <div style='float:right;'><a href="<?php echo EDITUSER .$row['user_id']; ?>"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></div>
                              <a class="pull-left profile-pic" href="#">
                              <?php echo ($row['image']!='')?"<img class='thumb media-object' src='".SITE_URL.$row['image']."' >":"<img class='thumb media-object' src='".SITE_URL."assets/admin/img/default.jpg' >" ?>
							  </a>
                              <div class="media-body" >
                             <h4><?php echo ucfirst($row['user_name']).' '.ucfirst($row['user_lname']);?><!--<span class="text-muted small"> - UI Engineer</span>--></h4>
									<address>
                                     <p><span style="float:left;margin-left:0px;"><strong><?php echo $row['designation']?></strong></span></p><br>
                                      <p><?php echo $row['user_email']?></p>
                                      <abbr title="Phone"></abbr><p><?php echo $row['user_mobile'];?></p>
									  <p style='color:green;'>New Task Issued : <?php echo $row['assigned'];?></p>
									  <p style='color:red;'>Task Pending    : <?php echo $row['assigned'];?></p>
									  <p style='color:blue;'>Comments        : <?php echo $row['comments'];?></p>
                                  </address>
                                  <ul>
                                      <li><a><i></i></a></li>
                                      <li><a><i></i></a></li>
                                      <li><a><i></i></a></li>
                                      <li><a><i></i></a></li>
                                  </ul>
                                  

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
<?php } ?>


              </div>
              </div>
              <!-- page end-->
          </section>
      </section>
<!--
#AUTHOR:Sumith Nalwala
Date:16-04-2015
-->
<script type='text/javascript'>
	var searchval;
		$("#listuser_sec li").click(function() {
		var searchval=$(this).attr('id');
		listuser(searchval);
	});
	
	function listuser(searchval){
		$.ajax({
		url:'<?=SITE_URL?>admin/user/get_searched_user',
		type:"POST",
		data:{srch:searchval}
		}).done(function(response){
			if(response){
			$('#adduser').html('<h3>No Records Found</h3>').removeClass('norec');
			$('#adduser').html(response);
			}
			else{
			$('#adduser').html('<h3>No Records Found</h3>').addClass('norec');
			}
		});
	}
</script>
<style>
.norec{
margin-left:500px;
color:red;
margin-top:200px;
}
</style>