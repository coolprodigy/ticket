/*
#Author:Sumith Nalwala
#Date:19/03/3015.
This function is ajax based for deleting the category.
If you are making any changes then please mention the date and comment the changes.
*/
/*
#Date:19/03/3015.
This function deletes category*/
function del_cat(id){
if(confirm('Are you sure you want to delete category?')){
	$.ajax({
	type:"POST",
	data:{id:id},
	url:'del_cat',
	}).done(function(data){
		resp=jQuery.parseJSON(data);
		if(resp==true){
		$('#'+id).hide('slow', function(){ $('#'+id).remove(); });
		
		}
	});
  }	
}

/*#Author:Sumith Nalwala
#Date:20/03/3015.
This function deletes user*/
function del_user(id){
if(confirm('Are you sure you want to delete user?')){
	$.ajax({
	type:"POST",
	data:{id:id},
	url:'del_user',
	}).done(function(data){
		resp=jQuery.parseJSON(data);
		if(resp==true){
		$('#'+id).hide('slow', function(){ $('#'+id).remove(); });
		
		}
	});
  }
}
/*#Author:Sumith Nalwala
Date:20/03/3015.
This function deletes complain*/
function del_comp(id){
if(confirm('Are you sure you want to delete complain?')){
	$.ajax({
	type:"POST",
	data:{id:id},
	url:'del_comp',
	}).done(function(data){
		resp=jQuery.parseJSON(data);
		if(resp==true){
		$('#'+id).hide('slow', function(){ $('#'+id).remove(); });
		
		}
	});
  }	
}

/*This function deletes section*/
function del_sec(id){
if(confirm('Are you sure you want to delete section?')){
	$.ajax({
	type:"POST",
	data:{id:id},
	url:'del_sec',
	}).done(function(data){
		resp=jQuery.parseJSON(data);
		if(resp==true){
		$('#'+id).hide('slow', function(){ $('#'+id).remove(); });
		
		}
	});
  }	
}