-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2015 at 08:28 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `grievance`
--

-- --------------------------------------------------------

--
-- Table structure for table `complaint_assigned`
--

CREATE TABLE IF NOT EXISTS `complaint_assigned` (
`comp_id` int(10) NOT NULL,
  `comp_cust_id` int(10) NOT NULL,
  `comp_user_id` int(10) NOT NULL,
  `comp_status` tinyint(2) NOT NULL,
  `comp_assigned_by` int(10) NOT NULL,
  `comp_added` varchar(60) NOT NULL,
  `comp_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `complaint_assigned`
--

INSERT INTO `complaint_assigned` (`comp_id`, `comp_cust_id`, `comp_user_id`, `comp_status`, `comp_assigned_by`, `comp_added`, `comp_modified`) VALUES
(1, 11, 2, 0, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(3, 13, 9, 0, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(4, 14, 9, 0, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(5, 15, 2, 0, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(10, 20, 2, 0, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(11, 22, 10, 1, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(13, 24, 2, 1, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(15, 26, 10, 1, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(21, 32, 2, 1, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(22, 33, 10, 1, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(24, 35, 9, 1, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(25, 36, 0, 1, 0, '2015-04-14 11:32:42', '2015-04-22 09:31:33'),
(26, 37, 10, 1, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00'),
(27, 38, 10, 1, 0, '2015-04-14 11:32:42', '2015-04-14 08:06:56'),
(28, 39, 15, 1, 0, '2015-04-18 12:19:17', '2015-04-22 06:27:03'),
(29, 40, 15, 1, 0, '2015-04-18 12:22:06', '2015-04-18 06:53:12'),
(30, 41, 15, 1, 0, '2015-04-22 11:22:59', '2015-04-22 05:53:13'),
(31, 42, 3, 1, 0, '2015-05-04 18:03:48', '2015-05-21 10:47:50'),
(32, 43, 3, 1, 2, '2015-05-15 14:27:55', '2015-05-22 09:40:29'),
(33, 44, 9, 1, 2, '2015-05-20 12:00:08', '2015-05-25 06:03:13'),
(34, 65, 0, 1, 2, '2015-05-26 15:52:53', '2015-05-26 10:23:34'),
(36, 71, 9, 0, 2, '2015-05-28 11:13:10', '0000-00-00 00:00:00'),
(37, 72, 9, 1, 2, '2015-05-28 16:21:09', '2015-05-28 10:52:18');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_assigned_record`
--

CREATE TABLE IF NOT EXISTS `complaint_assigned_record` (
`comp_record_id` int(10) NOT NULL,
  `comp_id` int(60) NOT NULL,
  `comp_cust_id` int(60) NOT NULL,
  `comp_user_id` int(60) NOT NULL,
  `comp_status` tinyint(10) NOT NULL,
  `comp_assigned_by` int(10) NOT NULL,
  `comp_added` varchar(30) NOT NULL,
  `comp_modified` varchar(30) NOT NULL,
  `comp_record_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `complaint_assigned_record`
--

INSERT INTO `complaint_assigned_record` (`comp_record_id`, `comp_id`, `comp_cust_id`, `comp_user_id`, `comp_status`, `comp_assigned_by`, `comp_added`, `comp_modified`, `comp_record_added`) VALUES
(1, 27, 36, 10, 1, 0, '2015-04-14 11:32:42', '0000-00-00 00:00:00', '2015-04-14 08:02:08'),
(2, 27, 36, 14, 1, 0, '2015-04-14 11:32:42', '2015-04-14 13:32:08', '2015-04-14 08:06:56'),
(3, 27, 36, 10, 1, 0, '2015-04-14 11:32:42', '2015-04-14 13:36:56', '2015-04-14 08:10:12'),
(4, 30, 41, 15, 1, 0, '2015-04-22 11:22:59', '2015-04-22 11:23:13', '0000-00-00 00:00:00'),
(5, 25, 36, 3, 1, 0, '2015-04-14 11:32:42', '2015-04-14 17:23:42', '0000-00-00 00:00:00'),
(6, 25, 36, 3, 1, 0, '2015-04-14 11:32:42', '2015-04-22 13:21:01', '0000-00-00 00:00:00'),
(9, 25, 36, 14, 1, 0, '2015-04-14 11:32:42', '2015-04-22 13:27:47', '2015-04-22 09:31:33'),
(10, 25, 36, 0, 1, 0, '2015-04-14 11:32:42', '2015-04-22 15:01:33', '2015-04-22 10:13:42'),
(33, 32, 43, 10, 1, 2, '2015-05-15 14:27:55', '2015-05-15 16:20:40', '2015-05-15 10:50:50');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_attached`
--

CREATE TABLE IF NOT EXISTS `complaint_attached` (
`comp_att_id` int(30) NOT NULL,
  `comp_cust_id` int(60) NOT NULL,
  `comp_assist_id` varchar(60) DEFAULT NULL,
  `comp_assist_added_by` int(10) NOT NULL,
  `comp_assist_added` varchar(60) NOT NULL,
  `comp_assist_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `complaint_attached`
--

INSERT INTO `complaint_attached` (`comp_att_id`, `comp_cust_id`, `comp_assist_id`, `comp_assist_added_by`, `comp_assist_added`, `comp_assist_modified`) VALUES
(1, 36, '12,13', 0, '2015-04-14 13:00:33', '2015-04-22 10:26:41'),
(2, 37, NULL, 0, '2015-04-14 13:00:33', '0000-00-00 00:00:00'),
(3, 38, '12,13', 0, '2015-04-14 13:00:33', '2015-04-14 10:50:44'),
(4, 39, '12', 0, '2015-04-18 12:19:17', '2015-05-21 07:15:55'),
(5, 40, '', 0, '2015-04-18 12:22:06', '0000-00-00 00:00:00'),
(6, 41, '12,13', 0, '2015-04-22 11:22:59', '2015-04-28 07:13:31'),
(7, 42, '', 0, '2015-05-04 18:03:48', '0000-00-00 00:00:00'),
(8, 43, '12,13', 2, '2015-05-15 14:27:55', '2015-05-15 10:36:30'),
(9, 44, '13,12', 2, '2015-05-20 12:00:08', '2015-05-21 11:44:32'),
(10, 65, '', 2, '2015-05-26 15:52:53', '0000-00-00 00:00:00'),
(12, 71, '12', 2, '2015-05-28 14:43:10', '0000-00-00 00:00:00'),
(13, 72, '12', 2, '2015-05-28 16:21:09', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_attached_record`
--

CREATE TABLE IF NOT EXISTS `complaint_attached_record` (
`comp_record_id` int(60) NOT NULL,
  `comp_att_id` int(60) NOT NULL,
  `comp_cust_id` varchar(60) NOT NULL,
  `comp_assist_id` varchar(25) DEFAULT NULL,
  `comp_assist_added_by` int(10) NOT NULL,
  `comp_assist_added` varchar(25) NOT NULL,
  `comp_assist_modified` varchar(25) NOT NULL,
  `comp_record_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `complaint_attached_record`
--

INSERT INTO `complaint_attached_record` (`comp_record_id`, `comp_att_id`, `comp_cust_id`, `comp_assist_id`, `comp_assist_added_by`, `comp_assist_added`, `comp_assist_modified`, `comp_record_added`) VALUES
(9, 6, '41', '', 0, '2015-04-22 11:22:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 1, '36', '13,12', 0, '2015-04-14 13:00:33', '2015-04-15 15:58:45', '0000-00-00 00:00:00'),
(11, 1, '36', '12,13', 0, '2015-04-14 13:00:33', '2015-04-22 13:21:31', '0000-00-00 00:00:00'),
(17, 1, '36', '13', 0, '2015-04-14 13:00:33', '2015-04-22 15:55:38', '2015-04-22 10:26:41'),
(18, 6, '41', '12,13', 0, '2015-04-22 11:22:59', '2015-04-28 12:43:31', '2015-05-02 05:52:09'),
(38, 8, '43', '13', 2, '2015-05-15 14:27:55', '2015-05-15 15:51:10', '2015-05-15 10:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_category`
--

CREATE TABLE IF NOT EXISTS `complaint_category` (
`category_id` int(10) NOT NULL,
  `comp_cat_id` varchar(60) DEFAULT NULL,
  `comp_cust_id` int(10) NOT NULL,
  `comp_cat_added_by` int(10) NOT NULL,
  `category_added` varchar(60) NOT NULL,
  `category_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `complaint_category`
--

INSERT INTO `complaint_category` (`category_id`, `comp_cat_id`, `comp_cust_id`, `comp_cat_added_by`, `category_added`, `category_modified`) VALUES
(1, '25,30', 36, 0, '2015-04-22 13:03:37', '2015-05-15 06:03:49'),
(2, '', 42, 0, '2015-05-04 18:03:48', '2015-05-15 07:41:11'),
(4, '12,23', 43, 2, '2015-05-15 15:03:03', '2015-05-21 06:48:33'),
(5, '23', 44, 2, '2015-05-20 12:00:08', '0000-00-00 00:00:00'),
(6, '23', 65, 2, '2015-05-26 15:52:53', '0000-00-00 00:00:00'),
(8, '23', 71, 2, '2015-05-28 14:43:10', '0000-00-00 00:00:00'),
(9, '23', 72, 2, '2015-05-28 16:21:09', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_category_record`
--

CREATE TABLE IF NOT EXISTS `complaint_category_record` (
`cat_record_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `comp_cat_id` varchar(60) NOT NULL,
  `comp_cust_id` int(25) NOT NULL,
  `comp_cat_added_by` int(10) NOT NULL,
  `category_added` varchar(30) NOT NULL,
  `category_modified` varchar(30) NOT NULL,
  `cat_record_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `complaint_category_record`
--

INSERT INTO `complaint_category_record` (`cat_record_id`, `category_id`, `comp_cat_id`, `comp_cust_id`, `comp_cat_added_by`, `category_added`, `category_modified`, `cat_record_added`) VALUES
(1, 1, '31,32', 36, 0, '2015-04-22 13:03:37', '2015-04-22 13:27:47', '2015-04-22 09:31:34'),
(2, 1, '23,25', 36, 0, '2015-04-22 13:03:37', '2015-04-22 15:20:42', '2015-04-22 10:13:42'),
(3, 1, '', 36, 0, '2015-04-22 13:03:37', '2015-04-22 15:43:42', '2015-04-22 10:25:38'),
(4, 1, '23,25', 41, 0, '2015-04-22 13:03:37', '2015-04-29 12:28:08', '2015-05-02 05:52:09'),
(5, 1, '25', 41, 0, '2015-04-22 13:03:37', '2015-05-02 11:22:09', '2015-05-02 06:00:51'),
(22, 4, '23', 43, 2, '2015-05-15 15:03:03', '2015-05-15 15:10:53', '2015-05-15 10:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_comment`
--

CREATE TABLE IF NOT EXISTS `complaint_comment` (
`comment_id` int(60) NOT NULL,
  `comment_complaint_id` int(60) NOT NULL,
  `comment_desc` text CHARACTER SET utf8 NOT NULL,
  `comment_user_id` int(60) NOT NULL,
  `comment_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment_status` tinyint(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `complaint_comment`
--

INSERT INTO `complaint_comment` (`comment_id`, `comment_complaint_id`, `comment_desc`, `comment_user_id`, `comment_added`, `comment_status`) VALUES
(2, 26, 'Please clean the society', 2, '2015-04-03 10:24:03', 1),
(3, 26, 'Cleanliness is neccessary....', 2, '2015-04-03 10:28:41', 1),
(4, 26, 'Swach bharat abhiyaan', 2, '2015-04-06 06:42:44', 1),
(5, 26, 'Garbages in dharavi', 2, '2015-04-06 06:43:37', 1),
(6, 24, 'Gutter', 3, '2015-04-06 09:12:17', 1),
(7, 24, 'latrine', 3, '2015-04-06 09:12:17', 1),
(8, 24, 'Urinal problem', 3, '2015-04-07 05:23:49', 1),
(9, 35, 'this is for the world', 2, '2015-04-07 11:19:49', 1),
(10, 26, 'Please make sure that the complaints is resolved within 7 days. You will be required  to take clearance from water department also.', 2, '2015-05-14 13:28:25', 1),
(11, 36, 'Please make sure that this issue is resolved within 7 days. Let me know if there is anyy problem', 2, '2015-04-07 14:09:18', 1),
(12, 24, 'This has been resolved at my end so kindly rectify', 3, '2015-04-07 14:12:13', 1),
(13, 36, 'It has been attached to abdul', 3, '2015-04-13 07:21:18', 1),
(14, 36, 'Please have a look', 13, '2015-04-13 07:22:53', 1),
(15, 36, 'Go through for the changes', 13, '2015-04-13 07:25:13', 1),
(16, 37, 'Please action in 3 days', 2, '2015-04-13 08:26:21', 1),
(17, 38, 'Please action in 5 days', 2, '2015-04-13 08:34:01', 1),
(18, 36, 'Recommended', 13, '2015-04-13 11:20:59', 1),
(19, 36, 'Little issues need to be resolved', 13, '2015-04-13 11:29:25', 1),
(25, 42, '\r\niz[kaM fodkl inkf/kdkjh] cgknqjiqj us vius i=kad 1981 fnukad 9-11-09 }kjk izfrosfnd fd;k gS fd vkosnd ds ekeys dh tkap djk;h x;h] tkapksijkUr ik;k x;k fd oSls ifjokj ftudk edku ck<+ esa /oLr gqvk Fkk] mUgsa eq[;ea=h vkokl dk ykHk fu;ekuqdwy fn;k tk pqdk gS A orZeku esa vkosnd dk dFku vizklafxd gS ', 2, '2015-05-04 12:33:53', 1),
(26, 41, 'Please action in 5 days', 2, '2015-05-15 06:13:38', 1),
(32, 44, 'Please action in 7 days', 2, '2015-05-15 08:57:55', 1),
(33, 44, 'Please action in 3 days', 2, '2015-05-20 06:30:08', 1),
(34, 44, 'Please action in 7 days', 3, '2015-05-21 06:51:20', 1),
(35, 43, 'Please action in 5 days', 13, '2015-05-21 07:39:23', 1),
(36, 70, 'Please action in 7 days', 2, '2015-05-28 08:01:50', 0),
(37, 71, 'Please action in 3 days', 2, '2015-05-28 09:13:11', 0),
(38, 72, 'Please action in 3 days', 2, '2015-05-28 10:51:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `complaint_section`
--

CREATE TABLE IF NOT EXISTS `complaint_section` (
`comp_sec_id` int(10) NOT NULL,
  `complaint_section` varchar(60) CHARACTER SET utf8 NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `complaint_section`
--

INSERT INTO `complaint_section` (`comp_sec_id`, `complaint_section`, `date_added`) VALUES
(1, 'eq[;ea=h lfpoky;', '2015-05-28 08:29:47'),
(2, 'vk;qDr dk;kZy;', '2015-05-28 08:29:47'),
(3, ' vU; foHkkx', '2015-05-28 08:29:47'),
(4, 'ftyk inkf/kdkjh', '2015-05-28 08:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `comp_cat`
--

CREATE TABLE IF NOT EXISTS `comp_cat` (
`comp_cat_id` int(10) NOT NULL,
  `cat_name` varchar(150) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `comp_cat`
--

INSERT INTO `comp_cat` (`comp_cat_id`, `cat_name`, `date_added`) VALUES
(12, 'Water Department', '2015-05-28 08:30:09'),
(23, 'Electricity Department', '2015-05-28 08:30:09'),
(24, 'Land Department', '2015-05-28 08:30:09'),
(25, 'Anti Corruption Department', '2015-05-28 08:30:09'),
(30, 'PDS Department', '2015-05-28 08:30:09'),
(31, 'Revenue Department', '2015-05-28 08:30:09'),
(32, 'Collector Office ', '2015-05-28 08:30:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_complain`
--

CREATE TABLE IF NOT EXISTS `user_complain` (
`cust_id` int(5) NOT NULL,
  `cust_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `cust_lastname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `cust_mobile` varchar(11) DEFAULT NULL,
  `cust_date` date DEFAULT NULL,
  `cust_message` text CHARACTER SET utf8,
  `cust_address` text CHARACTER SET utf8 NOT NULL,
  `cust_email` varchar(100) DEFAULT NULL,
  `ticket_no` varchar(60) NOT NULL,
  `section` varchar(60) CHARACTER SET utf8 NOT NULL,
  `date_added` varchar(60) NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `complain_added_by` varchar(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `user_complain`
--

INSERT INTO `user_complain` (`cust_id`, `cust_name`, `cust_lastname`, `cust_mobile`, `cust_date`, `cust_message`, `cust_address`, `cust_email`, `ticket_no`, `section`, `date_added`, `date_modified`, `complain_added_by`) VALUES
(11, 'ajay', 'chaudhary', '9757252991', '2015-03-27', ' \r\n									  Draining problem', '', 'abc@abc.com', '4', '2', '0000-00-00 00:00:00', '2015-05-20 06:33:22', '2'),
(13, 'pankaj', 'kappoo', '9823986547', '2015-03-31', ' \r\n									  dsdsdsdsdsd', '', 'abc@abc.com', '6', '2', '0000-00-00 00:00:00', '2015-05-20 06:33:34', '2'),
(14, 'prabhat', 'pal', '9757252992', '2015-03-31', ' dsfdfdfdgfgf\r\n									  ', '', 'abc@abc.com', '7', '2', '0000-00-00 00:00:00', '2015-05-20 06:33:38', '2'),
(15, 'ajay', 'chaudhary', '9757252991', '2015-04-02', ' Land jhdjfdfhfkdj\r\n									  ', '', 'abc@abc.com', '8', '2', '0000-00-00 00:00:00', '2015-05-13 11:37:33', '2'),
(20, 'sdfgsdfg', 'sdfgsdfg', '9565625623', '2015-04-03', ' \r\n									  sdsdsdsdsdsd', '', 'abc@abc.com', '9', '2', '2015-04-02 15:47:54', '2015-05-13 11:37:33', '2'),
(22, 'ajay', 'chaudhary', '9757252991', '2015-04-02', ' \r\n									  asasasasas', '', 'abc@abc.com', '11', '2', '2015-04-02 17:15:50', '2015-05-13 11:37:33', '2'),
(24, 'prabhat', 'pal', '9757252992', '2015-04-01', 'garbage \r\n									  ', '', 'abc@abc.com', '12', '2', '2015-04-03 15:22:47', '2015-05-13 11:37:33', '2'),
(26, 'prabhat', 'pal', '9757252992', '2015-04-03', 'Swach bharat abhiyaan \r\n									  ', '', 'abc@abc.com', '13', '2', '2015-04-03 15:54:03', '2015-05-13 11:37:33', '2'),
(32, 'dayacid', 'hgsdhdg', '9789562356', '2015-04-02', ' \r\n									  adssdsdsdff', '', 'abc@abc.com', '14', '2', '2015-04-07 15:50:11', '2015-05-13 11:37:33', '2'),
(33, 'pradhyuman', 'gayay', '9814745623', '2015-04-09', ' \r\n									  asdsdfdfdfdff', '', 'abc@abc.com', '15', '2', '2015-04-07 16:20:05', '2015-05-13 11:37:33', '2'),
(35, 'rakesh', 'roshan', '9812122325', '2015-04-03', ' hove fashions\r\n									  ', '', 'abc@abc.com', '16', '2', '2015-04-07 16:48:38', '2015-05-13 11:37:33', '2'),
(36, 'ajay', 'chaudhary', '9757252991', '2015-04-11', 'PLease clean water tank near our house \r\n									  ', '', 'abc@abc.com', '17', '2', '2015-04-07 19:39:13', '2015-05-13 11:37:33', '2'),
(37, 'Rakesh', 'Singh', '9865235689', '2015-04-12', ' \r\n									  Hospital Issues', '', 'abc@abc.com', '18/13-04-15', '2', '2015-04-13 13:56:16', '2015-05-15 06:48:43', '2'),
(38, 'Ramesh', 'Mehra', '9874567889', '2015-04-17', 'School Issues \r\n									  ', '', 'abc@abc.com', '19/13-04-15', '2', '2015-04-14 14:03:57', '2015-05-15 06:48:36', '2'),
(39, 'Meeankhshi', 'Shashadri', '9856235647', '2015-04-16', 'Film problems \r\n									  ', '', 'abc@abc.com', '20/18-04-15', '2', '0000-00-00 00:00:00', '2015-05-15 06:48:25', '2'),
(40, 'Akshay', 'Khanna', '9568925623', '2015-04-03', 'Society \r\n									  ', '', 'abc@abc.com', '1/18-04-15', '2', '2015-04-18 12:22:06', '2015-05-15 06:48:16', '2'),
(41, 'Pratinidhi', 'पाकिस्तान', '9875895623', '2015-04-03', 'Train Rush \r\n									  ', '', 'abc@abc.com', '2/22-04-15', '2', '2015-04-22 11:22:59', '2015-05-13 11:37:33', '2'),
(42, 'eukst ', 'dqekj feJ]', '9825321569', '2015-05-07', ' eq[;ea=h vkokl ds laca/k easa\r\n									  ', '', 'abc@abc.com', '3/04-05-15', '2', '2015-05-04 18:03:48', '2015-05-12 10:40:10', '2'),
(43, 'Amit', 'Parab', '9856784593', '2015-05-15', ' Technical Issues\r\n									  ', ' Technical Issues\r\n									  ', 'amitparab@amit.com', '4/15-05-15', '1', '2015-05-15 14:27:55', '2015-05-26 06:25:24', '2'),
(44, 'hsgdhgsh', 'sghdgsdhgsdj', '9875256389', '2015-05-20', 'asasdsdsdsdsd', 'sdsdsdsdsds', 'ashag@ahgs.com', '5/20-05-15', '1', '2015-05-20 12:00:08', '2015-05-20 06:33:44', '2'),
(64, 'ajit', 'Ingulkar', '9856897413', '2015-05-26', 'asdfddfgfgfgfghgh', 'fdsgfgfgdhhggfh', 'abc@abc.com', '6/26-05-15', '2', '2015-05-26 15:52:17', '0000-00-00 00:00:00', '2'),
(65, 'ajit', 'ingulkar', '9856897413', '2015-05-26', 'asdfddfgfgfgfghgh', 'fdsgfgfgdhhggfh', 'abc@abc.com', '7/26-05-15', '2', '2015-05-26 15:52:53', '2015-05-28 07:39:44', '2'),
(72, 'sumith', 'sjdksdjk', '9856238956', '2015-05-28', 'sdaffddfgfghh', 'dffffffffffffff', 'abc@abc.com', '8/28-05-15', '', '2015-05-28 16:21:09', '0000-00-00 00:00:00', '2');

-- --------------------------------------------------------

--
-- Table structure for table `user_complain_images`
--

CREATE TABLE IF NOT EXISTS `user_complain_images` (
`img_id` int(10) NOT NULL,
  `cust_id` int(10) NOT NULL,
  `img_path` varchar(100) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_complain_images`
--

INSERT INTO `user_complain_images` (`img_id`, `cust_id`, `img_path`, `date_added`) VALUES
(1, 43, 'uploads/private.docx', '2015-05-26 10:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_complain_record`
--

CREATE TABLE IF NOT EXISTS `user_complain_record` (
`cust_id_record` int(5) NOT NULL,
  `cust_id` int(5) NOT NULL,
  `cust_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `cust_lastname` varchar(50) CHARACTER SET utf8 NOT NULL,
  `cust_mobile` varchar(11) NOT NULL,
  `cust_date` varchar(60) NOT NULL,
  `cust_message` text CHARACTER SET utf8 NOT NULL,
  `cust_address` text CHARACTER SET utf8 NOT NULL,
  `cust_email` varchar(100) NOT NULL,
  `ticket_no` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `date_added` varchar(30) NOT NULL,
  `date_modified` varchar(30) NOT NULL,
  `complain_added_by` varchar(10) NOT NULL,
  `complain_edited_by` varchar(10) NOT NULL,
  `cust_record_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `user_complain_record`
--

INSERT INTO `user_complain_record` (`cust_id_record`, `cust_id`, `cust_name`, `cust_lastname`, `cust_mobile`, `cust_date`, `cust_message`, `cust_address`, `cust_email`, `ticket_no`, `section`, `date_added`, `date_modified`, `complain_added_by`, `complain_edited_by`, `cust_record_added`) VALUES
(13, 43, '', '', '9856784592', '', '', '', '', '', '', '2015-05-15 14:27:55', '2015-05-25 18:19:04', '2', '2', '2015-05-26 06:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `user_register`
--

CREATE TABLE IF NOT EXISTS `user_register` (
`user_id` int(5) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_lname` varchar(70) DEFAULT NULL,
  `user_email` varchar(70) DEFAULT NULL,
  `user_mobile` varchar(13) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `designation` varchar(60) NOT NULL,
  `cat_sel` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `user_register`
--

INSERT INTO `user_register` (`user_id`, `user_name`, `user_lname`, `user_email`, `user_mobile`, `password`, `role`, `designation`, `cat_sel`, `image`, `date_added`) VALUES
(2, 'rahul', 'pawar', 'admin@admin.com', '9757252991', '21232f297a57a5a743894a0e4a801fc3', 1, 'Block Devlopment Officer', '22,23,25', '', '2015-05-28 08:30:40'),
(3, 'Subhash', 'Kumar', 'user@user.com', '9757252991', '21232f297a57a5a743894a0e4a801fc3', 0, 'Block Devlopment Officer', '24,25', '', '2015-05-28 08:30:40'),
(9, 'Dharmendra', 'Gupta', 'sachin@softpillar.com', '9820568923', '21232f297a57a5a743894a0e4a801fc3', 0, 'Block Devlopment Officer', '22,29', '/uploads/men.jpg', '2015-05-28 08:30:40'),
(10, 'sumit', 'nalwala', 'sumitnalwala3@gmail.com', '9757252991', '21232f297a57a5a743894a0e4a801fc3', 0, 'Circle Officer', '22,23', '', '2015-05-28 08:30:40'),
(11, 'Rahul', 'Jain', 'sachin@softpillar.com', '91111125441', '3da1424477dab64ee77978af08aa77e5', 0, 'Circle Officer', '22,23', '/uploads/men.jpg', '2015-05-28 08:30:40'),
(12, 'Amit', 'Khan', 'abc@abc.com', '9856235623', '21232f297a57a5a743894a0e4a801fc3', 2, 'Block Devlopment Officer', '22', '/uploads/men.jpg', '2015-05-28 08:30:40'),
(13, 'Abdul', 'changez', 'assistant@gmail.com', '9757252991', '21232f297a57a5a743894a0e4a801fc3', 2, 'Circle Officer', '22', '', '2015-05-28 08:30:40'),
(14, 'Arjun', 'Singh', 'abc@abc.com', '9757252991', '4dfb5eb7b80265c94fbd35f744965396', 0, 'Circle Officer', '31', '', '2015-05-28 08:30:40'),
(15, 'Gitanjali', 'Dugal', 'gitanjali@dugal.com', '9874562341', '310eacfe0fd43ac6248e45535ca16242', 0, 'Legal Advisor', '23,24', '', '2015-05-28 08:30:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `complaint_assigned`
--
ALTER TABLE `complaint_assigned`
 ADD PRIMARY KEY (`comp_id`), ADD UNIQUE KEY `comp_cust_id` (`comp_cust_id`);

--
-- Indexes for table `complaint_assigned_record`
--
ALTER TABLE `complaint_assigned_record`
 ADD PRIMARY KEY (`comp_record_id`), ADD UNIQUE KEY `comp_cust_id` (`comp_cust_id`,`comp_user_id`,`comp_modified`);

--
-- Indexes for table `complaint_attached`
--
ALTER TABLE `complaint_attached`
 ADD PRIMARY KEY (`comp_att_id`), ADD UNIQUE KEY `comp_cust_id` (`comp_cust_id`);

--
-- Indexes for table `complaint_attached_record`
--
ALTER TABLE `complaint_attached_record`
 ADD PRIMARY KEY (`comp_record_id`), ADD UNIQUE KEY `comp_cust_id` (`comp_cust_id`,`comp_assist_id`,`comp_assist_modified`);

--
-- Indexes for table `complaint_category`
--
ALTER TABLE `complaint_category`
 ADD PRIMARY KEY (`category_id`), ADD UNIQUE KEY `comp_cust_id` (`comp_cust_id`);

--
-- Indexes for table `complaint_category_record`
--
ALTER TABLE `complaint_category_record`
 ADD PRIMARY KEY (`cat_record_id`), ADD UNIQUE KEY `comp_cat_id` (`comp_cat_id`,`comp_cust_id`,`category_modified`);

--
-- Indexes for table `complaint_comment`
--
ALTER TABLE `complaint_comment`
 ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `complaint_section`
--
ALTER TABLE `complaint_section`
 ADD PRIMARY KEY (`comp_sec_id`);

--
-- Indexes for table `comp_cat`
--
ALTER TABLE `comp_cat`
 ADD PRIMARY KEY (`comp_cat_id`);

--
-- Indexes for table `user_complain`
--
ALTER TABLE `user_complain`
 ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `user_complain_images`
--
ALTER TABLE `user_complain_images`
 ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `user_complain_record`
--
ALTER TABLE `user_complain_record`
 ADD PRIMARY KEY (`cust_id_record`);

--
-- Indexes for table `user_register`
--
ALTER TABLE `user_register`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `complaint_assigned`
--
ALTER TABLE `complaint_assigned`
MODIFY `comp_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `complaint_assigned_record`
--
ALTER TABLE `complaint_assigned_record`
MODIFY `comp_record_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `complaint_attached`
--
ALTER TABLE `complaint_attached`
MODIFY `comp_att_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `complaint_attached_record`
--
ALTER TABLE `complaint_attached_record`
MODIFY `comp_record_id` int(60) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `complaint_category`
--
ALTER TABLE `complaint_category`
MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `complaint_category_record`
--
ALTER TABLE `complaint_category_record`
MODIFY `cat_record_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `complaint_comment`
--
ALTER TABLE `complaint_comment`
MODIFY `comment_id` int(60) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `complaint_section`
--
ALTER TABLE `complaint_section`
MODIFY `comp_sec_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `comp_cat`
--
ALTER TABLE `comp_cat`
MODIFY `comp_cat_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `user_complain`
--
ALTER TABLE `user_complain`
MODIFY `cust_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `user_complain_images`
--
ALTER TABLE `user_complain_images`
MODIFY `img_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_complain_record`
--
ALTER TABLE `user_complain_record`
MODIFY `cust_id_record` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user_register`
--
ALTER TABLE `user_register`
MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
